import {BitFlag, IBitFlag} from "./bit-flag";

export class ExtraFlags {

  public static readonly extraFlags: Array<IBitFlag> = [
    {"bit": BitFlag.A, "name": "Glow"},
    {"bit": BitFlag.B, "name": "Hum"},
    {"bit": BitFlag.C, "name": "Dark"},
    {"bit": BitFlag.D, "name": "Hidden"},
    {"bit": BitFlag.E, "name": "Evil"},
    {"bit": BitFlag.F, "name": "Invis"},
    {"bit": BitFlag.G, "name": "Magic"},

    {"bit": BitFlag.H, "name": "No Drop"},
    {"bit": BitFlag.I, "name": "Bless"},
    {"bit": BitFlag.J, "name": "Anti-Good"},
    {"bit": BitFlag.K, "name": "Anti-Evil"},
    {"bit": BitFlag.L, "name": "Anti-Neutral"},
    {"bit": BitFlag.M, "name": "No Remove"},

    {"bit": BitFlag.N, "name": "Inventory"},
    {"bit": BitFlag.O, "name": "No Purge"},
    {"bit": BitFlag.P, "name": "Rot Death"},
    {"bit": BitFlag.Q, "name": "Vis Death"},
    {"bit": BitFlag.S, "name": "Non-metal"},

    {"bit": BitFlag.T, "name": "No Locate"},
    {"bit": BitFlag.U, "name": "Melt Drop"},
    {"bit": BitFlag.V, "name": "Had Timer"},
    {"bit": BitFlag.W, "name": "Sell Extract"},
    {"bit": BitFlag.X, "name": "Communicate"},
    {"bit": BitFlag.Y, "name": "Burn Proof"},
    {"bit": BitFlag.Z, "name": "No Remove Curse"},
    {"bit": BitFlag.aa, "name": "Quest"},
    {"bit": BitFlag.bb, "name": "Guild"},
    {"bit": BitFlag.cc, "name": "Blade Thirst"},
    {"bit": BitFlag.dd, "name": "Artifact"},
  ];

}
