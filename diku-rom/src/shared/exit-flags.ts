import {BitFlag, IBitFlag} from "./bit-flag";

export class ExitFlags {

  public static readonly exitFlags: Array<IBitFlag> = [
    {"bit": BitFlag.A, "name": "door"},
    {"bit": BitFlag.B, "name": "closed"},
    {"bit": BitFlag.C, "name": "locked"},
    {"bit": BitFlag.D, "name": "hidden"},
    {"bit": BitFlag.F, "name": "pickproof"},
    {"bit": BitFlag.G, "name": "no pass"},
    {"bit": BitFlag.H, "name": "easy"},
    {"bit": BitFlag.I, "name": "hard"},
    {"bit": BitFlag.J, "name": "infuriating"},
    {"bit": BitFlag.K, "name": "no close"},
    {"bit": BitFlag.L, "name": "no lock"}
  ];

}

