export class RoomSectors {

  public static readonly sectors =
    ["Inside", "City", "Field", "Forest", "Hills", "Mountain",
      "Swim", "No swim", "Unused", "Air", "Desert", "Jungle",
      "Underwater", "Marsh", "Swamp", "Tundra", "Rainforest", "Cavern"];

}

// Experimental.

const allForest = [
  "deep in the forest", "a clearing in the forest", "trees", "an abandoned camp in the forest",
  "edge of the forest", "a hill in the forest",
]

const types = [ "the forest", "in the forest", "trees"];
const type1 = ["deep in", "deep in", "deep in", "edge of",];
const type2 = ["", "", "", "a clearing", "a thicket", "an abandoned camp"];
const type3 = ["a thicket of", "sparse", "dense", "downed"]
const typeSet = [ type1, type2, type3];

function forestGen(): string {
  const type = Math.random() * types.length | 0;
  const subType = Math.random() * typeSet[type].length | 0;
  return typeSet[type][subType] + " " + types[type];
}

export { forestGen};
