import {BitFlag, IBitFlag} from "./bit-flag";

export class FormFlags {

  public static readonly formFlags: Array<IBitFlag> = [
    {"bit": BitFlag.A, "name": "Edible"},
    {"bit": BitFlag.B, "name": "Poison"},
    {"bit": BitFlag.C, "name": "Magical"},
    {"bit": BitFlag.D, "name": "Instant Decay"},
    {"bit": BitFlag.E, "name": "Other"},
    {"bit": BitFlag.G, "name": "Animal"},
    {"bit": BitFlag.H, "name": "Sentient"},
    {"bit": BitFlag.I, "name": "Undead"},
    {"bit": BitFlag.J, "name": "Construct"},
    {"bit": BitFlag.K, "name": "Mist"},
    {"bit": BitFlag.L, "name": "Intangible"},
    {"bit": BitFlag.M, "name": "Biped"},
    {"bit": BitFlag.N, "name": "Centaur"},
    {"bit": BitFlag.O, "name": "Insect"},
    {"bit": BitFlag.P, "name": "Spider"},
    {"bit": BitFlag.Q, "name": "Crustacean"},
    {"bit": BitFlag.R, "name": "Worm"},
    {"bit": BitFlag.S, "name": "Blob"},
    {"bit": BitFlag.V, "name": "Mammal"},
    {"bit": BitFlag.W, "name": "Bird"},
    {"bit": BitFlag.X, "name": "Reptile"},
    {"bit": BitFlag.Y, "name": "Snake"},
    {"bit": BitFlag.Z, "name": "Dragon"},
    {"bit": BitFlag.aa, "name": "Amphibian"},
    {"bit": BitFlag.bb, "name": "Fish"},
    {"bit": BitFlag.cc, "name": "Cold Blooded"}
  ];

}
