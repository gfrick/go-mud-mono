import {BitFlag, IBitFlag} from "./bit-flag";

export class OffensiveFlags {

  public static readonly offensiveFlags: Array<IBitFlag> = [
    {"bit": BitFlag.A, "name": "Area Attack"},
    {"bit": BitFlag.B, "name": "Backstab"},
    {"bit": BitFlag.C, "name": "Bash"},
    {"bit": BitFlag.D, "name": "Berserk"},
    {"bit": BitFlag.E, "name": "Disarm"},
    {"bit": BitFlag.F, "name": "Dodge"},
    {"bit": BitFlag.G, "name": "Fade"},
    {"bit": BitFlag.H, "name": "Fast"},
    {"bit": BitFlag.I, "name": "Kick"},
    {"bit": BitFlag.J, "name": "Kick Dirt"},
    {"bit": BitFlag.K, "name": "Parry"},
    {"bit": BitFlag.L, "name": "Rescue"},
    {"bit": BitFlag.M, "name": "Tail"},
    {"bit": BitFlag.N, "name": "Trip"},
    {"bit": BitFlag.O, "name": "Crush"}
  ];

}
