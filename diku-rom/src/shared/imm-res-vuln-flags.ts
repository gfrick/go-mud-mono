import {BitFlag, IBitFlag} from "./bit-flag";

export class ImmResVulnFlags {

  public static readonly immResVulnFlags: Array<IBitFlag> = [
    {"bit": BitFlag.A, "name": "Summon"},
    {"bit": BitFlag.B, "name": "Charm"},
    {"bit": BitFlag.C, "name": "Magic"},
    {"bit": BitFlag.D, "name": "Weapon"},
    {"bit": BitFlag.E, "name": "Bash"},
    {"bit": BitFlag.F, "name": "Pierce"},
    {"bit": BitFlag.G, "name": "Slash"},
    {"bit": BitFlag.H, "name": "Fire"},
    {"bit": BitFlag.I, "name": "Cold"},
    {"bit": BitFlag.J, "name": "Lightning"},
    {"bit": BitFlag.K, "name": "Acid"},
    {"bit": BitFlag.L, "name": "Poison"},
    {"bit": BitFlag.M, "name": "Negative"},
    {"bit": BitFlag.N, "name": "Holy"},
    {"bit": BitFlag.O, "name": "Energy"},
    {"bit": BitFlag.P, "name": "Mental"},
    {"bit": BitFlag.Q, "name": "Disease"},
    {"bit": BitFlag.R, "name": "Drowning"},
    {"bit": BitFlag.S, "name": "Light"},
    {"bit": BitFlag.T, "name": "Sound"}
  ];

}
