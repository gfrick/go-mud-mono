import {BitFlag, IBitFlag} from "./bit-flag";

export class ActFlags {

  public static readonly actFlags: Array<IBitFlag> = [
    {"bit": BitFlag.A, "name": "NPC"},
    {"bit": BitFlag.B, "name": "Sentinel"},
    {"bit": BitFlag.C, "name": "Scavenger"},
    {"bit": BitFlag.F, "name": "Aggressive"},
    {"bit": BitFlag.G, "name": "Stay Area"},
    {"bit": BitFlag.H, "name": "Wimpy"},
    {"bit": BitFlag.I, "name": "Pet"},
    {"bit": BitFlag.J, "name": "Train"},
    {"bit": BitFlag.K, "name": "Practice"},
    {"bit": BitFlag.Q, "name": "Cleric"},
    {"bit": BitFlag.R, "name": "Mage"},
    {"bit": BitFlag.S, "name": "Thief"},
    {"bit": BitFlag.T, "name": "Warrior"},
    {"bit": BitFlag.U, "name": "No Align"},
    {"bit": BitFlag.V, "name": "No Purge"},
    {"bit": BitFlag.W, "name": "Outdoors"},
    {"bit": BitFlag.Y, "name": "Indoors"},
    {"bit": BitFlag.aa, "name": "Healer"},
    {"bit": BitFlag.bb, "name": "Gain"},
    {"bit": BitFlag.cc, "name": "Update Always"},
    {"bit": BitFlag.dd, "name": "Changer"},
    {"bit": BitFlag.ee, "name": "Leader"}
  ];

}
