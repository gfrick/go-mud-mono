import {BitFlag, IBitFlag} from "./bit-flag";

export class AffectFlags {

  public static readonly affectedByFlags: Array<IBitFlag> = [
    {"bit": BitFlag.A, "name": "Blind"},
    {"bit": BitFlag.B, "name": "Invisible"},
    {"bit": BitFlag.C, "name": "Detect Evil"},
    {"bit": BitFlag.D, "name": "Detect Invis"},
    {"bit": BitFlag.E, "name": "Detect Magic"},
    {"bit": BitFlag.F, "name": "Detect Hidden"},
    {"bit": BitFlag.G, "name": "Detect Good"},
    {"bit": BitFlag.H, "name": "Sanctuary"},
    {"bit": BitFlag.I, "name": "Faerie Fire"},
    {"bit": BitFlag.J, "name": "Infrared"},
    {"bit": BitFlag.K, "name": "Curse"},
    {"bit": BitFlag.M, "name": "Poison"},
    {"bit": BitFlag.N, "name": "Protection Evil"},
    {"bit": BitFlag.O, "name": "Protection Good"},
    {"bit": BitFlag.P, "name": "Sneak"},
    {"bit": BitFlag.Q, "name": "Hide"},
    {"bit": BitFlag.R, "name": "Sleep"},
    {"bit": BitFlag.S, "name": "Charm"},
    {"bit": BitFlag.T, "name": "Flying"},
    {"bit": BitFlag.U, "name": "Pass Door"},
    {"bit": BitFlag.V, "name": "Haste"},
    {"bit": BitFlag.W, "name": "Calm"},
    {"bit": BitFlag.X, "name": "Plague"},
    {"bit": BitFlag.Y, "name": "Weaken"},
    {"bit": BitFlag.Z, "name": "Dark Vision"},
    {"bit": BitFlag.aa, "name": "Berserk"},
    {"bit": BitFlag.bb, "name": "Swim"},
    {"bit": BitFlag.cc, "name": "Regeneration"},
    {"bit": BitFlag.dd, "name": "Slow"}
  ];

}
