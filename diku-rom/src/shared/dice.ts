export class Dice {

  public diceCount: number;
  public diceFaces: number;
  public bonus: number;

  constructor(dice: number, sides: number, bonus: number) {
    this.diceCount = dice;
    this.diceFaces = sides;
    this.bonus = bonus;
  }

  static fromString(dice: string) {
    const split = dice.split(/[Dd]|\+/);
    return new Dice(+split[0], +split[1], +split[2]);
  }

}
