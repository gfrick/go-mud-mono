import {BitFlag} from "./bit-flag";

enum Direction {
  DIR_NORTH = 0,
  DIR_EAST = 1,
  DIR_SOUTH = 2,
  DIR_WEST = 3,
  DIR_UP = 4,
  DIR_DOWN = 5
}

const reverseDirection = [
  Direction.DIR_SOUTH,
  Direction.DIR_WEST,
  Direction.DIR_NORTH,
  Direction.DIR_EAST,
  Direction.DIR_DOWN,
  Direction.DIR_UP
]

enum ExitFlags {
  EXIT_ISDOOR = BitFlag.A,
  EXIT_CLOSED = BitFlag.B,
  EXIT_LOCKED = BitFlag.C,
  EXIT_PICKPROOF = BitFlag.F,
  EXIT_NOPASS = BitFlag.G,
  EXIT_EASY = BitFlag.H,
  EXIT_HARD = BitFlag.I,
  EXIT_INFURIATING = BitFlag.J,
  EXIT_NOCLOSE = BitFlag.K,
  EXIT_NOLOCK = BitFlag.L,
  EXIT_WALL = BitFlag.N,
  EXIT_NOWALL = BitFlag.O
}

export {Direction, reverseDirection, ExitFlags}
