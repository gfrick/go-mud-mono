import {BitFlag, IBitFlag} from "./bit-flag";

export class RoomFlags {

  public static readonly roomFlags: Array<IBitFlag> = [
    {"bit": BitFlag.A, "name": "dark"},
    {"bit": BitFlag.C, "name": "no_mob"},
    {"bit": BitFlag.D, "name": "indoors"},
    {"bit": BitFlag.J, "name": "private"},
    {"bit": BitFlag.K, "name": "safe"},
    {"bit": BitFlag.M, "name": "pet_shop"},
    {"bit": BitFlag.N, "name": "no_recall"},
    {"bit": BitFlag.O, "name": "underwater"},
    {"bit": BitFlag.P, "name": "gods_only"},
    {"bit": BitFlag.Q, "name": "death_t"},
    {"bit": BitFlag.R, "name": "newbies_only"},
    {"bit": BitFlag.S, "name": "law"},
    {"bit": BitFlag.T, "name": "nowhere"},
    {"bit": BitFlag.V, "name": "nomagic"},
    {"bit": BitFlag.W, "name": "teleport"},
    {"bit": BitFlag.X, "name": "fly_fall"},
    {"bit": BitFlag.Z, "name": "no_weapon"},
    {"bit": BitFlag.aa, "name": "no_gate_in"},
    {"bit": BitFlag.bb, "name": "no_gate_out"},
    {"bit": BitFlag.cc, "name": "nosummon"},
    {"bit": BitFlag.dd, "name": "slick"}
  ];

}
