import {BitFlag, IBitFlag} from "./bit-flag";

export class PartFlags {

  public static readonly partFlags: Array<IBitFlag> = [
    {"bit": BitFlag.A, "name": "Head"},
    {"bit": BitFlag.B, "name": "Arms"},
    {"bit": BitFlag.C, "name": "Legs"},
    {"bit": BitFlag.D, "name": "Heart"},
    {"bit": BitFlag.E, "name": "Brains"},
    {"bit": BitFlag.F, "name": "Guts"},
    {"bit": BitFlag.G, "name": "Hands"},
    {"bit": BitFlag.H, "name": "Feet"},
    {"bit": BitFlag.I, "name": "Fingers"},
    {"bit": BitFlag.J, "name": "Ear"},
    {"bit": BitFlag.K, "name": "Eye"},
    {"bit": BitFlag.L, "name": "Long Tongue"},
    {"bit": BitFlag.M, "name": "Eyestalks"},
    {"bit": BitFlag.N, "name": "Tentacles"},
    {"bit": BitFlag.O, "name": "Fins"},
    {"bit": BitFlag.P, "name": "Wings"},
    {"bit": BitFlag.Q, "name": "Tail"},
    {"bit": BitFlag.U, "name": "Claws"},
    {"bit": BitFlag.V, "name": "Fangs"},
    {"bit": BitFlag.W, "name": "Horns"},
    {"bit": BitFlag.X, "name": "Scales"},
    {"bit": BitFlag.Y, "name": "Tusks"}
  ];

}
