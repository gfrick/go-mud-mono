export enum BitFlag {
  A = 1,
  B = 2,
  C = 4,
  D = 8,
  E = 16,
  F = 32,
  G = 64,
  H = 128,
  I = 256,
  J = 512,
  K = 1024,
  L = 2048,
  M = 4096,
  N = 8192,
  O = 16384,
  P = 32768,
  Q = 65536,
  R = 131072,
  S = 262144,
  T = 524288,
  U = 1048576,
  V = 2097152,
  W = 4194304,
  X = 8388608,
  Y = 16777216,
  Z = 33554432,
  aa = 67108864,
  bb = 134217728,
  cc = 268435456,
  dd = 536870912,
  ee = 1073741824
}

export interface IBitFlag {
  bit: number;
  name: string;
}

export class BitFlags {
  public static hasFlag(value: number, flag: number) {
    return (value & flag) === flag;
  }

  public static addFlag(value: number, flag: number): number {
    return (value | flag);
  }

  public static removeFlag(value: number, flag: number): number {
    return ~(value & flag);
  }
}

export const getBitString = (bit: number): string => {
  if (bit === 0) {
    return '0';
  }
  console.log(bit);

  let builder = '';
  if ((bit & BitFlag.A) === BitFlag.A) {
    builder += ('A');
  }
  if ((bit & BitFlag.B) === BitFlag.B) {
    builder += ('B');
  }
  if ((bit & BitFlag.C) === BitFlag.C) {
    builder += ('C');
  }
  if ((bit & BitFlag.D) === BitFlag.D) {
    builder += ('D');
  }
  if ((bit & BitFlag.E) === BitFlag.E) {
    builder += ('E');
  }
  if ((bit & BitFlag.F) === BitFlag.F) {
    builder += ('F');
  }
  if ((bit & BitFlag.G) === BitFlag.G) {
    builder += ('G');
  }
  if ((bit & BitFlag.H) === BitFlag.H) {
    builder += ('H');
  }
  if ((bit & BitFlag.I) === BitFlag.I) {
    builder += ('I');
  }
  if ((bit & BitFlag.J) === BitFlag.J) {
    builder += ('J');
  }
  if ((bit & BitFlag.K) === BitFlag.K) {
    builder += ('K');
  }
  if ((bit & BitFlag.L) === BitFlag.L) {
    builder += ('L');
  }
  if ((bit & BitFlag.M) === BitFlag.M) {
    builder += ('M');
  }
  if ((bit & BitFlag.N) === BitFlag.N) {
    builder += ('N');
  }
  if ((bit & BitFlag.O) === BitFlag.O) {
    builder += ('O');
  }
  if ((bit & BitFlag.P) === BitFlag.P) {
    builder += ('P');
  }
  if ((bit & BitFlag.Q) === BitFlag.Q) {
    builder += ('Q');
  }
  if ((bit & BitFlag.R) === BitFlag.R) {
    builder += ('R');
  }
  if ((bit & BitFlag.S) === BitFlag.S) {
    builder += ('S');
  }
  if ((bit & BitFlag.T) === BitFlag.T) {
    builder += ('T');
  }
  if ((bit & BitFlag.U) === BitFlag.U) {
    builder += ('U');
  }
  if ((bit & BitFlag.V) === BitFlag.V) {
    builder += ('V');
  }
  if ((bit & BitFlag.W) === BitFlag.W) {
    builder += ('W');
  }
  if ((bit & BitFlag.X) === BitFlag.X) {
    builder += ('X');
  }
  if ((bit & BitFlag.Y) === BitFlag.Y) {
    builder += ('Y');
  }
  if ((bit & BitFlag.Z) === BitFlag.Z) {
    builder += ('Z');
  }
  if ((bit & BitFlag.aa) === BitFlag.aa) {
    builder += ('a');
  }
  if ((bit & BitFlag.bb) === BitFlag.bb) {
    builder += ('b');
  }
  if ((bit & BitFlag.cc) === BitFlag.cc) {
    builder += ('c');
  }
  if ((bit & BitFlag.dd) === BitFlag.dd) {
    builder += ('d');
  }
  if ((bit & BitFlag.ee) === BitFlag.ee) {
    builder += ('e');
  }

  return builder;
}

export const getBitInt = (bit: string): number => {
  let value = 0;
  for (let a = 0; a < bit.length; a++) {
    switch (bit[a]) {
      case 'A': {
        value += BitFlag.A;
        break;
      }
      case 'B': {
        value += BitFlag.B;
        break;
      }
      case 'C': {
        value += BitFlag.C;
        break;
      }
      case 'D': {
        value += BitFlag.D;
        break;
      }
      case 'E': {
        value += BitFlag.E;
        break;
      }
      case 'F': {
        value += BitFlag.F;
        break;
      }
      case 'G': {
        value += BitFlag.G;
        break;
      }
      case 'H': {
        value += BitFlag.H;
        break;
      }
      case 'I': {
        value += BitFlag.I;
        break;
      }
      case 'J': {
        value += BitFlag.J;
        break;
      }
      case 'K': {
        value += BitFlag.K;
        break;
      }
      case 'L': {
        value += BitFlag.L;
        break;
      }
      case 'M': {
        value += BitFlag.M;
        break;
      }
      case 'N': {
        value += BitFlag.N;
        break;
      }
      case 'O': {
        value += BitFlag.O;
        break;
      }
      case 'P': {
        value += BitFlag.P;
        break;
      }
      case 'Q': {
        value += BitFlag.Q;
        break;
      }
      case 'R': {
        value += BitFlag.R;
        break;
      }
      case 'S': {
        value += BitFlag.S;
        break;
      }
      case 'T': {
        value += BitFlag.T;
        break;
      }
      case 'U': {
        value += BitFlag.U;
        break;
      }
      case 'V': {
        value += BitFlag.V;
        break;
      }
      case 'W': {
        value += BitFlag.W;
        break;
      }
      case 'X': {
        value += BitFlag.X;
        break;
      }
      case 'Y': {
        value += BitFlag.Y;
        break;
      }
      case 'Z': {
        value += BitFlag.Z;
        break;
      }
      case 'a': {
        a++;
        value += BitFlag.aa;
        break;
      }
      case 'b': {
        a++;
        value += BitFlag.bb;
        break;
      }
      case 'c': {
        a++;
        value += BitFlag.cc;
        break;
      }
      case 'd': {
        a++;
        value += BitFlag.dd;
        break;
      }
      case 'e': {
        a++;
        value += BitFlag.ee;
        break;
      }
      case '': {
        break;
      }
      case '0':
      case '\n':
      case '\r':
      case '\'':
      case '\"':
        break;
      default: {
        console.log('found other in getbitint: [' + bit[a] + '] ');
        return +bit;
      }
    }
  }

  return value;
}
