import {BitFlag, IBitFlag} from "./bit-flag";

export class WearFlags {

  public static readonly wearFlags: Array<IBitFlag> = [
    {'bit': BitFlag.A, 'name': 'Take'},
    {'bit': BitFlag.B, 'name': 'Finger'},
    {'bit': BitFlag.C, 'name': 'Neck'},
    {'bit': BitFlag.D, 'name': 'Body'},
    {'bit': BitFlag.E, 'name': 'Head'},
    {'bit': BitFlag.F, 'name': 'Legs'},
    {'bit': BitFlag.G, 'name': 'Feet'},
    {'bit': BitFlag.H, 'name': 'Hands'},
    {'bit': BitFlag.I, 'name': 'Arms'},
    {'bit': BitFlag.J, 'name': 'Shield'},
    {'bit': BitFlag.K, 'name': 'About'},
    {'bit': BitFlag.L, 'name': 'Waist'},
    {'bit': BitFlag.M, 'name': 'Wrist'},
    {'bit': BitFlag.N, 'name': 'Wield'},
    {'bit': BitFlag.O, 'name': 'Hold'},
    {'bit': BitFlag.P, 'name': 'No Sacrifice'},
    {'bit': BitFlag.Q, 'name': 'Float'},
    {'bit': BitFlag.R, 'name': 'Ears'}
  ];

}
