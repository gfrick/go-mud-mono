import {RomArea} from './rom-area';
import { BitFlags, getBitInt} from '../shared/bit-flag';
import {RomObjType} from './rom-obj-type';
import {RomCondition} from './rom-condition';
import {RomObjectAffect} from './rom-object-affect';
import {RomAreaHeader} from './rom-header';
import {Mobile} from './rom-mobile';
import {RomRoom} from './rom-room';
import {RomReset} from './rom-reset';
import {RomObject} from './rom-object';
import {RomExit} from './rom-exit';
import {Dice} from '../shared/dice';
import {RomMobTrigger} from './rom-mob-trigger';
import {RaceService} from './race.service';
import {SexLookup} from "./rom-sex";
import {SizeLookup} from "./rom-size";
import {PositionLookup} from "./rom-position";

export class RomAreaParser {

  private raceService: RaceService = new RaceService();

  public readRawData(rawData: any, fileName: string): RomArea {

    // 1. Split the rom area file into single lines.
    const lines = rawData.split('\n');

    // 2. Split the individual lines into groups of lines.
    const splitData = {
      header: this.getLineSet(lines, '#AREADATA', 'End'),
      rooms: this.getLineSet(lines, '#ROOMS', '#0'),
      objs: this.getLineSet(lines, '#OBJECTS', '#0'),
      mobs: this.getLineSet(lines, '#MOBILES', '#0'),
      specials: this.getLineSet(lines, '#SPECIALS', 'S'),
      resets: this.getLineSet(lines, '#RESETS', 'S'),
      shops: this.getLineSet(lines, '#SHOPS', '0'),
      mobprogs: this.getLineSet(lines, '#MOBPROGS', '0')
    };

    // 3. Build objects from each portion.
    const area = new RomArea();

    area.header = this.createAreaHeader(splitData.header);
    area.rooms = this.createAreaRooms(splitData.rooms);
    area.mobs = this.createAreaMobs(splitData.mobs);
    area.objects = this.createAreaObjects(splitData.objs);
    area.resets = this.createAreaResets(splitData.resets);
    area.header.fileName = fileName;
    return area;
  }

  /**
   * Give a complete set of ROM area data broken into an array, find
   * a sub-array representing a set of data such as "Rooms"
   * @param begin #AREADATA
   * @param end End
   * @returns {T[]}
   */
  private getLineSet(lines: Array<string>, begin: string, end: String): Array<string> {

    let beginIndex = -1;
    let endIndex = -1;
    for (let index = 0; index < lines.length; index++) {
      if (lines[index].trim().toLowerCase() === begin.toLowerCase()) {
        beginIndex = index;
      }
      if (beginIndex > -1 && lines[index].trim().toLowerCase() === end.toLowerCase()) {
        endIndex = index;
        break;
      }
    }
    if (endIndex === -1) {
      console.log('didn\'t find ' + begin + ' / ' + end);
    }
    return lines.splice(beginIndex, endIndex - beginIndex + 1);
  }

  /**
   * Parse out the header rows, creating the header object.
   * @param header
   * @returns {RomAreaHeader}
   */
  private createAreaHeader(header: Array<string>): RomAreaHeader {
    const newHeader = new RomAreaHeader();
    const headerSet: any = {};
    header.forEach((eachRow) => {
      const splitData = eachRow.split(' ');
      headerSet[splitData[0].toLowerCase()] = splitData.splice(1);
    });
    newHeader.builder = headerSet['builders'][0].replace(/~/, '');
    newHeader.security = +headerSet['security'][0];
    if (headerSet['flags']) {
      newHeader.flags = +headerSet['flags'][0];
    }
    newHeader.lowVnum = +headerSet['vnums'][0];
    newHeader.highVnum = +headerSet['vnums'][1];
    newHeader.areaName = headerSet['name'].join(' ').replace(/~/, '');
    return newHeader;
  }

  private createAreaRooms(roomData: Array<string>): RomRoom[] {
    const rooms: RomRoom[] = [];
    const roomMultiArray = this.createVnumDistinguishedArrays(roomData);
    roomMultiArray.forEach((roomSet) => {
      rooms.push(this.createRoomFromArray(roomSet));
    });
    return rooms;
  }

  private createRoomFromArray(roomSet: Array<string>): RomRoom {
    const vnum = +roomSet.splice(0, 1)[0].substring(1);
    const room = new RomRoom(vnum);

    // 1. Basics
    room.name = this.readArrayToString(roomSet, '~');
    room.description = this.readArrayToString(roomSet, '~');
    const flagsArray = roomSet.splice(0, 1)[0].split(' '); // 0, flags, sector
    room.flags = +flagsArray[1];
    room.sector = +flagsArray[2];

    let key = roomSet[0][0].toUpperCase();
    while (key !== 'S') {
      if (key === 'E') {
        const keyword = this.readArrayToString(roomSet, '~');
        const desc = this.readArrayToString(roomSet, '~');
        room.extraDescriptions[keyword] = desc;
      } else if (key === 'H') {
        roomSet.splice(0, 1);
      } else if (key === 'D') {
        const direction = parseInt(roomSet.splice(0, 1)[0][1], 10);
        const desc = this.readArrayToString(roomSet, '~');
        const name = this.readArrayToString(roomSet, '~');
        const keySet = roomSet.splice(0, 1)[0].split(' '); // lock, key, tovnum
        const exit = new RomExit();
        exit.description = desc;
        exit.toVnum = +keySet[2];
        exit.myRoom = room;
        exit.keyword = name;
        exit.keyVnum = +keySet[1];
        exit.direction = direction;
        exit.setFlagsByKey(+keySet[0]);
        room.setExit(exit);
      } else if (key === 'M') {
        const healing = roomSet.splice(0, 1)[0].split(' ');
        room.heal = +healing[1];
        room.mana = +healing[3];
      } else {
        console.log('Unknown room optional argument: ' + key);
        console.log('Expected one of S, E, H, D, M');
      }
      key = roomSet[0][0].toUpperCase();
    }
    // 2. Specials = E, H, M, D, S (done)
    return room;
  }

  private createAreaMobs(mobData: Array<string>): Mobile[] {
    const mobiles: Mobile[] = [];
    const mobMultiArray = this.createVnumDistinguishedArrays(mobData);
    mobMultiArray.forEach((roomSet) => {
      mobiles.push(this.createMobileFromArray(roomSet));
    });
    return mobiles;
  }

  private createMobileFromArray(mobSet: Array<string>): Mobile {
    const vnum = +mobSet.splice(0, 1)[0].substring(1);
    const mobile = new Mobile(vnum);
    mobile.name = this.readArrayToString(mobSet, '~');
    mobile.shortDesc = this.readArrayToString(mobSet, '~');
    mobile.longDesc = this.readArrayToString(mobSet, '~');
    mobile.description = this.readArrayToString(mobSet, '~');
    mobile.race = this.raceService.getRace(this.readArrayToString(mobSet, '~'));

    let combinedRow = mobSet.splice(0, 1)[0].split(' ');
    // @TODO include race flags.
    mobile.actFlags = getBitInt(combinedRow[0]);
    mobile.affectedBy = getBitInt(combinedRow[1]);
    mobile.alignment = +combinedRow[2];
    mobile.group = +combinedRow[3];

    combinedRow = mobSet.splice(0, 1)[0].split(' ');
    mobile.level = +combinedRow[0];
    mobile.hitRoll = +combinedRow[1];
    mobile.hitDice = Dice.fromString(combinedRow[2]);
    mobile.manaDice = Dice.fromString(combinedRow[3]);
    mobile.damageDice = Dice.fromString(combinedRow[4]);
    mobile.damageType = combinedRow[5].trim();

    combinedRow = mobSet.splice(0, 1)[0].split(' ');
    mobile.armor = [+combinedRow[0], +combinedRow[1], +combinedRow[2], +combinedRow[3]];

    combinedRow = mobSet.splice(0, 1)[0].split(' ');
    // @TODO include race flags.
    mobile.offensiveFlags = getBitInt(combinedRow[0]);
    mobile.immunityFlags = getBitInt(combinedRow[1]);
    mobile.resistanceFlags = getBitInt(combinedRow[2]);
    mobile.vulnerabilityFlags = getBitInt(combinedRow[3]);

    combinedRow = mobSet.splice(0, 1)[0].split(' ');
    mobile.startPosition = PositionLookup(combinedRow[0]);
    mobile.defaultPosition = PositionLookup(combinedRow[1]);
    mobile.sex = SexLookup(combinedRow[2]);
    mobile.wealth = +combinedRow[3];

    combinedRow = mobSet.splice(0, 1)[0].split(' ');
    // @TODO include race flags
    mobile.form = getBitInt(combinedRow[0]);
    mobile.parts = getBitInt(combinedRow[1]);
    mobile.size = SizeLookup(combinedRow[2]);
    mobile.material = combinedRow[3];

    while (mobSet.length > 0) {
      this.readMobileExtra(mobile, mobSet.splice(0, 1)[0]);
    }
    return mobile;
  }

  private readMobileExtra(mobile: Mobile, extra: string) {

    if (extra[0] === 'D') {
      let deathcry = extra.substring(2);
      deathcry = deathcry.replace(/~/, '');
      if (deathcry !== '(null)' && deathcry !== 'default') {
        mobile.deathCry = deathcry;
      }
    } else if (extra[0] === 'M') {
      const trigger = extra.replace(/~/, '');
      const triggerArgs = trigger.split(' ');
      mobile.triggers.push(new RomMobTrigger(triggerArgs[0], +triggerArgs[1], triggerArgs[2]));
    } else if (extra[0] === 'F') {
      const args = extra.split(' ');
      const tType = args[1].toLowerCase();
      const removeBits = getBitInt(args[2]);
      let newBits = 0;
      if (tType === 'act') {
        newBits = BitFlags.removeFlag(mobile.actFlags, removeBits);
        mobile.actFlags = (newBits);
      } else if (tType === ('aff')) {
        newBits = BitFlags.removeFlag(mobile.affectedBy, removeBits);
        mobile.affectedBy = (newBits);
      } else if (tType === ('off')) {
        newBits = BitFlags.removeFlag(mobile.offensiveFlags, removeBits);
        mobile.offensiveFlags = (newBits);
      } else if (tType === ('imm')) {
        newBits = BitFlags.removeFlag(mobile.immunityFlags, removeBits);
        mobile.immunityFlags = (newBits);
      } else if (tType === ('res')) {
        newBits = BitFlags.removeFlag(mobile.resistanceFlags, removeBits);
        mobile.resistanceFlags = (newBits);
      } else if (tType === ('vul')) {
        newBits = BitFlags.removeFlag(mobile.vulnerabilityFlags, removeBits);
        mobile.vulnerabilityFlags = (newBits);
      } else if (tType === ('for')) {
        newBits = BitFlags.removeFlag(mobile.form, removeBits);
        mobile.form = (newBits);
      } else if (tType === ('par')) {
        newBits = BitFlags.removeFlag(mobile.parts, removeBits);
        mobile.parts = (newBits);
      } else {
        console.log('F ? ?');
      }
    } else {
      console.log(mobile.vnum + ': unknown extra: ' + extra);
    }
  }

  private createAreaObjects(objData: Array<string>): RomObject[] {
    const mudObjects: RomObject[] = [];
    const objMultiArray = this.createVnumDistinguishedArrays(objData);
    objMultiArray.forEach((roomSet) => {
      mudObjects.push(this.createMudObjectFromArray(roomSet));
    });
    return mudObjects;
  }

  private createMudObjectFromArray(objSet: any): RomObject {
    const vnum = +objSet.splice(0, 1)[0].substring(1);
    const obj = new RomObject(vnum);
    obj.name = this.readArrayToString(objSet, '~');
    obj.shortDesc = this.readArrayToString(objSet, '~');
    obj.longDesc = this.readArrayToString(objSet, '~');
    obj.material = this.readArrayToString(objSet, '~');

    let combinedRow = objSet.splice(0, 1)[0].split(' ');
    obj.type = RomObjType.typeFromString(combinedRow[0]);
    obj.extraFlags = getBitInt(combinedRow[1]);
    obj.wearFlags = getBitInt(combinedRow[2]);

    combinedRow = objSet.splice(0, 1)[0].split(' ');
    this.assignObjFieldsByType(obj, combinedRow);

    combinedRow = objSet.splice(0, 1)[0].split(' ');
    obj.level = +combinedRow[0];
    obj.weight = +combinedRow[1];
    obj.cost = +combinedRow[2];
    obj.condition = RomCondition.conditionLookup(combinedRow[3]);

    while (objSet.length > 0) {
      combinedRow = objSet.splice(0, 1)[0];
      const command = combinedRow[0];
      switch (command) {
        case 'A': {
          const affectRow = objSet.splice(0, 1)[0].split(' ');

          try {

            const nAffect = new RomObjectAffect(+affectRow[0], +affectRow[1]);
            obj.affects.push(nAffect);
          } catch (exception) {
            console.log('Exception creating object affect in loader');
          }
          break;
        }
        case 'R':  // wear
        {
          obj.wearMessage = this.readArrayToString(objSet, '~');
          break;
        }
        case 'M':  // remove
        {
          obj.removeMessage = this.readArrayToString(objSet, '~');
          break;
        }
        case 'E':  // extra desc
        {
          const dKeyword = this.readArrayToString(objSet, '~');
          const dDesc = this.readArrayToString(objSet, '~');
          obj.extraDescriptions[dKeyword] = dDesc;
          break;
        }
        case 'F': // affects on player?
        {
          console.log('Unsupported object affect: ' + command);
          break;
        }
        default: {
          console.log('Extra line: ' + command);
          break;
        }
      }
    }
    return obj;
  }

  private assignObjFieldsByType(obj: RomObject, combinedRow: Array<string>): void {
    switch (obj.type) {
      case RomObjType.ITEM_WEAPON: {
        obj.values[0] = combinedRow[0];
        obj.values[1] = +combinedRow[1];
        obj.values[2] = +combinedRow[2];
        obj.values[3] = combinedRow[3];
        obj.values[4] = getBitInt(combinedRow[4]);
        break;
      }
      case RomObjType.ITEM_CONTAINER: {
        obj.values[0] = +combinedRow[0];
        obj.values[1] = getBitInt(combinedRow[1]);
        obj.values[2] = +combinedRow[2];
        obj.values[3] = +combinedRow[3];
        obj.values[4] = +combinedRow[4];
        break;
      }
      case RomObjType.ITEM_DRINK_CON:
      case RomObjType.ITEM_FOUNTAIN: {
        obj.values[0] = +combinedRow[0];
        obj.values[1] = +combinedRow[1];
        obj.values[2] = combinedRow[2];
        obj.values[3] = +combinedRow[3];
        // v4 unused.
        break;
      }
      case RomObjType.ITEM_WAND:
      case RomObjType.ITEM_STAFF: {
        obj.values[0] = +combinedRow[0];
        obj.values[1] = +combinedRow[1];
        obj.values[2] = +combinedRow[2];
        obj.values[3] = combinedRow[3];
        // v4 unused
        break;
      }
      case RomObjType.ITEM_POTION:
      case RomObjType.ITEM_PILL:
      case RomObjType.ITEM_SCROLL: {
        obj.values[0] = +combinedRow[0];
        obj.values[1] = combinedRow[1];
        obj.values[2] = combinedRow[2];
        obj.values[3] = combinedRow[3];
        obj.values[4] = combinedRow[4];
        break;
      }
      default:
      case RomObjType.ITEM_MONEY:
      case RomObjType.ITEM_LIGHT:
      case RomObjType.ITEM_ARMOR: {
        obj.values[0] = +combinedRow[0];
        obj.values[1] = +combinedRow[1];
        obj.values[2] = +combinedRow[2];
        obj.values[3] = +combinedRow[3];
        obj.values[4] = +combinedRow[4];
        break;
      }
      case RomObjType.ITEM_PORTAL: {
        obj.values[0] = +combinedRow[0];
        obj.values[1] = getBitInt(combinedRow[1]);
        obj.values[2] = getBitInt(combinedRow[2]);
        obj.values[3] = +combinedRow[3];
        obj.values[4] = +combinedRow[4];
        break;
      }
      case RomObjType.ITEM_FOOD:
      case RomObjType.ITEM_FURNITURE: {
        obj.values[0] = +combinedRow[0];
        obj.values[1] = +combinedRow[1];
        obj.values[2] = getBitInt(combinedRow[2]);
        obj.values[3] = +combinedRow[3];
        obj.values[4] = +combinedRow[4];
        break;
      }
    }
  }

  private createAreaResets(resetData: Array<string>) {
    const resets = [];
    resetData.splice(0, 1); // remove "#RESETS";
    let reset = resetData.splice(0, 1)[0].split(' ');
    let command;
    let arg1, arg2, arg3, arg4, chance;
    while (reset[0] !== 'S') {
      command = reset[0];
      if (command[0] !== '*') {
        chance = +reset[1];
        arg1 = +reset[2];
        arg2 = +reset[3];
        if (command[0] !== 'G' && command[0] !== 'R') {
          arg3 = +reset[4];
        }

        if (command[0] === 'P' || command[0] === 'M') {
          arg4 = +reset[5];
        }
        resets.push(new RomReset(command, arg1, arg2, arg3, arg4, chance));
      }
      if (resetData.length === 0) {
        break;
      }
      reset = resetData.splice(0, 1)[0].split(' ');
    }

    return resets;
  }

  private readArrayToString(roomSet: Array<string>, match: string): string {

    let arrayCount = 0;
    while (arrayCount < roomSet.length && roomSet[arrayCount].indexOf(match) === -1) {
      arrayCount++;
    }
    const stringSet = roomSet.splice(0, arrayCount + 1);
    const stringJoined = (stringSet.length === 1 ? stringSet[0] : stringSet.join('\n'));
    return stringJoined.replace(/~/, '').trim();
  }

  private createVnumDistinguishedArrays(roomData: Array<string>) {
    roomData.splice(0, 1); // remove "#ROOMS"
    let currentRoom: any = [];
    currentRoom.push(roomData.splice(0, 1)[0]); // put first vnum in first array.
    const allRooms = [];
    roomData.forEach((dataRow) => {
      // If the row is nothing but a vnum... start a new set.
      if (dataRow.match(/#[0-9]+/)) {
        allRooms.push(currentRoom);
        currentRoom = [];
      }
      currentRoom.push(dataRow);
    });
    allRooms.push(currentRoom);
    allRooms.splice(allRooms.length - 1); // "#0" is not a room.
    return allRooms;
  }

}
