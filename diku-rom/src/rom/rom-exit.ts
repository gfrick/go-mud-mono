import {RomRoom} from './rom-room';
import {ExitFlags} from "../shared/direction";

export class RomExit {

  constructor(jsonData?: any) {
    this.toVnum = 0;
    this.keyVnum = 0;
    this.description = "";
    this.keyword = "";
    this.exitFlags = 0;
    this.direction = 0;
    if (jsonData) {
      Object.assign(this, jsonData);
    }
  }

  public myRoom?: RomRoom;
  public toVnum: number;
  public keyVnum: number;
  public description: string;
  public keyword: string;
  public exitFlags: number;
  public direction: number;

  public setFlagsByKey(key: number): void {
    switch (key) {
      case 0:
      default: {
        this.exitFlags = 0;
        return;
      }
      case 1: {
        this.exitFlags = ExitFlags.EXIT_ISDOOR;
        return;
      }
      case 2: {
        this.exitFlags = ExitFlags.EXIT_ISDOOR | ExitFlags.EXIT_PICKPROOF;
        return;
      }
      case 3: {
        this.exitFlags = ExitFlags.EXIT_ISDOOR | ExitFlags.EXIT_NOPASS;
        return;
      }
      case 4: {
        this.exitFlags = ExitFlags.EXIT_ISDOOR | ExitFlags.EXIT_PICKPROOF | ExitFlags.EXIT_NOPASS;
        return;
      }
    }
  }

  public getJSON(): any {
    const thisExit: any = Object.assign({}, this);
    delete thisExit.myRoom;
    return thisExit;
  }
}
