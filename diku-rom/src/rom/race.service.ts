import {RomRace} from './rom-race';

export class RaceService {

  private rawRaceData = [
    'human~ 1 walks 0 0 0 0 0 0 0 2101377 2047 2 punch',
    'elf~ 1 walks 0 512 0 0 0 2 33554432 2101377 2047 2 punch',
    'dwarf~ 1 walks 0 512 0 0 0 67584 131072 2101377 2047 2 punch',
    'giant~ 1 stomps 0 0 0 0 0 384 33280 2101377 2047 3 crush',
    'demon~ 1 flies 0 1048576 0 0 0 0 17920 2101377 2047 2 drain',
    'skeleton~ 1 walks 0 512 0 0 100482 0 1296 256 455 2 slash',
    'half-golem~ 1 walks 0 512 0 0 128 2 33554464 2101377 2047 2 pound',
    'bat~ 0 flies 0 34078720 0 160 0 0 262144 2097217 34493 0 bite',
    'bear~ 0 walks 0 0 0 16408 0 272 0 2097217 3147455 2 charge',
    'biohuman~ 0 walks 0 0 0 8 0 2 0 2097217 2047 2 punch',
    'cat~ 0 walks 0 33554432 0 160 0 0 0 2097217 3212989 0 scratch',
    'centipede~ 0 slithers 0 33554432 0 0 0 288 16 16451 1029 0 bite',
    'dog~ 0 walks 0 0 0 128 0 0 0 2097217 3147453 0 bite',
    'doll~ 0 floats 0 0 0 0 243968 262160 18112 268440080 1223 0 drain',
    'dragon~ 0 soars 0 524800 0 0 0 146 288 33554561 11634685 3 slash',
    'fido~ 0 walks 0 0 0 131104 0 0 4 2097219 2164413 1 bite',
    'fox~ 0 walks 0 33554432 0 160 0 0 0 2097217 2164413 1 bite',
    'goblin~ 0 walks 0 512 0 0 0 65536 4 2101377 2047 1 punch',
    'hobgoblin~ 0 walks 0 512 0 0 0 67584 0 2101377 16779263 2 punch',
    'kobold~ 0 walks 0 512 0 0 0 2048 4 2101379 67583 1 punch',
    'lizard~ 0 slithers 0 0 0 0 0 2048 256 276824129 2163901 2 bite',
    'modron~ 0 walks 0 512 0 196608 0 110594 1408 128 1735 2 punch',
    'orc~ 0 walks 0 512 0 0 0 65536 262144 2101377 2047 2 punch',
    'pig~ 0 walks 0 0 0 0 0 0 0 2097217 1725 0 charge',
    'rabbit~ 0 hops 0 0 0 160 0 0 0 2097217 1725 0 bite',
    'school monster~ 0 walks 1048576 0 0 0 3 0 4 2101249 1115839 2 punch',
    'snake~ 0 slithers 0 0 0 0 0 2048 256 293601345 10554425 0 slash',
    'song bird~ 0 soars 0 524288 0 160 0 0 0 4194369 33981 0 peck',
    'troll~ 0 walks 0 268436000 0 8 0 18 1152 2101379 3147775 3 slash',
    'water fowl~ 0 flies 0 134742016 0 0 0 131072 0 4194369 33981 0 peck',
    'wolf~ 0 walks 0 33554432 0 160 0 0 0 2097217 2164285 1 bite',
    'wyvern~ 0 flies 0 524328 0 164 2048 0 262144 33554499 10553021 3 slash',
    'bugrom~ 0 marches 0 0 0 0 0 0 0 2097217 2163901 3 punch'
  ];
  private readonly raceSet: RomRace[];

  constructor() {
    this.raceSet = [];
    let raceFields = null;
    let newRace: RomRace;
    this.rawRaceData.forEach((eachRace) => {
      let nameFieldsString = eachRace.split("~");
      raceFields = nameFieldsString[1].split(' ');
      // raceFields[0].replace(/~/, '')
      newRace = new RomRace(nameFieldsString[0]);
      newRace.isPcRace = (raceFields[1] === '1');
      newRace.movementMsg = raceFields[2];
      newRace.actFlags = +raceFields[3];
      newRace.affectedByFlags = +raceFields[4];
      newRace.affectedBy2Flags = +raceFields[5];
      newRace.offensiveFlags = +raceFields[6];
      newRace.immunityFlags = +raceFields[7];
      newRace.resistanceFlags = +raceFields[8];
      newRace.vulnerableFlags = +raceFields[9];
      newRace.formFlags = +raceFields[10];
      newRace.partsFlags = +raceFields[11];
      newRace.defaultSize = +raceFields[12];
      newRace.defaultDamage = raceFields[13];
      if( ['a','e','i','o','u'].indexOf(newRace.name[0]) >= 0) {
        newRace.prefix = "an";
      }
      this.raceSet.push(newRace);
    });
  }

  public getRaces(): RomRace[] {
    return this.raceSet;
  }

  public getRace(raceName: string): RomRace {
    let race = null;
    this.raceSet.forEach((eachRace) => {
      if (eachRace.name === raceName) {
        race = eachRace;
      }
    });
    return race || this.raceSet[1]; // default to human.
  }
}
