import {RomObjectAffect} from './rom-object-affect';
import {RomObjType} from "./rom-obj-type";

export class RomObject {
  public name: string;
  public shortDesc: string;
  public longDesc: string;
  public description: string;
  public vnum: number;
  public level: number;
  public affectedBy: number;
  public affectedBy2: number;
  public alignment: number;
  public material: string;
  public armor: Array<number>;
  public extraDescriptions: any = {};

  public condition: number;
  public cost: number;
  public removeMessage: string;
  public wearMessage: string;
  public casts: number;
  public weight: number;
  public wearFlags: number;
  public extraFlags: number;
  public type: number;
  public values: Array<any>;
  public affects: Array<RomObjectAffect>;
  public contents: Array<RomObject>;

  constructor(vnum: number, jsonData?: any) {
    this.vnum = vnum;
    this.name = "New Object";
    this.values = [];
    this.affects = [];
    this.contents = [];
    this.shortDesc = '';
    this.longDesc = '';
    this.description = '';
    this.level = 1;
    this.affectedBy = 0;
    this.affectedBy2 = 0;
    this.alignment = 0;
    this.material = '';
    this.armor = [0, 0, 0, 0];
    this.condition = 100;
    this.cost = 0;
    this.removeMessage = "";
    this.wearMessage = "";
    this.casts = 0;
    this.weight = 1;
    this.wearFlags = 0;
    this.extraFlags = 0;
    this.type = RomObjType.ITEM_TRASH;
    if (jsonData) {
      Object.assign(this, jsonData);
    }
  }

  public getJSON() {
    const newObj: any = Object.assign({}, this);
    delete newObj.contents;
    delete newObj.affects;
    // force the value map to be strings, because we need an array of same types
    if (newObj.values && newObj.values.length) {
      newObj.values = newObj.values.map( (x:any) => x + '');
    }
    return newObj;
  }

}
