enum Position {
    Dead,
    Mortal,
    Incapacitated,
    Stunned,
    Sleeping,
    Resting,
    Sitting,
    Fighting,
    Standing
}

const positionNames = [
    "dead", "mortally wounded", "incapacitated", "stunned", "sleeping", "resting", "sitting", "fighting", "standing"
];

function PositionLookup(position: string) {
    const posLookup = position.toLowerCase().trim();
    switch (posLookup) {
        case "dead":
            return Position.Dead;
        case "mortally wounded":
            return Position.Mortal;
        case "incapacitated":
            return Position.Incapacitated;
        case "stunned":
            return Position.Stunned;
        case "sleeping":
            return Position.Sleeping;
        case "resting":
            return Position.Resting;
        case "sitting":
            return Position.Sitting;
        case "fighting":
            return Position.Fighting;
        case "standing":
            return Position.Standing;
        default:
            return Position.Standing;
    }
}

function PositionDescription(position: Position): string {
    return positionNames[position];
}

export {Position, PositionLookup, PositionDescription}
