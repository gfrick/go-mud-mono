import {BitFlag, BitFlags} from "../shared/bit-flag";

export class RomDamageType {
  public static readonly damageTypeSet = [
    "none",
    "beating",
    "bite",
    "blast",
    "charge",
    "chill",
    "chop",
    "claw",
    "cleave",
    "crush",
    "digestion",
    "divine",
    "drain",
    "flame",
    "magic",
    "peck",
    "pierce",
    "pound",
    "punch",
    "scratch",
    "shock",
    "shot",
    "slap",
    "slash",
    "slice",
    "sting",
    "slime",
    "smash",
    "stab",
    "strangle",
    "suction",
    "thrust",
    "thwack",
    "whip",
    "wrath"
  ];

  public static getDamageTypeForParts(parts: number) {
    if( BitFlags.hasFlag(parts, BitFlag.U)) {
      console.log("has claws!");
      return "claw";
    }
    if( BitFlags.hasFlag(parts, BitFlag.V)) {
      console.log("has fangs!");
      return "bite";
    }
    if( BitFlags.hasFlag(parts, BitFlag.Q)) {
      console.log("has tail!");
      return "whip";
    }
    if( BitFlags.hasFlag(parts, BitFlag.G)) {
      console.log("has hands!");
      return "punch";
    }

    return "none";
  }
}
