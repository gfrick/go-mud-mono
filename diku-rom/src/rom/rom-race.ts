/**
 * Created by George Frick on 2/18/2018.
 */

export class RomRace {

  public name: string;
  public isPcRace: boolean;
  public movementMsg: string;
  public actFlags: number;
  public affectedByFlags: number;
  public affectedBy2Flags: number;
  public offensiveFlags: number;
  public immunityFlags: number;
  public resistanceFlags: number;
  public vulnerableFlags: number;
  public formFlags: number;
  public partsFlags: number;
  public defaultSize: number;
  public defaultDamage: string;
  public prefix: string;

  constructor(name: string) {
    this.name = name;
    this.isPcRace = false;
    this.movementMsg = "-walks-";
    this.actFlags = 0;
    this.affectedByFlags = 0;
    this.affectedBy2Flags = 0;
    this.offensiveFlags = 0;
    this.immunityFlags = 0;
    this.resistanceFlags = 0;
    this.vulnerableFlags = 0;
    this.formFlags = 0;
    this.partsFlags = 0;
    this.defaultSize = 2;
    this.prefix = "a";
    this.defaultDamage = "none";
  }

}
