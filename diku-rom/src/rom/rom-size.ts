enum Size {
    XS,
    SMALL,
    MEDIUM,
    LARGE,
    XL
}

const sizeNames = ["extra small", "small", "medium", "large", "extra large"];
const sizeShortNames = ["XS", "S", "M", "L", "XL"];

function SizeLookup(size: string) : Size {
    let size2 = size.toLowerCase().trim();
    if (size2 === ("extra small") || size2 === ("xs"))
        return Size.XS;
    else if (size2 === ("small") || size2 === ("s"))
        return Size.SMALL;
    else if (size2 === ("medium") || size2 === ("m"))
        return Size.MEDIUM;
    else if (size2 === ("large") || size2 === ("l"))
        return Size.LARGE;
    else if (size2 === ("extra large") || size2 === ("xl"))
        return Size.XL;
    else
        return Size.MEDIUM;
}

function SizeDescription(size: Size) : string {
    return sizeNames[size];
}

function SizeShortName(size: Size): string {
    return sizeShortNames[size];
}

export { Size, SizeLookup, SizeDescription, SizeShortName }
