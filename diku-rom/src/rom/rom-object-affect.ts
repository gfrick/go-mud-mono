export class RomObjectAffect {
  private value: number;
  private type: number;

  /*
   * affect types
   */
  public static readonly APPLY_NONE = 0;
  public static readonly APPLY_STR = 1;
  public static readonly APPLY_DEX = 2;
  public static readonly APPLY_INT = 3;
  public static readonly APPLY_WIS = 4;
  public static readonly APPLY_CON = 5;
  public static readonly APPLY_SEX = 6;
  public static readonly APPLY_CLASS = 7;
  public static readonly APPLY_LEVEL = 8;
  public static readonly APPLY_AGE = 9;
  public static readonly APPLY_HEIGHT = 10;
  public static readonly APPLY_WEIGHT = 11;
  public static readonly APPLY_MANA = 12;
  public static readonly APPLY_HIT = 13;
  public static readonly APPLY_MOVE = 14;
  public static readonly APPLY_GOLD = 15;
  public static readonly APPLY_EXP = 16;
  public static readonly APPLY_AC = 17;
  public static readonly APPLY_HITROLL = 18;
  public static readonly APPLY_DAMROLL = 19;
  public static readonly APPLY_SAVES = 20;
  public static readonly APPLY_SAVING_PARA = 21;
  public static readonly APPLY_SAVING_ROD = 22;
  public static readonly APPLY_SAVING_PETRI = 23;
  public static readonly APPLY_SAVING_BREATH = 24;
  public static readonly APPLY_SAVING_SPELL = 25;
  public static readonly NUM_APPLIES = 26;

  public static readonly affectNames = ["Strength", "Dexterity",
    "Intelligence", "Wisdom", "Constitution", "Mana", "Hit", "Move", "AC",
    "Hitroll", "Damroll", "Saves"];

  public static readonly affectFlags = [RomObjectAffect.APPLY_STR, RomObjectAffect.APPLY_DEX, RomObjectAffect.APPLY_INT,
    RomObjectAffect.APPLY_WIS, RomObjectAffect.APPLY_CON, RomObjectAffect.APPLY_MANA, RomObjectAffect.APPLY_HIT, RomObjectAffect.APPLY_MOVE, RomObjectAffect.APPLY_AC,
    RomObjectAffect.APPLY_HITROLL, RomObjectAffect.APPLY_DAMROLL, RomObjectAffect.APPLY_SAVES];

  public static readonly NUM_OBJ_AFFS = 12;


  public constructor(nType: number, nValue: number) {
    this.type = nType;
    this.value = nValue;
  }

  public  setType(nType: number): void {
    this.type = nType;
  }

  public setValue(nValue: number): void {
    this.value = nValue;
  }

  public  getType(): number {
    return this.type;
  }

  public getValue(): number {
    return this.value;
  }

  public  toString(): string {
    return this.stringLookup(this.type) + "  " + this.signLookup(this.value) + this.value + "";
  }

  public  fileString(): string {
    return "A\n" + this.type + " " + this.value + "\n";
  }


  public  signLookup(value: number): string {
    if (value >= 0)
      return "+";

    return "";
  }

  public static flagToIndex(i: number): number {
    for (let a = 0; a < RomObjectAffect.NUM_OBJ_AFFS; a++) {
      if (RomObjectAffect.affectFlags[a] == i)
        return a;
    }

    return 0;
  }

  public stringLookup(t: number): string {
    switch (t) {
      case RomObjectAffect.APPLY_NONE: {
        return "none";
      }
      case RomObjectAffect.APPLY_STR: {
        return "Strength";
      }
      case RomObjectAffect.APPLY_DEX: {
        return "Dexterity";
      }
      case RomObjectAffect.APPLY_INT: {
        return "Intelligence";
      }
      case RomObjectAffect.APPLY_WIS: {
        return "Wisdom";
      }
      case RomObjectAffect.APPLY_CON: {
        return "Constitution";
      }
      case RomObjectAffect.APPLY_SEX: {
        return "Sex";
      }
      case RomObjectAffect.APPLY_CLASS: {
        return "Class";
      }
      case RomObjectAffect.APPLY_LEVEL: {
        return "Level";
      }
      case RomObjectAffect.APPLY_AGE: {
        return "Age";
      }
      case RomObjectAffect.APPLY_HEIGHT: {
        return "Height";
      }
      case RomObjectAffect.APPLY_WEIGHT: {
        return "Weight";
      }
      case RomObjectAffect.APPLY_MANA: {
        return "Mana";
      }
      case RomObjectAffect.APPLY_HIT: {
        return "Hit Points";
      }
      case RomObjectAffect.APPLY_MOVE: {
        return "Movement";
      }
      case RomObjectAffect.APPLY_GOLD: {
        return "Gold/Yen";
      }
      case RomObjectAffect.APPLY_EXP: {
        return "Experience";
      }
      case RomObjectAffect.APPLY_AC: {
        return "Armor Class";
      }
      case RomObjectAffect.APPLY_HITROLL: {
        return "Hitroll";
      }
      case RomObjectAffect.APPLY_DAMROLL: {
        return "Damroll";
      }
      case RomObjectAffect.APPLY_SAVES: {
        return "Saves";
      }
      case RomObjectAffect.APPLY_SAVING_PARA: {
        return "Saves-Para";
      }
      case RomObjectAffect.APPLY_SAVING_ROD: {
        return "Saves-Rod";
      }
      case RomObjectAffect.APPLY_SAVING_PETRI: {
        return "Saves-Petrify";
      }
      case RomObjectAffect.APPLY_SAVING_BREATH: {
        return "Saves-Breath";
      }
      case RomObjectAffect.APPLY_SAVING_SPELL: {
        return "Saves-Spell";
      }
      default: {
        return "none";
      }
    }
  }
}
