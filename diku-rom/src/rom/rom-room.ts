import {Mobile} from './rom-mobile';
import {RomExit} from './rom-exit';
import {RomObject} from './rom-object';
import {Direction} from "../shared/direction";

/**
 * Created by George Frick on 2/18/2018.
 */
export class RomRoom {
  public vnum: number;
  public name: string;
  public description: string;
  public extraDescriptions: any = {};
  public flags: number;
  public sector: number;
  public heal: number;
  public mana: number;
  public exits: RomExit[];
  public exitsFromArea: RomExit[];
  public items: Array<RomObject>;
  public mobiles: Array<Mobile>;

  public mapX: number; // Store where it is graphically :-)
  public mapY: number;

  constructor(vnum: number, jsonData?: any) {
    this.vnum = vnum;
    this.name = 'New Room';
    this.description = 'New Room';
    this.flags = 0;
    this.sector = 0;
    this.heal = 100;
    this.mana = 100;
    this.exits = [];
    this.exitsFromArea = [];
    this.items = [];
    this.mobiles = [];
    this.mapX = -1;
    this.mapY = -1;
    if (jsonData) {
      Object.assign(this, jsonData);
      if (jsonData.exits) {
        this.exits = this.createExitList(jsonData);
      }
    }
  }

  public createExitList(jsonData?: any): RomExit[] {
    const exits: RomExit[] = [];
    if (jsonData && jsonData.exits) {
      jsonData.exits.forEach((eachExit: any) => {
        exits.push(new RomExit(eachExit));
      });
    }
    return exits;
  }

  public getExit(direction: Direction): RomExit | null {
    return this.exits.find((fExit) => fExit.direction === direction) || null;
  }

  public removeExit(direction: Direction): void {
    const exitIdx = this.exits.findIndex((fExit) => fExit.direction === direction);
    if( exitIdx >= 0 ) {
      this.exits.splice(exitIdx,1);
    }
  }

  public setExit(exit: RomExit): void {
    if (this.exits == null) {
      this.exits = [];
      this.exits.push(exit);
      return;
    }
    const index = this.exits.findIndex((fExit) => fExit.direction === exit.direction);
    if( index === -1 ) {
      this.exits.push(exit);
      return;
    }
    this.exits = this.exits.splice(index,1,exit);
  }

  public getJSON(): any {
    const thisRoom: any = Object.assign({}, this);
    delete thisRoom.exitsFromArea;
    delete thisRoom.items;
    delete thisRoom.mobiles;
    delete thisRoom.mapX;
    delete thisRoom.mapY;
    if (this.exits && this.exits.length > 0) {
      thisRoom.exits = [];
      this.exits.forEach((eachExit) => {
          thisRoom.exits.push(eachExit.getJSON());
      }, this);
    }
    return thisRoom;
  }

}
