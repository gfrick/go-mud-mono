
enum Sex {
  NEUTRAL = 0,
  MALE = 1,
  FEMALE = 2
}

const sexNames = ["neutral", "male", "female"];

function SexLookup(arg: string): Sex {
  let temp = arg.toLowerCase();
  switch (temp.charAt(0)) {
    case 'm':
      return Sex.MALE;
    case 'f':
      return Sex.FEMALE;
    case 'e':
    case 'n':
    default:
      return Sex.NEUTRAL;
  }
}

function SexDescription(sex: Sex) : string {
  return sexNames[sex];
}

function SexRandom(): Sex {
  return Math.random() * sexNames.length | 0;
}

export { Sex, SexLookup, SexDescription, SexRandom };
