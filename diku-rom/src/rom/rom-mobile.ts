import {RomRace} from './rom-race';
import {Dice} from '../shared/dice';
import {RomMobTrigger} from './rom-mob-trigger';
import {RomObject} from './rom-object';

export class Mobile {
  public name: string;
  public shortDesc: string;
  public longDesc: string;
  public description: string;
  public size: number;
  public vnum: number;
  public level: number;
  public affectedBy: number;
  public affectedBy2: number;
  public alignment: number;
  public material: string;
  public armor: Array<number>;
  public extraDescriptions = {};

  public race: RomRace;
  public deathCry?: string;
  public actFlags: number;
  public group: number;
  public hitRoll: number;
  public hitDice: Dice;
  public manaDice: Dice;
  public damageDice: Dice;
  public damageType: string;
  public offensiveFlags: number;
  public immunityFlags: number;
  public resistanceFlags: number;
  public vulnerabilityFlags: number;
  public startPosition: number;
  public defaultPosition: number;
  public sex: number;
  public wealth: number;
  public form: number;
  public parts: number;
  public maxInArea?: number; // for resets
  public mySpec?: string;
  public equipment: Array<number>;
  public inventory: Array<RomObject>;
  public triggers: Array<RomMobTrigger>;

  constructor(vnum: number, jsonData?: any) {
    this.vnum = vnum;
    this.name = '';
    this.shortDesc = '';
    this.longDesc = '';
    this.description = '';
    this.triggers = [];
    this.inventory = [];
    this.equipment = [];
    this.size = 0;
    this.level = 1;
    this.affectedBy = 0;
    this.affectedBy2 = 0;
    this.alignment = 0;
    this.material = "";
    this.armor = [0, 0, 0, 0];
    this.race = new RomRace("human");
    this.actFlags = 0;
    this.group = 0;
    this.hitRoll = 0;
    this.hitDice = new Dice(1, 1, 0);
    this.manaDice = new Dice(1, 1, 0);
    this.damageDice = new Dice(1, 1, 0);
    this.damageType = "punch";
    this.offensiveFlags = 0;
    this.immunityFlags = 0;
    this.resistanceFlags = 0;
    this.vulnerabilityFlags = 0;
    this.startPosition = 0;
    this.defaultPosition = 0;
    this.sex = 0;
    this.wealth = 0;
    this.form = 0;
    this.parts = 0;
    if (jsonData) {
      Object.assign(this, jsonData);
    }
  }

  public getJSON(): any {
    const jsonMob = Object.assign({}, {
      name: this.name,
      shortDesc: this.shortDesc,
      longDesc: this.longDesc,
      description: this.description,
      vnum: this.vnum,
      level: this.level,
      affectedBy: this.affectedBy,
      affectedBy2: this.affectedBy2,
      alignment: this.alignment,
      material: this.material,
      armor: this.armor,
      race: this.race ? this.race.name : 'unknown',
      deathCry: this.deathCry,
      actFlags: this.actFlags,
      group: this.group,
      hitRoll: this.hitRoll,
      hitDice: this.hitDice,
      manaDice: this.manaDice,
      damageDice: this.damageDice,
      damageType: this.damageType,
      offensiveFlags: this.offensiveFlags,
      immunityFlags: this.immunityFlags,
      resistanceFlags: this.resistanceFlags,
      vulnerabilityFlags: this.vulnerabilityFlags,
      startPosition: this.startPosition,
      defaultPosition: this.defaultPosition,
      sex: this.sex,
      wealth: this.wealth,
      form: this.form,
      parts: this.parts,
      maxInArea: this.maxInArea, // for resets
      mySpec: this.mySpec,
      equipment: this.equipment,
      inventory: this.buildInventoryArray()
    });
    return jsonMob;
  }

  private buildInventoryArray(): Array<number> {
    const inventory: number[] = [];
    this.inventory.forEach((item) => inventory.push(item.vnum));
    return inventory;
  }
}
