/**
 * Created by George Frick on 2/13/2018.
 */
export class RomCondition {

  public static readonly conditionFlags =
    [100, 90, 75, 50, 25, 10, 0];

  public static readonly conditionNames =
    ["P", "G", "A", "W", "D", "B", "R"];

  public static conditionLookup(code: string): number {
    let conditionIndex = 6;
    while (conditionIndex >= 0) {
      if ( code[0] === RomCondition.conditionNames[conditionIndex])
        return RomCondition.conditionFlags[conditionIndex];
      else
        conditionIndex--;
    }

    return 100;
  }

  public static conditionString(condition: number): string {
    let conditionIndex = 6;
    while (conditionIndex >= 0) {
      if (condition == RomCondition.conditionFlags[conditionIndex])
        return RomCondition.conditionNames[conditionIndex];
      else if (conditionIndex != 6 && conditionIndex != 0) {
        if (condition < RomCondition.conditionFlags[conditionIndex + 1] && condition > RomCondition.conditionFlags[conditionIndex - 1])
          return RomCondition.conditionNames[conditionIndex];
      }

      conditionIndex--;
    }

    return RomCondition.conditionNames[0];
  }

}
