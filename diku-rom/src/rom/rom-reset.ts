/**
 * Created by George Frick on 2/18/2018.
 */
export class RomReset {
  command: string;
  arg1: number;
  arg2: number;
  arg3: number;
  arg4: number;
  chance: number; // animud only.

  public constructor(c?: string, a1?: number, a2?: number, a3?: number, a4?: number, newChance?: number) {
    this.command = c || "";
    this.arg1 = a1 || 0;
    this.arg2 = a2 || 0;
    this.arg3 = a3 || 0;
    this.arg4 = a4 || 0;
    this.chance = newChance || 100;
  }

  public getCommand(): string {
    return this.command;
  }

  public getArg(which: number): number {
    switch (which) {
      case 1:
        return this.arg1;
      case 2:
        return this.arg2;
      case 3:
        return this.arg3;
      case 4:
        return this.arg4;
      default:
        throw new Error('bad value to get arg in resets');
    }
  }

  public setArg(which: number, newValue: number): void {
    switch (which) {
      case 1:
        this.arg1 = newValue;
        return;
      case 2:
        this.arg2 = newValue;
        return;
      case 3:
        this.arg3 = newValue;
        return;
      case 4:
        this.arg4 = newValue;
        return;
      default:
        return;
    }
  }

  public toString(): string {
    let temp;
    switch (this.command.charAt(0)) {
      case 'M': {
        temp = 'M 0 ' + (this.arg1) + ' ' + (this.arg2) + ' ' + (this.arg3) + ' ' + (this.arg4);
        break;
      }
      case 'O': {                                   // obj              // room
        temp = 'O ' + (this.chance) + ' ' + (this.arg1) + ' 0 ' + (this.arg3);
        break;
      }
      case 'P': {
        temp = 'P ' + (this.chance) + ' ' + (this.arg1) + ' ' + (this.arg2) + ' ' + (this.arg3) + ' ' + (this.arg4);
        break;
      }
      case 'G': {
        temp = 'G ' + (this.chance) + ' ' + (this.arg1) + ' 0';
        break;
      }
      case 'E': {
        temp = 'E ' + (this.chance) + ' ' + (this.arg1) + ' 0 ' + (this.arg3);
        break;
      }
      case 'D': {
        temp = 'D 0 ' + (this.arg1) + ' ' + (this.arg2) + ' ' + (this.arg3);
        break;
      }
      case 'R': {
        temp = 'R 0 ' + (this.arg1) + ' ' + (this.arg3);
        break;
      }
      default: {
        temp = 'BAD COMMAND IN RESET.';
        break;
      }
    }
    return temp;
  }


  public getJSON() {
    return Object.assign({}, this);
  }

}
