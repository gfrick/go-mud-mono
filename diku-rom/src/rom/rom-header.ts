export class RomAreaHeader {
  public fileName: string;
  public pathName: string;
  public areaName: string;
  public builder: string;
  public lowVnum: number;
  public highVnum: number;
  public security: number;
  public flags: number;

  constructor(jsonData?: any) {
    this.fileName = "";
    this.pathName = "";
    this.areaName = "NewArea";
    this.builder = "Anonymous";
    this.lowVnum = 1000;
    this.highVnum = 1100;
    this.security = 0;
    this.flags = 0;
    if (jsonData) {
      Object.assign(this, jsonData.header);
    }
  }

  public get isValid(): boolean {
    return !!(this.areaName && this.builder && this.lowVnum && this.highVnum);
  }

  public getJSON(): any {
    const json: any = Object.assign({}, this);
    delete json.security;
    delete json.flags;
    delete json.pathName;
    delete json.fileName;
    return json;
  }
}
