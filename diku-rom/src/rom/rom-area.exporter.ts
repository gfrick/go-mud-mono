import {RomArea} from './rom-area';

export interface JSONExport {
  getJSON(): any;
}

/**
 * Created by George Frick on 4/15/2019.
 */
export class RomAreaExporter {

  constructor() {
  }

  public getAreaAsJSON(area: RomArea): any {
    const baseObject = {
      id: area.id,
      header: area.getHeader().getJSON(),
      rooms: this.getJSON(area.rooms),
      mobs: this.getJSON(area.mobs),
      objects: this.getJSON(area.objects),
      resets: this.getJSON(area.resets)
    };
    return baseObject;
  }

  public getJSON(iterateOverMe: Array<JSONExport>): Array<any> {
    const output: any[] = [];
    iterateOverMe.forEach((eachItem) => {
      output.push(eachItem.getJSON());
    }, this);
    return output;
  }
}
