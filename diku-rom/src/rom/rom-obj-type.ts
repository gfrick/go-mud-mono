/**
 * Created by George Frick on 2/13/2018.
 */
export class RomObjType {
  /*
   * Item constants
   */
  public static readonly ITEM_LIGHT = 1;
  public static readonly ITEM_SCROLL = 2;
  public static readonly ITEM_WAND = 3;
  public static readonly ITEM_STAFF = 4;
  public static readonly ITEM_WEAPON = 5;
  // 6, 7
  public static readonly ITEM_TREASURE = 8;
  public static readonly ITEM_ARMOR = 9;
  public static readonly ITEM_POTION = 10;
  public static readonly ITEM_CLOTHING = 11;
  public static readonly ITEM_FURNITURE = 12;
  public static readonly ITEM_TRASH = 13;
  // 14
  public static readonly ITEM_CONTAINER = 15;
  // 16
  public static readonly ITEM_DRINK_CON = 17;
  public static readonly ITEM_KEY = 18;
  public static readonly ITEM_FOOD = 19;
  public static readonly ITEM_MONEY = 20;
  // 21
  public static readonly ITEM_BOAT = 22;
  public static readonly ITEM_CORPSE_NPC = 23;
  public static readonly ITEM_CORPSE_PC = 24;
  public static readonly ITEM_FOUNTAIN = 25;
  public static readonly ITEM_PILL = 26;
  // 27
  public static readonly ITEM_MAP = 28;
  public static readonly ITEM_PORTAL = 29;
  public static readonly ITEM_WARP_STONE = 30;
  public static readonly ITEM_ROOM_KEY = 31;
  public static readonly ITEM_GEM = 32;
  public static readonly ITEM_JEWELRY = 33;
  public static readonly ITEM_JUKEBOX = 34;
  public static readonly ITEM_PROJECTILE = 35;
  public static readonly ITEM_UPGRADE = 36;
  public static readonly ITEM_ORE = 37;
  public static readonly ITEM_RESTRING = 38;
  public static readonly ITEM_ENCHANT = 39;

  public static readonly typeNames =
    ["Light", "Scroll", "Wand", "Staff", "Weapon", "Treasure",
      "Armor", "Potion", "Clothing", "Furniture", "Trash", "Container",
      "Drink Container", "Key", "Food", "Money", "Boat",
      "Fountain", "Pill", "Map", "Portal", "Warpstone",
      "Roomkey", "Gem", "Jewelry", "Jukebox", "Ore"];

  public static readonly typeFlags =
    [RomObjType.ITEM_LIGHT, RomObjType.ITEM_SCROLL, RomObjType.ITEM_WAND, RomObjType.ITEM_STAFF, RomObjType.ITEM_WEAPON,
      RomObjType.ITEM_TREASURE, RomObjType.ITEM_ARMOR, RomObjType.ITEM_POTION, RomObjType.ITEM_CLOTHING, RomObjType.ITEM_FURNITURE,
      RomObjType.ITEM_TRASH, RomObjType.ITEM_CONTAINER, RomObjType.ITEM_DRINK_CON, RomObjType.ITEM_KEY, RomObjType.ITEM_FOOD,
      RomObjType.ITEM_MONEY, RomObjType.ITEM_BOAT, RomObjType.ITEM_FOUNTAIN, RomObjType.ITEM_PILL, RomObjType.ITEM_MAP, RomObjType.ITEM_PORTAL,
      RomObjType.ITEM_WARP_STONE, RomObjType.ITEM_ROOM_KEY, RomObjType.ITEM_GEM, RomObjType.ITEM_JEWELRY, RomObjType.ITEM_JUKEBOX,
      RomObjType.ITEM_ORE];

  public static readonly itemTypeSet = [
    {'type': RomObjType.ITEM_LIGHT, 'name': 'Light'},
    {'type': RomObjType.ITEM_SCROLL, 'name': 'SCROLL'},
    {'type': RomObjType.ITEM_WAND, 'name': 'WAND'},
    {'type': RomObjType.ITEM_STAFF, 'name': 'STAFF'},
    {'type': RomObjType.ITEM_WEAPON, 'name': 'WEAPON'},
    {'type': RomObjType.ITEM_TREASURE, 'name': 'TREASURE'},
    {'type': RomObjType.ITEM_ARMOR, 'name': 'ARMOR'},
    {'type': RomObjType.ITEM_POTION, 'name': 'POTION'},
    {'type': RomObjType.ITEM_CLOTHING, 'name': 'CLOTHING'},
    {'type': RomObjType.ITEM_FURNITURE, 'name': 'FURNITURE'},
    {'type': RomObjType.ITEM_TRASH, 'name': 'TRASH'},
    {'type': RomObjType.ITEM_CONTAINER, 'name': 'CONTAINER'},
    {'type': RomObjType.ITEM_DRINK_CON, 'name': 'DRINK_CON'},
    {'type': RomObjType.ITEM_KEY, 'name': 'KEY'},
    {'type': RomObjType.ITEM_FOOD, 'name': 'FOOD'},
    {'type': RomObjType.ITEM_MONEY, 'name': 'MONEY'},
    {'type': RomObjType.ITEM_BOAT, 'name': 'BOAT'},
    {'type': RomObjType.ITEM_FOUNTAIN, 'name': 'FOUNTAIN'},
    {'type': RomObjType.ITEM_PILL, 'name': 'PILL'},
    {'type': RomObjType.ITEM_MAP, 'name': 'MAP'},
    {'type': RomObjType.ITEM_PORTAL, 'name': 'PORTAL'},
    {'type': RomObjType.ITEM_WARP_STONE, 'name': 'WARP_STONE'},
    {'type': RomObjType.ITEM_ROOM_KEY, 'name': 'ROOM_KEY'},
    {'type': RomObjType.ITEM_GEM, 'name': 'GEM'},
    {'type': RomObjType.ITEM_JEWELRY, 'name': 'JEWELRY'},
    {'type': RomObjType.ITEM_JUKEBOX, 'name': 'JUKEBOX'},
    {'type': RomObjType.ITEM_ORE, 'name': 'Ore'}
  ];

  public static readonly NUM_ITEMS = 27;

  public static stringFromType(type: number): string {
    for (let a = 0; a < RomObjType.NUM_ITEMS; a++) {
      if (type == RomObjType.typeFlags[a])
        return RomObjType.typeNames[a];
    }

    return "Trash";
  }

  public static typeFromString(type: string): number {
    for (let a = 0; a < RomObjType.NUM_ITEMS; a++) {
      if (type.toLowerCase() === RomObjType.typeNames[a].toLowerCase())
        return RomObjType.typeFlags[a];
    }

    if (type.toLowerCase() === "drink") {
      //System.out.println("Found drink container");
      return RomObjType.ITEM_DRINK_CON;
    }

    return RomObjType.ITEM_TRASH;
  }

}
