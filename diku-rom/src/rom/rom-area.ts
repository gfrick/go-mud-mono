import {RomAreaHeader} from './rom-header';
import {RomRoom} from './rom-room';
import {RomObject} from './rom-object';
import {Mobile} from './rom-mobile';
import {RomReset} from './rom-reset';

export class MobileProgram {
}

export class RomArea {

  public id: string = "";
  public header: RomAreaHeader;
  public rooms: Array<RomRoom>;
  public objects: Array<RomObject>;
  public mobs: Array<Mobile>;
  public resets: Array<RomReset>;
  public mobprogs: Array<MobileProgram>;

  constructor(jsonData?: any) {
    if (jsonData) {
      this.id = jsonData._id;
    }
    this.header = new RomAreaHeader(jsonData);
    this.rooms = this.createRoomList(jsonData);
    this.objects = this.createObjectList(jsonData);
    this.mobs = this.createMobList(jsonData);
    this.resets = this.createResetList(jsonData);
    this.mobprogs = [];
  }


  public createRoomList(jsonData?: any): RomRoom[] {
    const rooms: RomRoom[] = [];
    if (jsonData && jsonData.rooms && jsonData.rooms.length > 0) {
      jsonData.rooms.forEach((eachRoom: any) => {
        rooms.push(new RomRoom(eachRoom.vnum, eachRoom));
      });
    } else {
      rooms.push(this.createRoom());
    }
    return rooms;
  }

  public createObjectList(jsonData?: any): RomObject[] {
    const objects: RomObject[] = [];
    if (jsonData && jsonData.objects && jsonData.objects.length > 0) {
      jsonData.objects.forEach((eachObject: any) => {
        objects.push(new RomObject(eachObject));
      });
    }
    return objects;
  }

  public createMobList(jsonData?: any): Mobile[] {
    const mobs: Mobile[] = [];
    if (jsonData && jsonData.mobs) {
      jsonData.mobs.forEach((eachMob: any) => {
        mobs.push(new Mobile(eachMob));
      });
    }
    return mobs;
  }

  public createResetList(jsonData?: any): RomReset[] {
    const reset: RomReset[] = [];
    if (jsonData && jsonData.resets) {
      jsonData.resets.forEach((eachReset: any) => {
        reset.push(new RomReset(eachReset.command, eachReset.arg1,
          eachReset.arg2, eachReset.arg3, eachReset.arg4, eachReset.command));
      });
    }
    return reset;
  }

  public isEmpty(): boolean {
    return this.rooms.length === 0 || this.objects.length === 0 || this.mobs.length === 0;
  }

  public getHeader(): RomAreaHeader {
    return this.header;
  }

  public get isValid(): boolean {
    return this.header && this.header.isValid && this.rooms && this.rooms.length > 0;
  }

  public get isEditable(): boolean {
    return this.header && this.header.isValid;
  }

  public getNextMobileVnum(): number {
    let vnum = this.header.lowVnum;

    if (this.mobs && this.mobs.length > 0) {
      vnum = +this.mobs[this.mobs.length - 1].vnum;
    }
    return vnum + 1;
  }

  public getNextRoomVnum(): number {
    let vnum = this.header.lowVnum;
    console.log(typeof vnum);
    if (this.rooms && this.rooms.length > 0) {
      vnum = +this.rooms[this.rooms.length - 1].vnum;
    }
    return vnum + 1;
  }

  public getNextObjectVnum(): number {
    let vnum = this.header.lowVnum;
    if (this.objects && this.objects.length > 0) {
      vnum = +this.objects[this.objects.length - 1].vnum;
    }
    return vnum + 1;
  }

  public createMobile(): number {
    const mobile = new Mobile(this.getNextMobileVnum());
    return this.mobs.push(mobile) - 1;
  }

  public deleteMobile(index: number): number {
    this.mobs.splice(index, 1);
    if (index < this.mobs.length) {
      return index;
    }
    return this.mobs.length > 0 ? this.mobs.length - 1 : 0;
  }

  public createObject(): number {
    const obj = new RomObject(this.getNextObjectVnum());
    return this.objects.push(obj) - 1;
  }

  public deleteObject(index: number): number {
    this.objects.splice(index, 1);
    if (index < this.objects.length) {
      return index;
    }
    return this.objects.length > 0 ? this.objects.length - 1 : 0;
  }

  public createRoom(): RomRoom {
    return new RomRoom(this.getNextRoomVnum());
  }

  public deleteRoom(vnum: number): RomRoom | null {
    // Cannot delete the only room.
    if (this.rooms.length === 1) {
      return this.rooms[0];
    }
    let index = this.rooms.findIndex(sRoom => sRoom.vnum === vnum);
    if (index >= 0) {
      this.rooms.splice(index, 1);
      while (index >= this.rooms.length) {
        index--;
      }
      if (index < this.rooms.length && index >= 0) {
        return this.rooms[index];
      }
    }
    return null;
  }

  public findRoomByVnum(vnum: number): RomRoom | undefined {
    if (!this.rooms || this.rooms.length === 0) {
      return undefined;
    }
    return this.rooms.find((eachRoom: RomRoom) => eachRoom.vnum === vnum);
  }

}
