import {Size, SizeDescription, SizeLookup} from "../../src/rom/rom-size";

describe('Size', () => {
    it('should correctly lookup from string', () => {
        let size = SizeLookup("xl");
        expect(size).toEqual(Size.XL);
    });

    it('should correctly describe a value', () => {
        expect(SizeDescription(Size.XL)).toEqual("extra large");
    });
});
