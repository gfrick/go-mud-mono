import {Position, PositionDescription, PositionLookup} from "../../src/rom/rom-position";

describe('Position', () => {
    it('should correctly lookup from string', () => {
        let position = PositionLookup("sitting");
        expect(position).toEqual(Position.Sitting);
        position = PositionLookup("mortally wounded");
        expect(position).toEqual(Position.Mortal);

        position = PositionLookup("fail");
        expect(position).toEqual(Position.Standing);
    });

    it('should correctly describe a value', () => {
        expect(PositionDescription(Position.Standing)).toEqual("standing");
        expect(PositionDescription(Position.Dead)).toEqual("dead");
        expect(PositionDescription(Position.Mortal)).toEqual("mortally wounded");
    });
});
