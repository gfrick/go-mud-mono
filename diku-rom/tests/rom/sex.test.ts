import {Sex, SexDescription, SexLookup} from "../../src/rom/rom-sex";

describe('Sex', () => {
    it('should correctly lookup from string', () => {
        let sex = SexLookup("male");
        expect(sex).toEqual(Sex.MALE);
        sex = SexLookup("female");
        expect(sex).toEqual(Sex.FEMALE);
        sex = SexLookup("fem");
        expect(sex).toEqual(Sex.FEMALE);
        sex = SexLookup("xyz");
        expect(sex).toEqual(Sex.NEUTRAL);
        sex = SexLookup("either");
        expect(sex).toEqual(Sex.NEUTRAL);
        sex = SexLookup("neutral");
        expect(sex).toEqual(Sex.NEUTRAL);
    });

    it('should correctly describe a value', () => {
        expect(SexDescription(Sex.MALE)).toEqual("male");
        expect(SexDescription(Sex.FEMALE)).toEqual("female");
        expect(SexDescription(Sex.NEUTRAL)).toEqual("neutral");
    });
});
