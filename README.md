# Ibrekeniel

This is a MUD written in Node & Typescript using WebSockets

This readme outlines the developer process, basic coding style guide and provides guidance and details around the technical approach to the user interface. You should always review and update this document to encompass your changes.

# Getting Started
Before you can work with the code, build it, or deploy it you must set up your development environment. The required environment is Git, Node, and something to work with Typescript. Because
there is a Docker Compose file (in the Runtime directory), you don't need much beyond an editor.

### Running 

The mud is run locally with Docker, primarily Docker-Compose in order to bring together the Server, Client, and Mongo. The Docker compose
file is located in the Runtime directory. The easiest way to run everything is with `docker-compose up` you may get an error from
Mongo along the lines of:
`ERROR: for mongo  Cannot create container for service mongo: Conflict. The container name "/mongo" is already in use by container "fb26e62..". You have to remove (or rename) that container to be able to reuse that name.`

In that case, just do a 'docker rm mongo' and try again.

With the app running, you can bring up the user interface on port 80 in your browser. Alternatively you can run
 everything with `docker-compose up -d --build` which will also build each container.

You probably want to run docker-compose rm when you are done.

Digital Ocean, these were the steps I took after setting up the account:

 1. `docker-machine create --driver digitalocean --digitalocean-access-token <yourtoken> docker-sandbox`
 1. `docker-machine ls`
 1. `docker-machine env docker-sandbox`
 1. `eval $(docker-machine env docker-sandbox)`

At this point you are set up (connected) with the droplet as your docker host. Rebuild both the client and server
docker images, and then you can run compose. When you issue docker commands, they'll happen against the host. To
switch back to the local docker instance, use `eval "$(docker-machine env -u)"`

# Design Section

store race in database vs static vs via the service
how can you provide the races to the website for
player selection and viewing while also making
them available in the game.

website could have a static list, or it could load
them. Do you want to the races in the website at
compile time or view time?

Both the editor and the character creator should be
loading the races in real time. You should be able
to pass an 'isPcRace' flag to get all or just PC races.

So then the controller can use the service and the
game can use the service. 

Some of these things should move to properties?

## List of Major Features:

1) Character Data (Level/Race/Class)
2) Objects
4) Combat
5) Skills
6) Spells
9) short commands
0) Logging

## Commands

 * who (Works, ugly, possibly web?)
 * score (works, ugly)
 * say/chat (chat works, say does not)
 * tell
 * kill
 * flee
 * get
 * drop
 * wear
 * advanced look
 * position commands (n,s,e,w,u,d)
 
What will it mean for this to be complete?

* area management
* Area import, export, edit, save, load, map
* position
* saving/loading character
* keys/doors
* fighting
* leveling
* race, class, skills, spells
* multiple areas
* objects :-)
* resets
* weather
* death
* regeneration
* documentation
* open sourced on github
* presentation
* immortal commands? (not all)
* color/formatting
* output vs client data


