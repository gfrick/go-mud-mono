import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {AccountService} from "./services/account.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  public subscription: Subscription | null = null;
  public localState = {
    loggedIn: false,
    canPlay: false,
    canBuild: false
  };

  constructor(private accountService: AccountService) {
  }

  public ngOnInit() {
    this.subscription = this.accountService.loggedIn$.subscribe((loggedIn: boolean) => {
      this.localState = {
        loggedIn: this.accountService.checkCookie(),
        canPlay: this.accountService.checkPermission('play'),
        canBuild: this.accountService.checkPermission('build')
      };
    });
  }

  public ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
