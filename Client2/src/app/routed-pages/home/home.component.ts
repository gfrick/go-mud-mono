/**
 * Created by George Frick on 2/10/2018.
 */
import {Component} from '@angular/core';

@Component({
  selector: '[home]',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css']
})
export class HomeComponent  {

  constructor() {
  }

}
