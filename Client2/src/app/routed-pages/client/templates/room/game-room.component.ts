import {SummaryRoom} from "../models";
import {Component, Input} from "@angular/core";

@Component({
  selector: 'app-game-room',
  templateUrl: 'game-room.component.html',
  styleUrls: ['game-room.component.css']
})
export class GameRoomComponent {
  @Input()
  public json: any;// SummaryRoom;
  public room: SummaryRoom | null = null;

  ngOnInit() {
    console.log(this.json);
    this.room = this.json as SummaryRoom;
  }

  public exitList(): string {

    if (!this.room || !this.room.exits || this.room.exits.length === 0) {
      return '[None]';
    }
    return '[ ' + this.room.exits.join(', ') + ' ]';
  }
}
