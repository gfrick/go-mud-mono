import {Component, Input} from "@angular/core";

@Component({
  selector: 'app-game-score',
  templateUrl: 'score.component.html',
  styleUrls: ['score.component.css']
})
export class ScoreComponent {
  @Input()
  public json: any;
  public score: any | null = null;

  ngOnInit() {
    console.log(this.json);
    this.score = this.json;
  }

}
