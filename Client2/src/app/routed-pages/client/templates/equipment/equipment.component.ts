import {Component, Input} from "@angular/core";

interface IWornItem {
  slotDesc: string;
  itemDesc: string;
}

@Component({
  selector: 'app-game-equipment',
  templateUrl: 'equipment.component.html',
  styleUrls: ['equipment.component.css']
})
export class EquipmentComponent {
  @Input()
  public json: any;
  public equipment: IWornItem[] | null = null;

  ngOnInit() {
    console.log(this.json);
    this.equipment = this.json as IWornItem[];
  }

  equipmentDescription = (name:string) : string => {
    if( !name) {
      return "";
    }
    switch( name ) {
      default: return "!!MISSING!!";
      case "head": return "Head";
      case "about": return "About";
      case "neck": return "Neck";
      case "body": return "Body";
      case "arms": return "Arms";
      case "hands": return "Hands";
      case "waist": return "Waist";
      case "legs": return "Legs";
      case "feet": return "Feet";
      case "shield": return "Shield";
      case "wrist1": return "Wrist";
      case "wrist2": return "Wrist";
      case "finger1": return "Finger";
      case "finger2": return "Finger";
      case "hold1": return "Holding";
      case "hold2": return "Holding";
    }
  }


}
