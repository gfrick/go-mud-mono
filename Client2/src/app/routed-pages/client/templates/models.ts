
export interface SummaryRoom {
  vnum: number; //      `json:"vnum"`
  description: string; //   `json:"description"`
  name: string; //   `json:"name"`
  exits: string[]; // `json:"exits"`
  entities: string[]; // `json:"entities"`
  items: string[]; // `json:"items"`
}

export interface SummaryEquipment {
  neck: string; // IWearing
}

