import {Component, isDevMode, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

export interface JsonContent {
  template: string;
  message?: string;
  content?: any;
}

@Component({
  selector: 'app-client',
  templateUrl: 'client.component.html',
  styleUrls: ['client.component.css']
})
export class ClientComponent implements OnDestroy, OnInit {

  public joined = false;
  public message: string = "";
  public username: string = "";
  public content: JsonContent[]; // Array<SafeHtml | JsonContent>;
  private ws: WebSocket | null = null;

  constructor(private router: Router,
              private route: ActivatedRoute) {
    this.content = [];
  }

  public ngOnInit() {
    this.route.queryParams
      .subscribe((params) => {
        this.username = params['q'] || '';
        if (this.username) {
          this.commandJoin();
        }
      });
  }

  ngOnDestroy() {
    if (this.ws) {
      this.joined = false;
      this.ws.close();
    }
  }

  public buildWsUrl(target: string): string {
    if( isDevMode()) {
      return 'ws://' + window.location.hostname + ':' + window.location.port + '/' + target;
    }
    return 'wss://' + window.location.hostname + ':' + window.location.port + '/api/' + target;
  }

  public commandJoin() {
    if (!this.username) {
      console.log('you must choose a username');
      // Materialize.toast('You must choose a username', 2000);
      return;
    }

    this.content = [];
    this.ws = new WebSocket(this.buildWsUrl('play?key=' + this.username));
    this.ws.addEventListener('message', (e: MessageEvent<any>) => {
      const msg = JSON.parse(e.data);
      console.log(msg);
      if (msg.template) {
        this.content.push({
          message: "",
          template: msg.template,
          content: msg[msg.template] // can only be room for now...
        });
      } else {
        this.content.push({
          template: "none",
          message: msg.message
        });
      }

      const element = document.getElementById('chat-messages');
      if (element) {
        // This is ugly, but that's angular for you.
        setTimeout(() => {
          element.scrollTop = element.scrollHeight; // Auto scroll to the bottom
        }, 1);
      } else {
        console.log("No element found for scrolling text.");
      }
    });
    this.ws.addEventListener('error', (e) => {
      console.log(e);
      this.joined = false;
    });
    this.ws.addEventListener('close', (e) => {
      console.log(e);
      if( this.joined ) {
        this.joined = false;
        this.router.navigate(['dashboard']);
      }
    });
    this.ws.addEventListener('open', (e) => {
      this.joined = true;
    });
    this.ws.addEventListener('ping', (e) => {
      console.log("ping");
      console.log(e);
    });
    this.ws.addEventListener('pong', (e) => {
      console.log("pong");
      console.log(e);
    });

  }

  public commandSend() {
    if (this.message !== '' && this.ws) {
      this.ws.send(
        JSON.stringify({
            message: this.message
          }
        ));
      this.message = '';
    }
  }
}
