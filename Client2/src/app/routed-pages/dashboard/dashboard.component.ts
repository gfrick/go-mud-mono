import {Component, OnInit} from '@angular/core';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {AccountService} from "../../services/account.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public players: any[] = [];
  public content: Array<SafeHtml>;

  constructor(private sanitizer: DomSanitizer, private accountService: AccountService) {
    this.content = [];
  }

  public ngOnInit() {
    this.accountService.getPlayers().subscribe((results) => {
      this.players = results;
    });
  }

}
