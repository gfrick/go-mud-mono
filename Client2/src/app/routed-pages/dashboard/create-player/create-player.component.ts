import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AccountService} from "../../../services/account.service";

@Component({
  selector: 'app-create-player',
  templateUrl: 'create-player.component.html',
  styleUrls: ['create-player.component.css']
})
export class CreatePlayerComponent {

  public submitted = false;

  public playerGroup = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(5),
      Validators.maxLength(15),
      Validators.pattern('^[a-zA-Z]{5,15}')])
  });

  constructor(private accountService: AccountService, private router: Router) {
  }

  public commandSubmit() {
    this.submitted = true;

    if (this.playerGroup.invalid) {
      return;
    }
    console.log(this.playerGroup.value);
    const newPlayer = Object.assign({}, this.playerGroup.value);

    this.accountService.createPlayer(newPlayer).subscribe(
      (success) => {
        console.log(success);
        if( success ) {
          console.log("Player Created!");
        }
        // route back to dashboard, yo.
        this.router.navigate(['dashboard']);
      }
    );
  }

  get controls() {
    return this.playerGroup.controls;
  }
}
