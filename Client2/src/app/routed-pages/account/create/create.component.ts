import {Component} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AccountService} from "../../../services/account.service";
import {RouterModule} from '@angular/router';
@Component({
  selector: 'app-create',
  templateUrl: 'create.component.html',
  styleUrls: ['create.component.css']
})
export class CreateComponent {

  public loginGroup = new FormGroup({
    username: new FormControl(),
    password: new FormControl(),
    cpassword: new FormControl()
  });

  constructor(private accountService: AccountService) {
  }

  public commandSubmit() {
    const newAccount = Object.assign({}, this.loginGroup.value);
    delete newAccount.cpassword;
    this.accountService.createAccount(newAccount).subscribe(
      (success) => {
        if (success) {
          console.log('Account Created.');
        } else {
          console.log('Account Creation Failed.');
        }
      }
    );
  }
}
