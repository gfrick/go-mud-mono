import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountService} from "../../../services/account.service";

@Component({
  selector: 'app-logout',
  templateUrl: 'logout.component.html',
  styleUrls: ['logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router,
              private route: ActivatedRoute,
              private accountService: AccountService) {
  }

  public ngOnInit() {
    this.accountService.logout().subscribe(() => {
      this.router.navigateByUrl('/home');
    });
  }
}
