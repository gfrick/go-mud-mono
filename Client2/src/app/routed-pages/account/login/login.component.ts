import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountService} from "../../../services/account.service";

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {

  public loginGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  public nextRoute = '';
  public localState = {
    formValid: false,
    loginFailCount: 0
  };

  constructor(private router: Router,
              private route: ActivatedRoute,
              private accountService: AccountService) {
  }

  public ngOnInit() {
    // Get the query params
    this.route.queryParams
      .subscribe(params => this.nextRoute = params['return'] || 'dashboard');
    this.loginGroup.statusChanges.subscribe((newStatus) => {
      this.localState = {
        formValid: newStatus === 'VALID',
        loginFailCount: 0
      };
    });
  }

  public commandSubmit() {
    this.accountService.login(this.loginGroup.value).subscribe(
      (success: boolean) => {
        if (success) {
          if (this.nextRoute) {
            this.router.navigateByUrl('/' + this.nextRoute);
          }
        } else {
          this.loginGroup.patchValue({password: ''});
          this.localState = Object.assign({}, this.localState, {loginFailCount: this.localState.loginFailCount + 1});
        }
      }
    );
  }
}
