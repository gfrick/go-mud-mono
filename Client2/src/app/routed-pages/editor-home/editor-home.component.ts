import {Component, OnInit} from "@angular/core";
import {AreaService} from "../../services/area.service";
import {filter} from "rxjs";
import {IArea} from "omud/dist-lib/area";
import {Router} from "@angular/router";

@Component({
  selector: 'app-editor-home',
  templateUrl: 'editor-home.component.html',
  styleUrls: ['editor-home.component.css']
})
export class EditorHomeComponent implements OnInit {

  public active:number = 1;
  private area?: IArea;
  public areas: Array<IArea> = [];

  constructor(private router: Router, private areaService: AreaService) {
  }

  public ngOnInit() {
    this.areaService.loadAreas();
    this.areaService.areaList$
      .pipe(
        filter((isLoaded) => isLoaded.length > 0)
      )
      .subscribe((areas: IArea[]) => {
        this.areas = areas;
      });
    this.areaService.area$.pipe(
      filter( (selected) => selected !== undefined),
    ).subscribe( (area) => {
      this.area = area;

    });

  }

  public commandDelete(area: IArea): void {
    // this.areaService.deleteArea(area).subscribe((result: any) => {
    //   this.areaService.getAreas().subscribe((data) => {
    //     this.areas = data;
    //   });
    // }, (error: any) => {
    //   console.log('There was an error deleting the area');
    //   console.log(error);
    // });
  }

  public commandLoad(area: IArea): void {
    this.areaService.selectAreaData(area);
    this.router.navigate(['/editor'],{});
  }

  public commandNewArea() {
    this.areaService.selectAreaData(this.areaService.newBasicArea());
    this.router.navigate(['/editor'],{});
  }
}
