import {Component, OnInit} from "@angular/core";
import {AreaService} from "../../services/area.service";
import {filter} from "rxjs";
import {IArea} from "omud/dist-lib/area";
import {Router} from "@angular/router";

@Component({
  selector: 'app-editor-home',
  templateUrl: 'editor.component.html',
  styleUrls: ['editor.component.css']
})
export class EditorComponent implements OnInit {

  private errorDisplay: string;
  public active: number = 1;
  public area?: IArea;
  public isShowAlert = false;
  public alertType = "success";
  public alertContent = "";

  constructor(private router: Router, private areaService: AreaService) {
    this.errorDisplay = '';
  }

  public ngOnInit() {
    this.areaService.area$.pipe(
      filter((selected) => selected !== undefined),
    ).subscribe((area) => {
      this.area = area;
    });
  }

  public getAreaName(): string {
    if (this.area) {
      return this.area.header.areaName;
    }
    return "!NO AREA!";
  }

  public canBeEdited(): boolean {
    return this.areaService.canEditArea();
  }

  public canBeSaved(): boolean {
    return !!this.area;
  }

  public commandChangeArea(area: IArea) {
    this.area = area;
  }

  public showAlert(type: string, alertContent: string) {
    if( this.isShowAlert ) {
      return;
    }
    this.isShowAlert = true;
    this.alertType = type;
    this.alertContent = alertContent;
    setTimeout(() => {
      this.isShowAlert = false;
    },5000);
  }
  public commandSave(): void {

    this.areaService.saveArea().subscribe(
      {
        next: (result) => {
          this.showAlert("success", "Area saved.");
        },
        error: (err) => {
          this.showAlert("danger", "Failed to save area, you likely need to login again.");
        },
        complete: () => {
          console.log("complete");
        }
      }
    )
  }

  public commandExport(): void {
    this.areaService.exportArea();
  }

  public commandClose(): void {
    this.areaService.selectAreaData(this.areaService.NO_AREA);
    this.router.navigate(['/build'], {});
  }

  changeSuccessMessage() {
    this.errorDisplay = '';
  }
}
