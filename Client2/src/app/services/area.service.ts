import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IArea} from "omud/dist-lib/area";
import {IActorSpawn, IItemSpawn} from "omud/dist-lib/reset";
import {IRoom} from "omud/dist-lib/room";
import {IExit} from "omud/dist-lib/exit";
import {IActorTemplate} from "omud/dist-lib/actor";
import {IItemTemplate} from "omud/dist-lib/item";
import {IGameScript} from "omud/dist-lib/events/events";
import {nameGen, raceGen} from "./generator.service";
import {RaceService} from "diku-rom/dist/rom/race.service";
import {Direction, reverseDirection} from "diku-rom/dist/shared/direction";
import {BitFlag} from "diku-rom/dist/shared/bit-flag";
import {RomObjType} from "diku-rom/dist/rom/rom-obj-type";
import {Dice} from "diku-rom/dist/shared/dice";
import {RomPosition} from "diku-rom/dist/rom/rom-position";
import {RomSex} from "diku-rom/dist/rom/rom-sex";
import {RomSize} from "diku-rom/dist/rom/rom-size";
import {RomArea} from "diku-rom/dist/rom/rom-area";

@Injectable()
export class AreaService {

  public readonly NO_AREA : IArea = {
    _id : "NONE",
    items: [],
    scripts: [],
    actors: [],
    header: {
      areaName: "NONE",
      lowVnum: 0,
      builder: "NONE",
      highVnum: 0
    },
    rooms: [],
    resets: {
      actors: [],
      items: []
    }
  };
  private scriptSubject = new BehaviorSubject<IGameScript | null>(null);
  public script$ = this.scriptSubject.asObservable();
  private roomSubject = new BehaviorSubject<IRoom | null>(null);
  public room$ = this.roomSubject.asObservable();
  private itemSubject = new BehaviorSubject<IItemTemplate | null>(null);
  public item$ = this.itemSubject.asObservable();
  private mobSubject = new BehaviorSubject<IActorTemplate | null>(null);
  public mob$ = this.mobSubject.asObservable();
  private areaSubject = new BehaviorSubject<IArea>(this.NO_AREA);
  public area$ = this.areaSubject.asObservable();
  private areaListSubject = new BehaviorSubject<IArea[]>([]);
  public areaList$ = this.areaListSubject.asObservable();
  private raceService = new RaceService();

  public hasSelectedArea(): boolean {
    return this.getCurrentArea() !== this.NO_AREA;
  }

  public getCurrentArea(): IArea {
    return this.areaSubject.value;
  }

  public getCurrentRoom(): IRoom | null {
    return this.roomSubject.value;
  }

  public selectAreaData(area: IArea): void {
    if( area === this.NO_AREA) {
      console.log("No area selected");
    }
    // TODO Ensure area has required data..
    if (!area.scripts) {
      area.scripts = [];
    }
    this.areaSubject.next(area);
  }

  constructor(private http: HttpClient) {
  }

  public buildUrl(target: string): string {
    return '/api/' + target;
  }

  public saveArea(): Observable<string> {
    const area = this.getCurrentArea();
    console.log(area);
    const url = this.buildUrl('areas');
    if (area._id) {
      return this.http.put<string>(url + "/" + area._id, area, {withCredentials: true});
    }
    return this.http.post<string>(url, area, {withCredentials: true});
  }

  public deleteArea(area: IArea): Observable<string> {
    const url = this.buildUrl('areas');
    return this.http.delete<string>(url + "/" + area._id, {withCredentials: true});
  }

  public loadAreas(): void {
    const url = this.buildUrl('areas');
    this.http.get<IArea[]>(url, {withCredentials: true}).pipe(
      map((data) => {
        return data;
      })
    ).subscribe((data: IArea[]) => {
        this.areaListSubject.next(data);
      }
    )
  }

  public newBasicArea(): IArea {
    return {
      _id: "",
      header: {
        areaName: "NewArea",
        lowVnum: 1000,
        builder: "Anonymous",
        highVnum: 1100
      },
      resets: {
        actors: [],
        items: []
      },
      rooms: [],
      items: [],
      actors: [],
      scripts: []
    };
  }

  public newRoom(): void {
    const room = this.createRoom();
    this.roomSubject.next(room);
  }

  public newScript(): void {
    const script = this.createScript();
    this.scriptSubject.next(script);
  }

  public newActor(randomize: boolean = false): void {
    const actor = this.createActor();
    if( randomize ) {
      actor.sex = 1;// SexRandom();
      const race = raceGen(this.raceService);
      actor.shortDesc = race.prefix + " " + race.name;
      actor.longDesc = race.prefix + " " + race.name + " is here.";
      actor.name = nameGen(1) + " " + race.name;
      actor.race = race.name;
      actor.size = race.defaultSize;
      actor.damageType = race.defaultDamage;
      console.log(actor.race);
    }
    this.mobSubject.next(actor);
  }

  public newItem(): void {
    const item = this.createItem();
    this.itemSubject.next(item);
  }

  public canEditArea() {
    return !!this.getCurrentArea();
  }

  public canEditHeader() {
    const area = this.getCurrentArea();
    return area && area.rooms.length === 0 && area.items.length === 0 && area.actors.length === 0;
  }

  public selectRoom(selected: IRoom | null) {
    if (!selected && this.getCurrentArea().rooms.length > 0) {
      selected = this.getCurrentArea().rooms[0];
    }
    if (selected) {
      this.roomSubject.next(selected);
    }
  }

  public selectScript(selected: IGameScript | null) {
    if (!selected && this.getCurrentArea().scripts.length > 0) {
      selected = this.getCurrentArea().scripts[0];
    }
    if (selected) {
      this.scriptSubject.next(selected);
    }
  }

  public selectActor(selected: IActorTemplate | null) {
    if (!selected && this.getCurrentArea().actors.length > 0) {
      selected = this.getCurrentArea().actors[0];
    }
    if (selected) {
      this.mobSubject.next(selected);
    }
  }

  public selectItem(selected: IItemTemplate | null) {
    if (!selected && this.getCurrentArea().items.length > 0) {
      selected = this.getCurrentArea().items[0];
    }
    if (selected) {
      this.itemSubject.next(selected);
    }
  }

  public getExit(direction: Direction, room: IRoom | null = this.getCurrentRoom()): IExit | null {
    if (room) {
      return room.exits?.find((fExit) => fExit.direction === direction) || null;
    }
    return null;
  }

  public removeExit(direction: Direction): void {
    const room = this.getCurrentRoom();
    if (room) {
      const exitIdx = room.exits.findIndex((fExit) => fExit.direction === direction);
      if (exitIdx >= 0) {
        room.exits.splice(exitIdx, 1);
      }
    }
  }

  public setExit(room: IRoom, exit: IExit): void {
    if (room.exits == null) {
      room.exits = [];
      room.exits.push(exit);
      return;
    }
    const index = room.exits.findIndex((fExit) => fExit.direction === exit.direction);
    if (index === -1) {
      room.exits.push(exit);
      return;
    }
    room.exits = room.exits.splice(index, 1, exit);

  }

  newExit(room: IRoom, direction: Direction, toVnum: number) {
    const exit: IExit = {
      toVnum: toVnum,
      direction: direction,
      exitFlags: 0,
      defaultExitFlags: 0,
      description: "",
      keyword: "",
      keyVnum: 0
    }
    this.setExit(room, exit);
  }

  moveRoom(direction: Direction) {
    const exit = this.getExit(direction);
    if (exit) {
      let room = this.findRoomByVnum(exit.toVnum);
      if (room) {
        this.roomSubject.next(room);
      }
    }
  }

  public findRoomByVnum(vnum: number): IRoom | undefined {
    return this.getCurrentArea().rooms.find(room => room.vnum === vnum);
  }

  // What rooms are available "west" of this room
  // What rooms do not have an east exit.
  // Also, not THIS room?
  public roomsAvailableInDirection(direction: Direction): IRoom[] {
    const reverse = reverseDirection[direction];
    const current = this.getCurrentRoom();
    return this.getCurrentArea().rooms.filter((room) => {
      const exit = this.getExit(reverse, room);
      return room != current && (exit?.toVnum === current?.vnum || !exit);
    })
  }

  public connectRoom(direction: Direction, vnum: number): number {
    const fromRoom = this.getCurrentRoom();
    const toRoom = this.findRoomByVnum(vnum);
    if (fromRoom && toRoom) {
      this.newExit(fromRoom, direction, toRoom.vnum);
      this.newExit(toRoom, reverseDirection[direction], fromRoom.vnum);
      return toRoom.vnum;
    }
    return vnum;
  }

  public digRoom(direction: Direction) {
    const fromRoom = this.getCurrentRoom();
    if (fromRoom) {
      const toRoom = this.createRoom();
      this.newExit(fromRoom, direction, toRoom.vnum);
      this.newExit(toRoom, reverseDirection[direction], fromRoom.vnum);
      return toRoom.vnum;
    }
    return -1;
  }

  private createRoom(): IRoom {
    let vnum = this.getCurrentArea().header.lowVnum;
    let rooms = this.getCurrentArea().rooms;
    rooms.forEach((room) => {
      if (room.vnum > vnum) {
        vnum = room.vnum;
      }
    });
    let room: IRoom = {
      flags: 0,
      vnum: vnum + 1,
      name: `Room ${vnum + 1}`,
      description: "",
      sector: 0,
      exits: [],
      mapX: -1, // Store where it is graphically :-)
      mapY: -1,
      triggers: [],
      tags: [],
      properties: {}
    }
    rooms.push(room);
    return room;
  }

  private createScript(): IGameScript {
    let vnum = this.getCurrentArea().header.lowVnum;
    let scripts = this.getCurrentArea().scripts;
    if (!scripts) {
      this.getCurrentArea().scripts = [];
      scripts = this.getCurrentArea().scripts;
    }
    scripts.forEach((script) => {
      if (script.vnum > vnum) {
        vnum = script.vnum;
      }
    });
    let script: IGameScript = {
      vnum: vnum + 1,
      name: `Script ${vnum + 1}`,
      script: "",
    }
    scripts.push(script);
    return script;
  }

  private createItem(): IItemTemplate {
    let vnum = this.getCurrentArea().header.lowVnum;
    let items = this.getCurrentArea().items;
    items.forEach((item) => {
      if (item.vnum > vnum) {
        vnum = item.vnum;
      }
    });
    let item: IItemTemplate = {
      condition: 100,
      cost: 0,
      level: 1,
      longDesc: "An item lies here.",
      material: "paper",
      shortDesc: "an item",
      wearFlags: BitFlag.A, // "Take"
      weight: 0,
      type: RomObjType.ITEM_TRASH,
      vnum: vnum + 1,
      name: `Item ${vnum + 1}`,
      description: "",
      triggers: [],
      tags: [],
      properties: {}
    }
    items.push(item);
    return item;
  }

  public asIbrekenielArea(area: RomArea): IArea {
    return {
      _id: "",
      items: area.objects.map((romObject) => {
        return {
          vnum: romObject.vnum,
          properties: {},
          tags: ["animud"],
          name: romObject.name,
          description: romObject.description,
          triggers: [],
          cost: romObject.cost,
          level: romObject.level,
          longDesc: romObject.longDesc,
          material: romObject.material,
          type: romObject.type,
          shortDesc: romObject.shortDesc,
          weight: romObject.weight,
          wearFlags: romObject.wearFlags,
          condition: romObject.condition
        }
      }),
      scripts: [],
      actors: area.mobs.map( (romActor) => {
        return {
          vnum: romActor.vnum,
          properties: {},
          tags: ["animud"],
          name: romActor.name,
          description: romActor.description,
          triggers: [],
          shortDesc: romActor.shortDesc,
          damageType: romActor.damageType,
          sex: romActor.sex,
          race: romActor.race.name,
          level: romActor.level,
          longDesc: romActor.longDesc,
          size: romActor.size,
          wealth: romActor.wealth,
          hitRoll: romActor.hitRoll,
          defaultPosition: romActor.defaultPosition,
          startPosition: romActor.startPosition,
          // These will go away...
          hitDice: romActor.hitDice,
          manaDice: romActor.manaDice,
          damageDice: romActor.damageDice,
          alignment: romActor.alignment,
          armor: romActor.armor,
          group: romActor.group
        }
      }),
      header: {
        areaName: area.header.areaName,
        lowVnum: area.header.lowVnum,
        builder: area.header.builder,
        highVnum: area.header.highVnum
      },
      rooms: area.rooms.map(( romRoom) => {
        return {
          vnum: romRoom.vnum,
          properties: {},
          tags: ["animud"],
          mapX: -1,
          mapY: -1,
          triggers: [],
          exits: romRoom.exits.map( (exit) => {
            return {
              toVnum: exit.toVnum,
              direction: exit.direction,
              exitFlags: exit.exitFlags,
              description: exit.description,
              keyVnum: exit.keyVnum,
              keyword: exit.keyword,
              defaultExitFlags: exit.exitFlags
            };
          }),
          flags: romRoom.flags,
          name: romRoom.name,
          description: romRoom.description,
          sector: romRoom.sector
        }
      }),
      resets: {
        actors: area.resets.filter((reset) => reset.command === 'M').map((reset)=> {
          return {
            maxRoom: reset.arg4,
            maxWorld: reset.arg2,
            roomVnum: reset.arg3,
            actorVnum: reset.arg1
          }
        }),
        items:  area.resets.filter((reset) => reset.command === 'O').map((reset)=> {
          return {
            maxRoom: reset.arg4,
            maxWorld: reset.arg2,
            roomVnum: reset.arg3,
            itemVnum: reset.arg1
          }
        })
      }
    };
  }

  private createActor(): IActorTemplate {
    let vnum = this.getCurrentArea().header.lowVnum;
    let actors = this.getCurrentArea().actors;
    actors.forEach((actors) => {
      if (actors.vnum > vnum) {
        vnum = actors.vnum;
      }
    });
    let actor: IActorTemplate = {
      alignment: 0,
      armor: [0, 0, 0, 0],
      damageDice: new Dice(1, 1, 1),
      damageType: "",
      defaultPosition: RomPosition.POS_STANDING,
      group: 0,
      hitDice: new Dice(1, 1, 1),
      hitRoll: 1,
      level: 1,
      longDesc: "An actor stands here.",
      manaDice: new Dice(1, 1, 1),
      race: "human",
      sex: RomSex.SEX_NEUTRAL,
      shortDesc: "an actor",
      size: RomSize.SIZE_MEDIUM,
      startPosition: RomPosition.POS_STANDING,
      wealth: 0,
      vnum: vnum + 1,
      name: `Actor ${vnum + 1}`,
      description: "",
      triggers: [],
      tags: [],
      properties: {}
    }
    actors.push(actor);
    return actor;
  }

  scriptsAvailable(): IGameScript[] {
    return this.getCurrentArea().scripts;
  }

  public getActorResetsFor(vnum: number): IActorSpawn[] {
    return this.getCurrentArea().resets.actors.filter(x => x.roomVnum === vnum);
  }

  public getItemResetsFor(vnum: number): IItemSpawn[] {
    return this.getCurrentArea().resets.items.filter(x => x.roomVnum === vnum);
  }

  public newActorSpawn(roomVnum: number, resetActor: number): void {
    const newSpawn = {
      actorVnum: resetActor,
      roomVnum: roomVnum,
      maxWorld: 1,
      maxRoom: 1
    };
    console.log(newSpawn);
    this.getCurrentArea().resets.actors.push(newSpawn);
  }

  public newItemSpawn(roomVnum: number, resetItem: number): void {
    const newSpawn = {
      itemVnum: resetItem,
      roomVnum: roomVnum,
      maxWorld: 1,
      maxRoom: 1
    };
    console.log(newSpawn);
    this.getCurrentArea().resets.items.push(newSpawn);
  }

  public deleteActorSpawn(spawn: IActorSpawn) {
    const resets = this.getCurrentArea().resets;
    const index = resets.actors.findIndex(x => x === spawn);
    if( index !== undefined && index >= 0 ) {
      resets.actors.splice(index,1);
    }
  }

  public deleteItemSpawn(spawn: IItemSpawn) {
    const resets = this.getCurrentArea().resets;
    const index = resets.items.findIndex(x => x === spawn);
    if( index !== undefined && index >= 0 ) {
      resets.items.splice(index,1);
    }
  }

  public syncExit(exit: IExit): void {
    let oppDir = reverseDirection[exit.direction];
    let oppRoom = this.findRoomByVnum(exit.toVnum);
    let oppExit = this.getExit(oppDir, oppRoom);
    if( oppExit ) {
      // do not want to sync "hidden", as that is "one side" ?
      oppExit.defaultExitFlags = exit.defaultExitFlags;
    }
  }

  public exportArea(): void {
    this.downloadObjectAsJson(this.getCurrentArea(), this.getCurrentArea().header.areaName);
  }

  public downloadObjectAsJson(exportObj: any, exportName: string): void{
    const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj));
    const downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href",     dataStr);
    downloadAnchorNode.setAttribute("download", exportName + ".json");
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
  }
}
