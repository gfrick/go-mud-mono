import {Injectable} from "@angular/core";
import {BehaviorSubject, catchError, map, Observable, of, tap} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Account, NewAccount} from "../../domain/account";
import {Player} from "../../domain/player";
import {CookieService} from "ngx-cookie-service";

@Injectable()
export class AccountService {

  private loggedIn = new BehaviorSubject<boolean>(false);
  public loggedIn$ = this.loggedIn.asObservable();
  private permissions: string[];

  constructor(private http: HttpClient, private cookieService: CookieService) {
    this.permissions = [];
  }

  public buildUrl(target: string): string {
    return '/api/' + target;
  }

  public login(accountInfo: Account): Observable<boolean> {
    return this.http.post<boolean>(
      this.buildUrl('auth/login'), accountInfo, {withCredentials: true})
      .pipe(
        map((result: boolean) => {
          const success = result && this.checkCookie();
          if (success) {
            const cookie = this.cookieService.get('account');
            const value = JSON.parse(atob(cookie));
            this.permissions = value.permissions || [];
          }
          return success;
        }),
        tap((result: boolean) => {
          this.loggedIn.next(result);
        }),
        catchError((failure) => {
          console.error(failure);
          return of(false);
        })
      );
  }

  public createPlayer(playerInfo: Player): Observable<boolean> {
    return this.http.post<string>(
      this.buildUrl('players'), playerInfo, {withCredentials: true})
      .pipe(
        map((result: string) => {
          const id = parseInt(result, 10);
          return (id > 0);
        }),
        catchError((failure) => {
          console.error(failure);
          return of(false);
        })
      );
  }

  public logout(): Observable<boolean> {
    return this.http.get<boolean>(
      this.buildUrl('auth/logout'), {withCredentials: true})
      .pipe(
        map(() => {
          return this.checkCookie() === false;
        }),
        tap((result) => {
          this.cookieService.delete('account');
          this.cookieService.delete('token');
          this.loggedIn.next(false);
        }),
        catchError((failure) => {
          console.error(failure);
          return of(false);
        })
      );
  }

  public getPlayers(): Observable<any> {
    return this.http.get(this.buildUrl('players'), {withCredentials: true}).pipe(
      map((result: any) => {
        return result;
      }),
      catchError((failure) => {
        console.error(failure);
        return of([]);
      })
    );
  }

  public checkPermission(permission: string): boolean {
    return !!(this.permissions.find(x => x === permission));
  }

  public checkCookie(): boolean {
    return this.cookieService.check('account');
  }

  public createAccount(newAccount: NewAccount): Observable<boolean> {
    return this.http.post<string>(
      this.buildUrl('accounts'), newAccount, {withCredentials: true})
      .pipe(
        map((result: string) => {
          const id = parseInt(result, 10);
          return (id > 0);
        }),
        catchError((failure) => {
          console.error(failure);
          return of(false);
        })
      );
  }

}
