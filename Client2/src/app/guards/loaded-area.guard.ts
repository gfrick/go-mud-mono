import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {AreaService} from "../services/area.service";

@Injectable()
export class LoadedAreaGuard implements CanActivate {

  constructor(private router: Router, private areaService: AreaService) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!route || !route.routeConfig) {
      // No Route, Allow.
      return true;
    }

    if (route.url[0].path === "build") {
      if (this.areaService.hasSelectedArea()) {
        this.router.navigate(['/editor'], {});
        return false;
      }
      return true;
    }

    if (route.url[0].path === "editor") {
      if (!this.areaService.hasSelectedArea()) {
        this.router.navigate(['/build'], {});
        return false;
      }
      return true;
    }

    return true;
  }
}
