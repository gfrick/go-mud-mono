import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {AccountService} from "../services/account.service";
import {AreaService} from "../services/area.service";

@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(private router: Router, private accountService: AccountService,
              private areaService: AreaService) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if( !route || !route.routeConfig ) {
      // No Route, Allow.
      return true;
    }
    const desiredRoute = route.routeConfig.path || '/';
    if (this.accountService.checkCookie()) {
      return true;
    }
    this.router.navigate(['/account/login'],
      {
        queryParams: {
          return: desiredRoute
        }
      });
    this.areaService.selectAreaData(this.areaService.NO_AREA);
    return false;
  }
}
