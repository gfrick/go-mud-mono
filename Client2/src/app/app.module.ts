import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {CookieService} from "ngx-cookie-service";
import {AccountService} from "./services/account.service";
import {HttpClientModule} from "@angular/common/http";
import {HomeComponent} from "./routed-pages/home/home.component";
import {AccountComponent} from "./routed-pages/account/account.component";
import {LoginComponent} from "./routed-pages/account/login/login.component";
import {LogoutComponent} from "./routed-pages/account/logout/logout.component";
import {CreateComponent} from "./routed-pages/account/create/create.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DashboardComponent} from "./routed-pages/dashboard/dashboard.component";
import {CreatePlayerComponent} from "./routed-pages/dashboard/create-player/create-player.component";
import {AuthenticationGuard} from "./guards/authentication.guard";
import {ClientComponent} from "./routed-pages/client/client.component";
import {GameRoomComponent} from "./routed-pages/client/templates/room/game-room.component";
import {ImporterComponent} from "./components/importer/importer.component";
import {EditorHomeComponent} from "./routed-pages/editor-home/editor-home.component";
import {AreaComponent} from "./components/area/area.component";
import {AreaService} from "./services/area.service";
import {RoomNavComponent} from "./components/room/room-nav.component";
import {RoomComponent} from "./components/room/room.component";
import {ExitEditorComponent} from "./components/room/exit-editor.component";
import {BitFlagEditorComponent} from "./components/bit-flag/bit-flag-editor.component";
import {MobEditorComponent} from "./components/actor/mob-editor.component";
import {MobNavComponent} from "./components/actor/mob-nav.component";
import {WandEditorComponent} from "./components/item/value-editors/wand-editor.component";
import {LightEditorComponent} from "./components/item/value-editors/light-editor.component";
import {ItemValuesEditorComponent} from "./components/item/value-editors/item-values-editor.component";
import {ItemEditorComponent} from "./components/item/obj.component";
import {ObjNavComponent} from "./components/item/obj-nav.component";
import {MapComponent} from "./components/map/map.component";
import {TriggerEditorComponent} from "./components/trigger/trigger-editor.component";
import {ScriptNavComponent} from "./components/script/script-nav.component";
import {ScriptEditorComponent} from "./components/script/script-editor.component";
import {ActorSpawnEditorComponent} from "./components/reset/actor-spawn-editor.component";
import {TagPropEditorComponent} from "./components/tag-props/tag-prop-editor.component";
import {ItemSpawnEditorComponent} from "./components/reset/item-spawn-editor.component";
import {EditorComponent} from "./routed-pages/editor/editor.component";
import {LoadedAreaGuard} from "./guards/loaded-area.guard";
import {EquipmentComponent} from "./routed-pages/client/templates/equipment/equipment.component";
import {ScoreComponent} from "./routed-pages/client/templates/score/score.component";

const appRoutes: Routes = [
  {
    path: 'build',
    component: EditorHomeComponent,
    canActivate: [AuthenticationGuard, LoadedAreaGuard]
  },
  {
    path: 'editor',
    component: EditorComponent,
    canActivate: [AuthenticationGuard, LoadedAreaGuard]
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'play',
    component: ClientComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'account',
    component: AccountComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'logout',
        component: LogoutComponent
      },
      {
        path: 'create',
        component: CreateComponent
      },
      {
        path: 'create-player',
        component: CreatePlayerComponent
      },
      {
        path: '',
        redirectTo: 'create',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  declarations: [
    AccountComponent,
    AppComponent,
    AreaComponent,
    BitFlagEditorComponent,
    ClientComponent,
    CreateComponent,
    CreatePlayerComponent,
    DashboardComponent,
    EditorComponent,
    EditorHomeComponent,
    ExitEditorComponent,
    EquipmentComponent,
    GameRoomComponent,
    HomeComponent,
    ImporterComponent,
    ItemEditorComponent,
    ItemSpawnEditorComponent,
    ItemValuesEditorComponent,
    LightEditorComponent,
    LoginComponent,
    LogoutComponent,
    MapComponent,
    MobEditorComponent,
    MobNavComponent,
    ObjNavComponent,
    ActorSpawnEditorComponent,
    RoomComponent,
    RoomNavComponent,
    ScoreComponent,
    ScriptNavComponent,
    ScriptEditorComponent,
    TagPropEditorComponent,
    TriggerEditorComponent,
    WandEditorComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {useHash: true, enableTracing: false} // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AccountService, AreaService, AuthenticationGuard, LoadedAreaGuard, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
