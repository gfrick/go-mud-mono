import {Component, ViewChild} from '@angular/core';
import {IArea} from "omud/dist-lib/area";
import {AreaService} from "../../services/area.service";
import {Router} from "@angular/router";
import {RomAreaParser} from "diku-rom/dist/rom/rom-area.parser";
import {RomAreaExporter} from "diku-rom/dist/rom/rom-area.exporter";
import {RomArea} from "diku-rom/dist/rom/rom-area";

enum FileType {
  AREA,
  JSON
}

@Component({
  selector: 'area-file-importer',
  templateUrl: 'importer.component.html',
  styleUrls: ['importer.component.css']
})
export class ImporterComponent {

  @ViewChild('fileImportInput', {static: false})
  fileImportInput: any;

  private parser: RomAreaParser;

  constructor(private router: Router, private areaService: AreaService) {
    this.parser = new RomAreaParser();
  }

  public fileChangeListener($event: any): void {
    const input = $event.target;
    if (!input.files || input.files.length === 0) {
      return;
    }
    const name = input.files[0].name;
    const reader = new FileReader();
    const fileType = this.determineFileType(name);
    reader.onload = () => {
      const rawData = reader.result;
      console.log(rawData);
      let areaData: IArea;
      if (fileType === FileType.AREA) {
        const romArea = this.parser.readRawData(rawData, input.files[0].name);
        console.log(romArea);
        areaData = this.areaService.asIbrekenielArea(romArea);
      } else {
         areaData = JSON.parse(rawData as string);
        console.log(areaData);
      }
      this.fileReset();
      if( areaData ) {
        this.areaService.selectAreaData(areaData);
        this.router.navigate(['/editor'], {});
      }
    };
    reader.onerror = function () {
      alert('Unable to read ' + input.files[0]);
    };
    reader.readAsText(input.files[0]);
  }

  public determineFileType(name: string): FileType {
    if (name.indexOf(".") < 1) {
      return FileType.AREA;
    }
    const nameExt = name.split(".");
    const ext = nameExt[nameExt.length - 1];
    if (ext.toLowerCase() === 'json') {
      return FileType.JSON;
    }
    return FileType.AREA;
  }

  public getAreaAsJSON(area: RomArea): any {
    const jsonExporter = new RomAreaExporter();
    return jsonExporter.getAreaAsJSON(area);
  }

  public fileReset(): void {
    this.fileImportInput.nativeElement.value = '';
  }
}
