import {Component, OnInit} from '@angular/core';
import {filter} from "rxjs";
import {AreaService} from "../../services/area.service";
import {IArea} from "omud/dist-lib/area";
import {IItemTemplate} from "omud/dist-lib/item";

@Component({
  selector: 'obj-nav',
  templateUrl: 'obj-nav.component.html'
})
export class ObjNavComponent implements OnInit {

  public area!: IArea;
  public selected: IItemTemplate | null = null;

  constructor(private areaService: AreaService) {
  }

  public ngOnInit() {
    this.areaService.area$.pipe(
      filter((selected) => selected !== undefined)
    ).subscribe((area) => {
      this.area = area;
      if (!this.selected) {
        this.commandSelect();
      }
    });
    this.areaService.item$.subscribe((item) => {
      this.selected = item;
    });
  }

  public commandSelect(): void {
    this.areaService.selectItem(this.selected);
  }

  public commandNew(): void {
    this.areaService.newItem();
  }

  public commandDelete(): void {
  }

  public get items(): IItemTemplate[] {
    if (this.area) {
      return this.area.items;
    }
    return [];
  }
}
