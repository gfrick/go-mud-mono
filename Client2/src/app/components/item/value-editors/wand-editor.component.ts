import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';

@Component({
  selector: 'wand-editor',
  templateUrl: 'wand-editor.component.html'
})
export class WandEditorComponent implements OnInit, OnDestroy {

  @Input()
  public parentGroup!: FormGroup;

  public lightForm = new FormGroup({
    v0: new FormControl(),
    v1: new FormControl(),
    v2: new FormControl(),
    v3: new FormControl(),
  });
  private parentSubscription!: Subscription;

  constructor() {

  }

  public ngOnInit() {
    this.parentSubscription = this.parentGroup.get('values')!.valueChanges.subscribe((val) => {
      this.updateFormValue(val);
    });

    this.lightForm.valueChanges.subscribe((val) => {
      const allValues = this.parentGroup.get('values')!.value;
      allValues[0] = +val.v0;
      allValues[1] = +val.v1;
      allValues[2] = +val.v2;
      allValues[3] = val.v3;
      this.parentGroup.get('values')!.setValue(allValues);
    });

    this.updateFormValue(this.parentGroup.get('values')!.value);
  }

  public ngOnDestroy() {
    if (this.parentSubscription) {
      this.parentSubscription.unsubscribe();
    }
  }

  public updateFormValue(val: any) {

    this.lightForm.patchValue({
      v0: val[0],
      v1: val[1],
      v2: val[2],
      v3: val[3]
    }, {
      onlySelf: true,
      emitEvent: false
    });

  }

}
