import {Component, Input, OnDestroy, OnInit, OnChanges} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Subscription} from "rxjs";

@Component({
  selector: 'item-values-editor',
  templateUrl: 'item-values-editor.component.html'
})
export class ItemValuesEditorComponent implements OnInit, OnDestroy, OnChanges {

  @Input()
  public itemType!: number;

  @Input()
  public parentGroup!: FormGroup;

  public valuesForm = new FormGroup({
    v0: new FormControl(),
    v1: new FormControl(),
    v2: new FormControl(),
    v3: new FormControl(),
    v4: new FormControl()
  });
  private parentSubscription!: Subscription;

  constructor() {
  }

  public ngOnChanges() {
    this.reactToItemType();
  }

  public ngOnInit() {

    this.reactToItemType();
    this.parentSubscription = this.parentGroup.get('values')!.valueChanges.subscribe((val) => {
      this.updateFormValue(val);
    });

    this.updateFormValue(this.parentGroup.get('values')!.value);
  }

  public reactToItemType() {

  }

  public ngOnDestroy() {
    if (this.parentSubscription) {
      this.parentSubscription.unsubscribe();
    }
  }

  public updateFormValue(val: any) {
  }

}
