import {Component, Input,  OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';

@Component({
  selector: 'light-editor',
  templateUrl: 'light-editor.component.html'
})
export class LightEditorComponent implements OnInit, OnDestroy {

  @Input()
  public parentGroup!: FormGroup;

  public lightForm = new FormGroup({
    v3: new FormControl()
  });
  private parentSubscription!: Subscription;

  constructor() {
  }

  public ngOnInit() {
    this.parentSubscription = this.parentGroup.get('values')!.valueChanges.subscribe((val) => {
      this.updateFormValue(val);
    });

    this.lightForm.valueChanges.subscribe((val) => {
      const allValues = this.parentGroup.get('values')!.value;
      allValues[3] = +val.v3;
      this.parentGroup.get('values')!.setValue(allValues);
    });

    this.updateFormValue(this.parentGroup.get('values')!.value);
  }

  public ngOnDestroy() {
    if (this.parentSubscription) {
      this.parentSubscription.unsubscribe();
    }
  }

  public updateFormValue(val: any) {
    this.lightForm.get('v3')!.setValue(val[3], {
      onlySelf: true,
        emitEvent: true,
        emitModelToViewChange: true,
        emitViewToModelChange: false
    });

  }

}
