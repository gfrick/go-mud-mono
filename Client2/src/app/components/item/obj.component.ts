import {Component, OnInit} from '@angular/core';
import {  FormControl, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';
import {AreaService} from "../../services/area.service";
import {IItemTemplate} from "omud/dist-lib/item";
import {ITrigger, TriggerType} from "omud/dist-lib/events/events";
import {WearFlags} from "diku-rom/dist/shared/wear-flags";
import {ExtraFlags} from "diku-rom/dist/shared/extra-flags";
import {RomObjType} from "diku-rom/dist/rom/rom-obj-type";

@Component({
  selector: 'app-obj-editor',
  templateUrl: 'obj.component.html',
  styleUrls: ['obj.component.css']
})
export class ItemEditorComponent implements OnInit {

  public objForm = new FormGroup({
    vnum: new FormControl(),
    name: new FormControl(),
    shortDesc: new FormControl(),
    longDesc: new FormControl(),
    level: new FormControl(),
    material: new FormControl(),
    condition: new FormControl(),
    cost: new FormControl(),
    weight: new FormControl(),
    type: new FormControl(),
    values: new FormControl()
  });

  public wearFlag: number = 0;
  public subscription!: Subscription;
  public selectedObject: IItemTemplate | null = null;
  public wearFlags = WearFlags.wearFlags;
  public extraFlags = ExtraFlags.extraFlags;
  public itemTypeSet = RomObjType.itemTypeSet;

  constructor(private areaService: AreaService) {
  }

  public ngOnInit() {
    this.areaService.item$.subscribe( (item) => {
      this.onSelectionCommand(item);
    });
    this.objForm.valueChanges.subscribe((val) => {
      console.log(JSON.stringify(val));
      if (this.selectedObject && val) {
        Object.assign(this.selectedObject, val);
      }
    });
  }

  public updateWearFlag(newValue: number): void {
    this.wearFlag = newValue;
    if( this.selectedObject ) {
      this.selectedObject.wearFlags = newValue;
    }
  }

  public onSelectionCommand(obj: IItemTemplate | null): void {
    if (obj) {
      this.selectedObject = obj;
      this.objForm.patchValue(obj);
      this.wearFlag = obj.wearFlags;
    } else {
      this.selectedObject = null;
      this.objForm.reset();
      this.wearFlag = 0;
    }
  }

  public commandAddTrigger(): void {
    if (this.selectedObject) {
      if (!this.selectedObject.triggers) {
        this.selectedObject.triggers = [];
      }
      this.selectedObject.triggers.push({
        vnum: 0,
        type: TriggerType.Enter,
        script: 0,
        typeArg: ""
      })
    }
  }

  public commandDeleteTrigger(trigger: ITrigger): void {
    const index = this.selectedObject?.triggers.indexOf(trigger);
    if (index !== undefined && index >= 0) {
      this.selectedObject?.triggers.splice(index, 1);
    }
  }
}
