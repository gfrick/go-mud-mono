import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AreaService} from "../../services/area.service";
import {IHasProperties} from "omud/dist-lib/events/events";

@Component({
  selector: 'app-tag-editor',
  templateUrl: 'tag-prop-editor.component.html'
})
export class TagPropEditorComponent implements OnInit, OnChanges {

  public form!: FormGroup;

  @Input()
  public entity: IHasProperties | null = null;

  constructor(public areaService: AreaService, private fb: FormBuilder) {
    this.form = new FormGroup({
      tags: this.fb.array([]),
      properties: this.fb.array([])
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("tagprop onchanges");
    const entity = changes['entity'];
    this.onSelectionCommand(changes['entity'].currentValue);
  }

  public ngOnInit() {
    console.log("init?");
    this.form.valueChanges.subscribe((val) => {
      if (this.entity && val) {
        console.log(JSON.stringify(val));
        Object.assign(this.entity,val);
      }
    });
  }

  createPropertyElement(keyVal: any = '', valVal: any = ''): FormGroup {
    return this.fb.group(
      {
        key: this.fb.control(keyVal),
        value: this.fb.control(valVal)
      }
    );
  }

  addProperty(): void {
    this.properties.push(this.createPropertyElement());
  }

  get properties(): FormArray {
    return this.form.get('properties') as FormArray;
  }

  deleteProperty(i: number): void {
    if (this.properties.length !== 1) {
      this.properties.removeAt(i);
    }
  }

  createTagElement(value: any = ''): FormControl {
    return this.fb.control(value, Validators.required);
  }

  addTag(): void {
    this.tags.push(this.createTagElement());
  }

  get tags(): FormArray {
    return this.form.get('tags') as FormArray;
  }

  deleteTag(i: number): void {
    if (this.tags.length !== 1) {
      this.tags.removeAt(i);
    }
  }

  public onSelectionCommand(entity: IHasProperties | null): void {
    if (entity) {
      this.tags.clear({
        emitEvent: false
      });
      if (entity.tags && entity.tags.length) {
        entity.tags.forEach((tag) => {
          this.tags.push(this.createTagElement(tag), {
            emitEvent: false
          });
        });
      }
      // 2. Reset properties.
      this.properties.clear({
        emitEvent: false
      });
      if (entity.properties ) {
        for (let propertiesKey in entity.properties) {
          this.properties.push(this.createPropertyElement(propertiesKey, entity.properties[propertiesKey]), {
            emitEvent: false
          });
        }
      }
    } else {
      this.tags.clear();
      this.properties.clear();
    }
  }
}
