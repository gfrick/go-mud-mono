import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {AreaService} from "../../services/area.service";
import {AllTriggers, IGameScript, ITrigger, ITriggerInfo, TriggerType} from "omud/dist-lib/events/events";

@Component({
  selector: 'app-trigger-editor',
  templateUrl: 'trigger-editor.component.html'
})
export class TriggerEditorComponent implements  OnInit, OnChanges, OnDestroy {

  @Input()
  public trigger!: ITrigger;

  public triggerForm = new FormGroup({
    script: new FormControl(''),
    type: new FormControl(''),
    typeArg: new FormControl('')
  });

  constructor(private areaService: AreaService) {
  }

  public ngOnInit() {

    this.triggerForm.valueChanges.subscribe((val) => {
      if( this.trigger ) {
        this.trigger.script = parseInt(val.script, 10);
        this.trigger.type = val.type;
        this.trigger.typeArg = val.typeArg;
      }
    });

    this.updateFormValue();
  }

  public ngOnChanges() {
  }

  public ngOnDestroy() {
  }

  public updateFormValue() {

    if (!this.trigger) {
      this.triggerForm.patchValue({
        script: 0,
        type: TriggerType.Enter,
        typeArg: ""
      });
      return;
    }
    this.triggerForm.patchValue(this.trigger);
  }

  public updateTrigger(): void {

  }

  public scriptList(): IGameScript[] {
    return this.areaService.scriptsAvailable();
  }

  public get allTriggers(): ITriggerInfo[] {
    return AllTriggers;
  }
}
