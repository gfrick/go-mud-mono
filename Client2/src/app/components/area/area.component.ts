import {Component, OnInit} from "@angular/core";
import {FormControl, FormGroup} from "@angular/forms";
import {filter} from "rxjs";
import {AreaService} from "../../services/area.service";
import {IArea} from "omud/dist-lib/area";
import {nameGenPlace1} from "../../services/generator.service";

@Component({
  selector: 'app-area',
  templateUrl: 'area.component.html',
  styleUrls: ['area.component.css']
})
export class AreaComponent implements OnInit {

  public area?: IArea;
  public areas: Array<IArea> = [];

  public areaForm = new FormGroup({
    areaName: new FormControl(),
    builder: new FormControl(),
    lowVnum: new FormControl(),
    highVnum: new FormControl()
  });

  constructor(private areaService: AreaService) {
  }

  public ngOnInit() {
    this.areaService.loadAreas();
    this.areaService.areaList$
      .pipe(
        filter((isLoaded) => isLoaded.length > 0)
      )
      .subscribe((areas: IArea[]) => {
        this.areas = areas;
    });
    this.areaService.area$.pipe(
      filter( (selected) => selected !== undefined),
    ).subscribe( (area) => {
      this.area = area;
      this.areaForm.patchValue(this.area.header, {emitEvent: false});
    });
    this.areaForm.valueChanges.subscribe((val) => {
      if (this.area && val) {
        Object.assign(this.area.header, val)
      }
    });
  }
  public commandRandomName() : void {
    this.areaForm.patchValue({
      areaName: nameGenPlace1()
    });
  }

  public canEditArea(): boolean {
    return !!this.area;
  }

  public nonEmptyArea(): boolean {
    return !this.areaService.canEditHeader();
  }

}
