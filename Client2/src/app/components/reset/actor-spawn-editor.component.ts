import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {AreaService} from "../../services/area.service";
import {IActorSpawn} from "omud/dist-lib/reset";
import {IActorTemplate} from "omud/dist-lib/actor";

@Component({
  selector: 'app-actor-spawn-editor',
  templateUrl: 'actor-spawn-editor.component.html'
})
export class ActorSpawnEditorComponent implements OnChanges, OnDestroy, OnInit {

  @Input()
  public reset!: IActorSpawn;

  public resetForm = new FormGroup({
    actorVnum: new FormControl(0),
    maxRoom: new FormControl(1),
    maxWorld: new FormControl(1)
  });

  constructor(private areaService: AreaService) {
  }

  public ngOnInit() {
    this.resetForm.valueChanges.subscribe((val) => {
      if( this.reset ) {
        this.reset.actorVnum = parseInt(val.actorVnum, 10);
        this.reset.maxRoom =  parseInt(val.maxRoom,10);
        this.reset.maxWorld =  parseInt(val.maxWorld, 10);
      }
    });
    this.updateFormValue();
  }

  public ngOnChanges() {
  }

  public ngOnDestroy() {
  }

  public updateFormValue() {
    if (!this.reset) {
      this.resetForm.patchValue({
        actorVnum: 0,
        maxRoom: 1,
        maxWorld: 1
      });
      return;
    }
    this.resetForm.patchValue(this.reset);
  }

  public updateSpawn(): void {

  }
  public get actors(): IActorTemplate[] {
    return this.areaService.getCurrentArea().actors;
  }
}
