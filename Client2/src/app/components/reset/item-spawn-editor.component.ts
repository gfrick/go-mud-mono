import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {AreaService} from "../../services/area.service";
import {IItemSpawn} from "omud/dist-lib/reset";
import {IItemTemplate} from "omud/dist-lib/item";

@Component({
  selector: 'app-item-spawn-editor',
  templateUrl: 'item-spawn-editor.component.html'
})
export class ItemSpawnEditorComponent implements OnChanges, OnDestroy, OnInit {

  @Input()
  public reset!: IItemSpawn;

  public resetForm = new FormGroup({
    itemVnum: new FormControl(0),
    maxRoom: new FormControl(1),
    maxWorld: new FormControl(1)
  });

  constructor(private areaService: AreaService) {
  }

  public ngOnInit() {
    this.resetForm.valueChanges.subscribe((val) => {
      if( this.reset ) {
        this.reset.itemVnum = parseInt(val.itemVnum, 10);
        this.reset.maxRoom =  parseInt(val.maxRoom,10);
        this.reset.maxWorld =  parseInt(val.maxWorld, 10);
      }
    });
    this.updateFormValue();
  }

  public ngOnChanges() {
  }

  public ngOnDestroy() {
  }

  public updateFormValue() {
    if (!this.reset) {
      this.resetForm.patchValue({
        itemVnum: 0,
        maxRoom: 1,
        maxWorld: 1
      });
      return;
    }
    this.resetForm.patchValue(this.reset);
  }

  public updateSpawn(): void {
  }

  public get items(): IItemTemplate[] {
    return this.areaService.getCurrentArea().items;
  }
}
