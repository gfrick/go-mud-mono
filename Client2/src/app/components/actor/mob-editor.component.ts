import {Component, OnInit} from '@angular/core';
import { FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';
import {AreaService} from "../../services/area.service";
import {IActorTemplate} from "omud/dist-lib/actor";
import {ITrigger, TriggerType} from "omud/dist-lib/events/events";
import {RomRace} from "diku-rom/dist/rom/rom-race";
import {RomPosition} from "diku-rom/dist/rom/rom-position";
import {RomDamageType} from "diku-rom/dist/rom/rom-damage-type";
import {RomSize} from "diku-rom/dist/rom/rom-size";
import {RaceService} from "diku-rom/dist/rom/race.service";

@Component({
  selector: 'app-mob-editor',
  templateUrl: 'mob-editor.component.html',
  styleUrls: ['mob-editor.component.css']
})
export class MobEditorComponent implements OnInit {

  public mobForm = new FormGroup({
    name: new FormControl(),
    description: new FormControl(),
    shortDesc: new FormControl(),
    longDesc: new FormControl(),
    size: new FormControl(),
    vnum: new FormControl(),
    level: new FormControl(),
    race: new FormControl(),
    hitRoll: new FormControl(),
    damageType: new FormControl(),
    startPosition: new FormControl(),
    defaultPosition: new FormControl(),
    sex: new FormControl(),
    wealth: new FormControl()
  });

  public subscription!: Subscription;
  public selectedMobile: IActorTemplate | null = null;
  public raceSet!: RomRace[];
  public positionSet = RomPosition.positionSet;
  public damageTypeSet = RomDamageType.damageTypeSet;
  public sizeSet = RomSize.sizeSet;
  private raceService: RaceService = new RaceService();

  constructor(private areaService: AreaService, private fb: FormBuilder ) {
    this.raceSet = this.raceService.getRaces();
  }

  public ngOnInit() {

    this.areaService.mob$.subscribe( (mob) => {
      this.onSelectionCommand(mob);
    });

    this.mobForm.valueChanges.subscribe((val) => {
      if (this.selectedMobile && val) {
        Object.assign(this.selectedMobile, val);
      }
    });
  }

  public onSelectionCommand(mobile: IActorTemplate | null): void {
    if (mobile) {
      this.selectedMobile = mobile;
      this.mobForm.patchValue(mobile, {emitEvent: false});
    } else {
      this.selectedMobile = null;
      this.mobForm.reset();
    }
  }
  public commandAddTrigger(): void {
    if (this.selectedMobile) {
      if (!this.selectedMobile.triggers) {
        this.selectedMobile.triggers = [];
      }
      this.selectedMobile.triggers.push({
        vnum: 0,
        type: TriggerType.Enter,
        script: 0,
        typeArg: ""
      })
    }
  }

  public commandDeleteTrigger(trigger: ITrigger): void {
    const index = this.selectedMobile?.triggers.indexOf(trigger);
    if (index !== undefined && index >= 0) {
      this.selectedMobile?.triggers.splice(index, 1);
    }
  }
}
