import {Component, OnInit} from '@angular/core';
import {AreaService} from "../../services/area.service";
import {filter} from "rxjs";
import {IArea} from "omud/dist-lib/area";
import {IActorTemplate} from "omud/dist-lib/actor";

@Component({
  selector: 'mob-nav',
  templateUrl: 'mob-nav.component.html'
})
export class MobNavComponent implements OnInit {

  public area!: IArea;
  public selected: IActorTemplate | null = null;

  constructor(private areaService: AreaService) {
  }

  public ngOnInit() {
    this.areaService.area$.pipe(
      filter((selected) => selected !== undefined)
    ).subscribe((area) => {
      this.area = area;
      if (!this.selected) {
        this.commandSelect();
      }
    });
    this.areaService.mob$.subscribe((mob) => {
      this.selected = mob;
    });
  }

  public commandSelect(): void {
    this.areaService.selectActor(this.selected);
  }

  public commandNew(randomize: boolean = false): void {
    this.areaService.newActor(randomize);
  }

  public commandDelete(): void {
  }

  public get actors(): IActorTemplate[] {
    if (this.area) {
      return this.area.actors;
    }
    return [];
  }
}
