import * as Phaser from 'phaser-ce';
import {AreaService} from "../../services/area.service";
import {IArea} from "omud/dist-lib/area";
import {IRoom} from "omud/dist-lib/room";
import {Direction, reverseDirection} from "diku-rom/dist/shared/direction";

export class PhaserMap {

  public game: Phaser.Game;
  public area: IArea | null = null;
  public retryList: any[] = [];
  public shouldRender = false;
  public graphics: any;
  public bounds = {
    minX: 0,
    minY: 0,
    maxX: 0,
    maxY: 0
  };

  constructor(private areaService: AreaService) {
    const config = {
      width: 1000,
      height: 750,
      renderer: Phaser.AUTO,
      antialias: true,
      parent: 'map-canvas-container',
      state: {
        preload: () => {
        },
        create: () => {
        },
        update: () => {
        },
        render: () => {
          if (this.area && this.shouldRender) {
            console.log("Rendering...");

            //const nodes = this.processRooms(this.area.rooms);
            this.renderRooms();
          }

        }
      }
    };
    this.game = new Phaser.Game(config);
  }

  public destroy() {
    this.shouldRender = false;
    if (this.graphics) {
      this.graphics.destroy();
    }
    this.game.destroy();
  }

  public refresh(area: IArea): void {
    this.area = area;
    area.rooms.forEach((eachRoom) => {
      eachRoom.mapX = -1;
      eachRoom.mapY = -1;
    });
    this.retryList = [];
    let room = area.rooms[0];
    this.processRoom(room, 0, 0);
    if (this.retryList.length > 0) {
      console.log(this.retryList);
      this.padRooms(area);
      const retryList = this.retryList.slice();
      this.retryList = [];
      retryList.forEach((retry) => {
        this.processRoom(retry.room, retry.x, retry.y);
      })
    }
    console.log(this.retryList);
    console.log(this.bounds);
    this.shouldRender = true;
  }

  public padRooms(area: IArea) {
    area.rooms.forEach((eachRoom, yIndex) => {
      if (eachRoom.mapX !== undefined) {
        eachRoom.mapX *= 2;
        eachRoom.mapY *= 2;
      }
    });
  }

  private directions = [
    {
      dir: Direction.DIR_NORTH,
      x: 0,
      y: -1 // Phaser renders downward from 0!
    }, {
      dir: Direction.DIR_EAST,
      x: 1,
      y: 0
    }, {
      dir: Direction.DIR_SOUTH,
      x: 0,
      y: 1
    }, {
      dir: Direction.DIR_WEST,
      x: -1,
      y: 0
    }
  ];

  public isCollision(room: IRoom, colX: number, rowY: number): boolean {
    let collision = null;
    if (this.area) {
      this.area.rooms.forEach((eachRoom, yIndex) => {
        if (colX != undefined && eachRoom.mapX === colX && eachRoom.mapY === rowY) {
          collision = eachRoom;
        }
      });
    }
    return collision !== null;
  }

  public isAlreadyRetry(room: IRoom, colX: number, rowY: number) {
    return this.retryList.find((item) => {
      return item.room === room;
    })
  }

  public processRoom(room: IRoom, colX: number, rowY: number): void {
    if (room.mapX != -1 || this.isAlreadyRetry(room, colX, rowY)) {
      return;
    }
    if (this.isCollision(room, colX, rowY)) {
      this.retryList.push({
        room: room,
        x: colX,
        y: rowY
      });
      return;
    }
    console.log(`Setting ${room.vnum} as ${colX}/${rowY}`);
    room.mapX = colX;
    room.mapY = rowY;
    this.updateBounds(colX, rowY);

    this.directions.forEach((dir) => {
      let exit = this.areaService.getExit(dir.dir,room);
      if (exit) {
        let tempRoom = this.area!.rooms.find((searchRoom) => searchRoom.vnum == exit!.toVnum);
        if (tempRoom) {
          this.processRoom(tempRoom, colX + dir.x, rowY + dir.y);
        }
      }
    });
  }

  public updateBounds(x: number, y: number): void {
    if (x >= this.bounds.maxX) {
      this.bounds.maxX = x;
    }
    if (x <= this.bounds.minX) {
      this.bounds.minX = x;
    }
    if (y >= this.bounds.maxY) {
      this.bounds.maxY = y;
    }
    if (y <= this.bounds.minY) {
      this.bounds.minY = y;
    }
  }

  public isConnected(eachRoom: IRoom, room: IRoom, direction: Direction): boolean {
    let exit = this.areaService.getExit(direction, room);
    return !!exit
      && exit.toVnum === eachRoom.vnum
      && !!this.areaService.getExit(reverseDirection[direction], eachRoom)
      && this.areaService.getExit(reverseDirection[direction], eachRoom)!.toVnum === room.vnum;
  }

  public renderRooms(): void {
    if (this.graphics) {
      this.graphics.destroy();
    }
    this.graphics = this.game.add.graphics(0, 0);
    this.graphics.lineStyle(2, 0x0000FF, 1);

    this.area!.rooms.forEach((eachRoom, yIndex) => {
      if (eachRoom.mapX !== -1) {
        this.renderRoom(eachRoom, eachRoom.mapX, eachRoom.mapY);
      }
    });
    this.shouldRender = false;
  }

  public renderRoom(room: IRoom, x: number, y: number): void {
    let rX = 145 + (this.bounds.minX * -35) + (x * 35);
    let rY = 145 + (this.bounds.minY * -35) + (y * 35);
    this.graphics.drawRect(rX, rY, 30, 30);
    var style = {font: "bold 12px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle"};
    //  The Text is positioned at 0, 100
    this.game.add.text(rX + 2, rY + 5, `${room.vnum}`, style);
  }
}
