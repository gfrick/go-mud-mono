import {Component, OnDestroy, OnInit} from '@angular/core';
import {PhaserMap} from "./map.phaser";
import {AreaService} from "../../services/area.service";
import {IArea} from "omud/dist-lib/area";

@Component({
  selector: 'app-map',
  templateUrl: 'map.component.html',
  styleUrls: ['map.component.css']
})
export class MapComponent implements OnInit, OnDestroy {

  public phaserMap: PhaserMap;
  private area!:IArea;

  constructor(private areaService: AreaService) {
    console.log("new map...");
    this.phaserMap = new PhaserMap(areaService);
  }

  public ngOnInit() {
    this.areaService.area$.subscribe((area) => {
      if (area && area.rooms.length > 0) {
        this.area = area;
        this.phaserMap.refresh(area);
      }
    });
    this.areaService.room$.subscribe((room) => {
      if (this.area && this.area.rooms.length > 0) {
        this.phaserMap.refresh(this.area);
      }
    });
  }

  public ngOnDestroy() {
    console.log("Destroy");
    this.phaserMap.destroy();
  }
}
