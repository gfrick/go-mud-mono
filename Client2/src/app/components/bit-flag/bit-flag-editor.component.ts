import {Component, Input, OnChanges, OnInit, Output, EventEmitter, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {IBitFlag} from "diku-rom/dist/shared/bit-flag";

@Component({
  selector: 'bit-flag-editor',
  templateUrl: './big-flag-editor.component.html'
})
export class BitFlagEditorComponent implements OnInit, OnChanges {

  @Input()
  public value: number = 0;

  @Output() valueChange = new EventEmitter<number>();

  @Input()
  public flagSet!: Array<IBitFlag>;

  public flagForm = new FormGroup({});

  constructor() {
  }

  public ngOnInit() {
    this.flagForm.valueChanges.subscribe((val) => {
      const newValue = this.getFlagValue(val, this.flagSet);
      this.valueChange.emit(newValue);
    });
  }

  public getFlagValue(val: any, flagSet: Array<IBitFlag>): number {
    let newFlag = 0;
    flagSet.forEach((flag) => {
      if (val[flag.name] === true) {
        newFlag |= flag.bit;
      }
    });
    return newFlag;
  }

  public updateFormValue(val: any) {
    let updateFlag = false;
    this.flagSet.forEach((flag) => {
      updateFlag = (val & flag.bit) === flag.bit;
      let control = this.flagForm.get(flag.name);
      if (control) {
        control.setValue(updateFlag);
      }
    });
  }

  public ngOnChanges(changes: SimpleChanges) {
    const flagSet = changes['flagSet'];
    if (flagSet) {
      this.createFormFields();
    }
    const change = changes['value'];
    this.updateFormValue(change.currentValue);
  }

  public createFormFields(): void {
    this.flagForm = new FormGroup({});
    this.flagSet.forEach((flag) => {
      this.flagForm.addControl(flag.name, new FormControl(''));
    });
  }

}
