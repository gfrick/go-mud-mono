import {Component, OnInit} from '@angular/core';
import {filter} from "rxjs";
import {AreaService} from "../../services/area.service";
import {IArea} from "omud/dist-lib/area";
import {IGameScript} from "omud/dist-lib/events/events";

@Component({
  selector: 'app-script-nav',
  templateUrl: 'script-nav.component.html'
})
export class ScriptNavComponent implements OnInit {

  public area!: IArea;
  public selected: IGameScript | null = null;

  constructor(private areaService: AreaService) {
  }

  public ngOnInit() {
    this.areaService.area$.pipe(
      filter( (selected) => selected !== undefined)
    ).subscribe( (area) => {
      this.area = area;
      if( !this.selected ) {
        this.commandSelect();
      }
    });
    this.areaService.script$.subscribe( (script) => {
      this.selected = script;
    });
  }

  public commandSelect(): void {
    this.areaService.selectScript(this.selected);
  }

  public commandNew(): void {
    this.areaService.newScript();
  }

  public commandDelete(): void {
  }

  public get scripts(): IGameScript[] {
    if( this.area ) {
      return this.area.scripts;
    }
    return [];
  }
}
