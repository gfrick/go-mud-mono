import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AreaService} from "../../services/area.service";
import {IGameScript} from "omud/dist-lib/events/events";

@Component({
  selector: 'app-script-editor',
  templateUrl: 'script-editor.component.html',
  styleUrls: ['script-editor.component.css']
})
export class ScriptEditorComponent implements OnInit {

  public scriptForm = new FormGroup({
    name: new FormControl(),
    script: new FormControl()
  });

  public selectedScript: IGameScript | null = null;

  constructor(public areaService: AreaService) {
  }

  public ngOnInit() {
    this.areaService.script$.subscribe( (script) => {
      this.onSelectionCommand(script);
    });

    this.scriptForm.valueChanges.subscribe((val) => {
      if (this.selectedScript && val) {
        Object.assign(this.selectedScript, val);
      }
    });
  }

  public onSelectionCommand(script: IGameScript | null): void {
    if (script) {
      this.selectedScript = script;
      this.scriptForm.patchValue(script);
    } else {
      this.scriptForm.reset();
      this.selectedScript = null;
    }
  }

}
