import {Component, OnInit} from '@angular/core';
import { FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {AreaService} from "../../services/area.service";
import {IRoom} from "omud/dist-lib/room";
import {IActorSpawn, IItemSpawn} from "omud/dist-lib/reset";
import {ITrigger, TriggerType} from "omud/dist-lib/events/events";
import {IActorTemplate} from "omud/dist-lib/actor";
import {IItemTemplate} from "omud/dist-lib/item";
import {RoomSectors} from "diku-rom/dist/shared/room-sectors";
import {Direction} from "diku-rom/dist/shared/direction";

@Component({
  selector: 'app-room-editor',
  templateUrl: 'room.component.html',
  styleUrls: ['room.component.css']
})
export class RoomComponent implements OnInit {

  public roomForm!: FormGroup;
  public sectorSet = RoomSectors.sectors;
  public selectedRoom: IRoom | null = null;
  public resetActor = 0;
  public resetItem = 0;
  public exitTitles = ['North', 'East', 'South', 'West', 'Up', 'Down'];

  constructor(public areaService: AreaService, private fb: FormBuilder) {
    this.roomForm = new FormGroup({
      name: new FormControl(),
      description: new FormControl(),
      sector: new FormControl()
    });
  }

  public ngOnInit() {
    this.areaService.room$.subscribe((room) => {
      this.onSelectionCommand(room);
    });

    this.roomForm.valueChanges.subscribe((val) => {
      if (this.selectedRoom && val) {
        Object.assign(this.selectedRoom,val);
      }
    });
  }

  public commandDeleteActorSpawn(spawn: IActorSpawn): void {
    this.areaService.deleteActorSpawn(spawn);
  }

  public commandDeleteItemSpawn(spawn: IItemSpawn): void {
    this.areaService.deleteItemSpawn(spawn);
  }

  public onSelectionCommand(room: IRoom | null): void {
    if (room) {
      this.selectedRoom = room;
      // 1. Patch basic values.
      this.roomForm.patchValue(room, {
        onlySelf: true,
        emitEvent: false
      });
    } else {
      this.selectedRoom = null;
      this.roomForm.reset();
    }
    this.resetActor = 0;
    this.resetItem = 0;
  }

  public commandAddTrigger(): void {
    if (this.selectedRoom) {
      if (!this.selectedRoom.triggers) {
        this.selectedRoom.triggers = [];
      }
      this.selectedRoom.triggers.push({
        vnum: 0,
        type: TriggerType.Enter,
        script: 0,
        typeArg: ""
      })
    }
  }

  public commandDeleteTrigger(trigger: ITrigger): void {
    const index = this.selectedRoom?.triggers.indexOf(trigger);
    if (index !== undefined && index >= 0) {
      this.selectedRoom?.triggers.splice(index, 1);
    }
  }

  public commandDeleteExit(direction: Direction): void {
    this.areaService.removeExit(direction);
  }

  public commandGoExit(direction: Direction): void {
    this.areaService.moveRoom(direction);
  }

  public get actors(): IActorTemplate[] {
    return this.areaService.getCurrentArea().actors;
  }

  public get actorResets(): IActorSpawn[] {
    if (this.selectedRoom) {
      return this.areaService.getActorResetsFor(this.selectedRoom.vnum);
    }
    return [];
  }

  public commandSelectActor(event: any): boolean {
    this.areaService.newActorSpawn(this.selectedRoom!.vnum, this.resetActor);
    setTimeout(() => {
      this.resetActor = 0;
    }, 10);
    event.preventDefault();
    return true;
  }

  public get items(): IItemTemplate[] {
    return this.areaService.getCurrentArea().items;
  }

  public get itemResets(): IItemSpawn[] {
    if (this.selectedRoom) {
      return this.areaService.getItemResetsFor(this.selectedRoom.vnum);
    }
    return [];
  }

  public commandSelectItem(event: any): boolean {
    this.areaService.newItemSpawn(this.selectedRoom!.vnum, this.resetItem);
    setTimeout(() => {
      this.resetItem = 0;
    }, 10);
    event.preventDefault();
    return true;
  }

}
