import {Component, OnInit} from '@angular/core';
import {filter} from "rxjs";
import {AreaService} from "../../services/area.service";
import {IRoom} from "omud/dist-lib/room";
import {IArea} from "omud/dist-lib/area";

@Component({
  selector: 'room-nav',
  templateUrl: 'room-nav.component.html'
})
export class RoomNavComponent implements OnInit {

  public area!: IArea;
  public selected: IRoom | null = null;

  constructor(private areaService: AreaService) {
  }

  public ngOnInit() {
    this.areaService.area$.pipe(
      filter( (selected) => selected !== undefined)
    ).subscribe( (area) => {
      this.area = area;
      if( !this.selected ) {
        this.commandSelect();
      }
    });
    this.areaService.room$.subscribe( (room) => {
      this.selected = room;
    });
  }

  public commandSelect(): void {
    this.areaService.selectRoom(this.selected);
  }

  public commandNew(): void {
    this.areaService.newRoom();
  }

  public commandDelete(): void {
  }

  public get rooms(): IRoom[] {
    if( this.area ) {
      return this.area.rooms;
    }
    return [];
  }
}
