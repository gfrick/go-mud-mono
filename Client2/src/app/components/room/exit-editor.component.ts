import {Component, Input, OnChanges} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {AreaService} from "../../services/area.service";
import {IRoom} from "omud/dist-lib/room";
import {IItemTemplate} from "omud/dist-lib/item";
import {ExitFlags} from "diku-rom/dist/shared/exit-flags";
import {Direction} from "diku-rom/dist/shared/direction";

@Component({
  selector: 'exit-editor',
  templateUrl: 'exit-editor.component.html'
})
export class ExitEditorComponent implements OnChanges {

  @Input()
  public direction!: number;

  public showDetails: boolean = false;

  @Input()
  public room!: IRoom;

  public exitFlags = ExitFlags.exitFlags;
  public keys: Array<IItemTemplate> = [];

  public exitForm = new FormGroup({
    keyword: new FormControl(''),
    description: new FormControl(''),
    toVnum: new FormControl('none'),
    keyVnum: new FormControl(''),
    defaultExitFlags: new FormControl('')
  });

  constructor(private areaService: AreaService) {
  }

  public ngOnInit() {
    this.keys = this.areaService.getCurrentArea().items;

    this.exitForm.valueChanges.subscribe((val) => {
      let exit = this.areaService.getExit(this.direction, this.room);
      if (exit) {
        exit.keyVnum = val.keyVnum;
        exit.toVnum = val.toVnum;
        exit.keyword = val.keyword;
        exit.description = val.description;
        exit.defaultExitFlags = val.defaultExitFlags;
      }
    });

    this.updateFormValue();
  }

  public commandToggleDetails() {
    this.showDetails = !this.showDetails;
  }

  public ngOnChanges() {
    this.updateFormValue();
  }

  public ngOnDestroy() {
  }

  get exit() {
    return this.areaService.getExit(this.direction, this.room);
  }

  public updateFormValue() {
    const exit = this.exit;
    if (!exit) {
      this.exitForm.patchValue({
        toVnum: 'none',
        keyword: null,
        description: null,
        defaultExitFlags: 0,
        keyVnum: 0
      });
      return;
    }
    this.exitForm.patchValue(exit, {emitEvent: false});
  }

  public roomList(direction: Direction): IRoom[] {
    return this.areaService.roomsAvailableInDirection(direction);
  }

  updateExitFlags(newValue: number){
    let exit = this.exit;
    if( exit ) {
      exit.defaultExitFlags = newValue;
      this.areaService.syncExit(exit);
    }
  }

  public updateExit(): void {
    const direction = this.direction;
    const decision = this.exitForm.get("toVnum")?.value;
    if (decision === "none") return;
    if (decision === "remove") {
      this.areaService.removeExit(direction);
      return;
    }
    if (decision === "dig") {
      const result = this.areaService.digRoom(direction);
      this.exitForm.patchValue({
        toVnum: result
      });
      return;
    }
    const iDecision = parseInt(decision, 10);
    if (iDecision > 0) {
      this.areaService.connectRoom(direction, iDecision);
      return;
    }
  }

}
