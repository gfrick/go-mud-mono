export interface Player {
  ownerId: string;
  name: string;
  raceId: number;
  jobId: number;
  level: number;
}
