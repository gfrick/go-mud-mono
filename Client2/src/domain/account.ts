
export interface NewAccount {
  username: string,
  password: string,
  cpassword: string
}

export interface Account {
  id: string;
  username: string;
  password: string;
}
