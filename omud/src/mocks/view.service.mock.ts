import {IViewService} from "../services/view.service";
import {Actor} from "../domain/actor";
import {Position} from "../domain/position";
import {Room} from "../domain/room";
import {IExit} from "../domain/exit";

const viewService = jest.fn<IViewService, []>(() => {
    return {
        act_new(format: string, ch: Actor, arg1: any, arg2: any, type: number, min_pos: Position) {
        },
        echoToRoom(room: Room, output: string, inputEntity: Actor) {
        },
        getNameFor(ch: Actor, victim: Actor): string {
            return "";
        },
        showEntityToEntity(inputEntity: Actor, entity: Actor) {
        },
        showExitToEntity(inputEntity: Actor, door: IExit) {
        },
        showRoomToEntity(inputEntity: Actor, currentRoom: Room) {
        }
    };
});

export { viewService };
