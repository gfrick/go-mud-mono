import {Actor} from "../domain/actor";
import {ICommandService} from "../services/command.service";
import {IMudCommand} from "../core/mud.icommand";

const commandService = jest.fn<ICommandService, []>(() => {
    return {
        registerCommands() {

        },
        runCommand(inputEntity: Actor, input: string | undefined | null) {
        },
        parseCommand(input: string | undefined | null) : string {
            return null;
        },
        findCommand(input: string | undefined | null) : IMudCommand | null {
            return null;
        },
        parseArgs(input: string | undefined | null) : string {
            return ""
        }
    };
});

export { commandService };
