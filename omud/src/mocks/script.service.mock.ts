import {IScriptService} from "../services/script.service";
import {ICommandService} from "../services/command.service";
import {IGameEvent} from "../domain/events/events";
import {Actor} from "../domain/actor";

const scriptService = jest.fn<IScriptService, []>(() => {
    return {
        runScriptForEvent(inputEntity: Actor, event: IGameEvent, commandService: ICommandService): boolean {
            return false;
        }
    }
});

export {scriptService};
