import {Item} from "../domain/item";

const defaultItem = (vnum: number, override: any = {}) => {
    const item = new Item({
        condition: 100,
        cost: 1,
        description: "can tab",
        level: 1,
        longDesc: "A can tab lies here being defaulty.",
        material: "aluminum",
        name: "can tab",
        properties: {},
        shortDesc: "a can tab",
        tags: [],
        triggers: [],
        type: 0,
        vnum: vnum,
        wearFlags: 0,
        weight: 1
    });
    return Object.assign(item, override);
};

export { defaultItem }
