import {IWorldService} from "../core/world";
import {IItemTemplate} from "../domain/item";
import {IItemContainer} from "../domain/container";
import {Room} from "../domain/room";
import {Actor, IActorTemplate} from "../domain/actor";
import {IGameScript} from "../domain/events/events";

const worldService = jest.fn<IWorldService, []>(() => {
    return {
        objectTemplateToContainer(objTemplate: IItemTemplate, entity: IItemContainer) {
        },
        findRoom(vnum: string | number): Room | null {
            return null;
        },
        findObjectTemplate(vnum: number): IItemTemplate | null {
            return null;
        },
        countEntities(vnum: number): number {
            return 0;
        },
        findEntityTemplate(vnum: number): IActorTemplate | null {
            return null;
        },
        entityTemplateToWorld(mobTemplate: IActorTemplate, room: Room) {
        },
        getTriggerScript(script: number): IGameScript | null {
            return {
                name: "testScript",
                vnum: 999,
                script: "(function cool(trig, cmdSrvc) { trig.event.entity.stringToEntity(`test output`); })"
            };
        },
        moveEntityToVnum(entity: Actor, vnum: string | number): boolean {
            return false;
        },
        showRoomToEntity(inputEntity: Actor, currentRoom?: Room) {
        },
        showEquipmentToEntity(inputEntity: Actor, lookEntity: Actor) {
        },
        showScoreToEntity(inputEntity: Actor) {}
    };
});

export {worldService};
