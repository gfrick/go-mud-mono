import {Room} from "../domain/room";

const defaultRoom = (vnum: number, override: any = {}) : Room => {
    let room = new Room({
        vnum: vnum,
        sector: 1,
        description: "You appear to be lost.",
        name: "Lost",
        exits: [],
        flags: 0,
        triggers: [],
        tags: [],
        properties: {},
        mapX: -1,
        mapY: -1
    });
    return Object.assign(room, override);
};

export { defaultRoom }
