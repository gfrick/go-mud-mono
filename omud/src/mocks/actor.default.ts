import {Sex} from "../domain/sex";
import {Size} from "../domain/size";
import {Position} from "../domain/position";
import {Actor} from "../domain/actor";

const defaultActor = (vnum: number, override: any = {}) : Actor => {
    let actor = new Actor({
        alignment: 0,
        damageType: "",
        defaultPosition: 0,
        description: "",
        hitRoll: 0,
        level: 1,
        longDesc: "A black cat just walked by again.",
        name: "black cat",
        properties: {},
        race: "cat",
        sex: Sex.NEUTRAL,
        shortDesc: "a black cat",
        size: Size.Medium,
        startPosition: Position.POS_STANDING,
        tags: [],
        triggers: [],
        vnum: vnum,
        wealth: 1
    });
    return Object.assign(actor,override);
};

export { defaultActor }
