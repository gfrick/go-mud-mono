import {Area, IArea} from "../domain/area";
import Logger from "../core/logger";

export class AreaService {

    public static createAreasFromTemplates(areaTemplates: IArea[]): Area[] {
        const areas = [];
        Logger.info("Creating Areas from templates.");
        areaTemplates.forEach((areaTemplate: IArea) => {
            Logger.info(`${areaTemplate.header.areaName} with id ${areaTemplate._id} loaded.`);
            areas.push(new Area(areaTemplate));
        })
        return areas;
    }
}
