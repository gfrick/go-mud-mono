import { Room} from "../domain/room";
import {ExitFlags, ExitService, IExit} from "../domain/exit";
import {Actor} from "../domain/actor";
import {BitFlags} from "../domain/flags";
import {defaultRoom} from "../mocks/room.default";

class TemplateService {

    static readonly defaultRoom = defaultRoom(999999);
    public static createScoreView(entity: Actor): any {
        return {
            "template": "score",
            "score": {
                name: entity.getShortName(),
                level: entity.getLevel(),
                race: entity.getRace(),
                sex: entity.getSex()
                // Class
            }
        }
    }

    public static createEquipmentView(entity: Actor, viewing: Actor): any {
        const wearing = entity.equipment;
        return {
            "template": "equipment",
            "equipment": wearing.getItemView()
        };
    }

    public static createRoomView(entity: Actor, room: Room = this.defaultRoom): any {
        return {
            "template": "room",
            "room": {
                vnum: room.vnum,
                description: room.description,
                name: room.name,
                exits: this.buildExitList(room.exits, entity),
                entities: room.entities.filter(roomEntity => roomEntity != entity).map((entity) => {
                    return `${entity.getShortName()} is ${entity.getPositionName()} here.`;
                }),
                items: room.items.map((roomItem) => {
                    return roomItem.getLongDesc();
                })
            }
        };
    }

    public static buildExitList(exits: IExit[], entity: Actor): string[] {
        let values = [];
        exits.forEach((exit) => {
            if (!BitFlags.hasFlag(exit.exitFlags, ExitFlags.EX_HIDDEN)) {
                let name = ExitService.dirName(exit.direction);
                if (BitFlags.hasFlag(exit.exitFlags, ExitFlags.EX_CLOSED)) {
                    name += ' (closed)';
                }
                values.push(name);
            }
        })
        return values;
    }
}

export {TemplateService};
