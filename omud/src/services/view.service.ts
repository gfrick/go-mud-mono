import {Actor} from "../domain/actor";
import {Item} from "../domain/item";
import {IExit} from "../domain/exit";
import {Room} from "../domain/room";
import {Position} from "../domain/position";
import {ActFlags} from "../domain/act-flags";
import {TemplateService} from "./template.service";
import Logger from "../core/logger";

interface IViewService {
    showRoomToEntity(inputEntity: Actor, currentRoom: Room): void;

    showEntityToEntity(inputEntity: Actor, entity: Actor): void;

    showExitToEntity(inputEntity: Actor, door: IExit): void;

    getNameFor(ch: Actor, victim: Actor): string;

    echoToRoom(room: Room, output: string, inputEntity: Actor): void;

    act_new(format: string, ch: Actor, arg1: any, arg2: any, type: number, min_pos: Position): void;
}

class ViewService implements IViewService {

    showRoomToEntity(inputEntity: Actor, currentRoom: Room = inputEntity.currentRoom) {
        if (inputEntity.position <= Position.POS_SLEEPING) {
            inputEntity.stringToEntity(`You are ${inputEntity.getPositionName()!}`);
            return;
        }
        inputEntity.templateToEntity(TemplateService.createRoomView(inputEntity, currentRoom));
    }

    showEntityToEntity(inputEntity: Actor, entity: Actor) {
        inputEntity.stringToEntity(entity.description);
    }

    showExitToEntity(inputEntity: Actor, door: IExit) {
        inputEntity.stringToEntity(door.description);
    }

    getNameFor(ch: Actor, victim: Actor) {
        if (ch == victim) {
            return ch.getShortName();
        }
        // do your can_see, etc here.
        return ch.getShortName();
    }

    echoToRoom(room: Room, output: string, inputEntity: Actor = null) {
        room.entities.forEach((otherEntity) => {
            if (otherEntity != inputEntity) {
                otherEntity.stringToEntity(output);
            }
        })
    }

    act_new(format: string, ch: Actor, arg1: any, arg2: any, type: number, min_pos: Position) {
        const he_she = ["it", "he", "she"];
        const him_her = ["it", "him", "her"];
        const his_her = ["its", "his", "her"];

        let to: Actor[];
        let vch = arg2 as Actor;
        let obj1 = arg1 as Item;
        let obj2 = arg2 as Item;

        if (!format || format.length === 0 || !ch) {
            return;
        }

        to = ch.currentRoom.entities;

        if (type == ActFlags.TO_VICT) {
            if (!vch) {
                Logger.error("Act: null vch with TO_VICT.");
                return;
            }
            to = vch.currentRoom.entities;
        }
        to.forEach((eachEntity) => {
            if (!eachEntity.getShortName() || eachEntity.getPosition() < min_pos) return;

            if ((type == ActFlags.TO_CHAR) && eachEntity != ch)
                return;
            if (type == ActFlags.TO_VICT && (eachEntity != vch || eachEntity == ch))
                return;
            if (type == ActFlags.TO_ROOM && eachEntity == ch)
                return;
            if (type == ActFlags.TO_NOTVICT && (eachEntity == ch || eachEntity == vch))
                return;

            let replaceIndex = format.indexOf("$");
            while (replaceIndex != -1) {
                const selector = format.charAt(replaceIndex + 1);
                let i: string = "";
                switch (selector) {
                    default :
                        i = '@BUG@';
                        break;
                    case 't':
                        i = arg1 as string;
                        break;
                    case 'T':
                        i = arg2 as string;
                        break;
                    case 'n':
                        i = this.getNameFor(ch, eachEntity);
                        break;
                    case 'N':
                        i = this.getNameFor(vch, eachEntity);
                        break;
                    case 'e':
                        i = he_she[0];
                        break; // should be ch.sex
                    case 'E':
                        i = he_she[0];
                        break; // should be vch.sex
                    case 'm':
                        i = him_her[0];
                        break; // should be ch.sex
                    case 'M':
                        i = him_her[0];
                        break; // should be vch.sex
                    case 's':
                        i = his_her[0];
                        break; // should be ch.sex
                    case 'S':
                        i = his_her[0];
                        break; // should be vch.sex
                    case 'p':
                        i = obj1.getShortDesc();
                        break;
                    case 'P':
                        i = obj2.getShortDesc();
                        break;
                    case 'd':
                        i = !arg2 ? "door" : arg2 as string;
                        break;
                }
                format = format.replace("$" + selector, i);
                replaceIndex = format.indexOf("$");
            }
            eachEntity.stringToEntity(format);
        })

        return;
    }
}

export {IViewService, ViewService};
