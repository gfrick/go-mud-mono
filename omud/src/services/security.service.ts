import * as jwt from "jsonwebtoken";
import {Request} from "express";
import {IAccount} from "../models/account";

interface IUserRequest extends Request {
    user?: IAccount;
}

class SecurityService {
    public getAccountForWebSocket(request): IAccount  {
        const cookies = {};
        if (request.headers.cookie) request.headers.cookie.split(';').forEach(function (cookie) {
            const parts = cookie.match(/(.*?)=(.*)$/);
            const name = parts[1].trim();
            const value = (parts[2] || '').trim();
            cookies[name] = value;
        });
        if( cookies && cookies["token"]) {
            const result = jwt.verify(cookies["token"], "secret_key");
            return result as IAccount;
        }
        return {
            id: "",
            permissions: [],
            password: "",
            username: ""
        };
    }

    public checkTokenMiddleware(req: IUserRequest, res, next) {
        // get authcookie from request
        const authcookie = req.cookies.token
        // verify token which is in cookie value
        jwt.verify(authcookie, "secret_key", (err, data) => {
            if (err) {
                res.sendStatus(403)
                return;
            } else if (data) {
                req.user = data
            }
            next();
        });
    }

    public addSecurityCookies(foundUser: IAccount, res) {
        const userData: IAccount = {
            id: foundUser.id,
            username: foundUser.username,
            password: '',
            permissions: foundUser.permissions
        };
        const token = jwt.sign(userData, 'secret_key');
        const accountCookie = Buffer.from(JSON.stringify(userData)).toString('base64')
        res.cookie('token', token, {maxAge: 12000000, httpOnly: true});
        res.cookie('account', accountCookie, {maxAge: 12000000, httpOnly: false});
    }
}

export {IUserRequest, SecurityService};
