import * as vm from 'vm';
import {IWorldService} from "../core/world";
import {Actor} from "../domain/actor";
import {Room} from "../domain/room";
import {IGameEvent, ITriggerContainer, ITriggeredEvent} from "../domain/events/events";
import {ICommandService} from "./command.service";

// event: the event that triggers this.
// trigger: entity that triggered this if relevant.
// self: entity triggered on (room, object, npc, or null)
// args: args passed as part of attaching script to entity, array of any/string
// (event, trigger, self, args )
export interface IScriptContext {
    event: IGameEvent,
    trigger: Actor,
    self: Actor | Object | Room,
    args: string[]
}

const IScriptFunc = (scriptContext: ITriggeredEvent, commandService: ICommandService): void => {
};

export interface IScriptService {
    runScriptForEvent(inputEntity: Actor, event: IGameEvent, commandService: ICommandService): boolean;
}

export class ScriptService implements IScriptService {

    private readonly context;
    private readonly worldService: IWorldService;

    constructor(worldService: IWorldService) {
        this.worldService = worldService;
        this.context = vm.createContext({
            console: console, // dangerous
            process: process, // dangerous
            worldService: worldService
        });
    }

    runScriptForEvent(inputEntity: Actor, event: IGameEvent, commandService: ICommandService): boolean {
        // Get triggers for the room, actors, and items in the room.
        const triggered = (event.entity.currentRoom as ITriggerContainer).getTriggeredEvents(event);


        // TODO trigger % chance to run, only Self, etc.
        if (triggered && triggered.length > 0) {
            triggered.forEach((trigger) => {
                const script = this.worldService.getTriggerScript(trigger.trigger.script);
                if (script) {
                    const fn: typeof IScriptFunc = vm.runInContext(script.script, this.context);
                    fn(trigger, commandService);
                }
            });
        }
        return false;
    }
}
