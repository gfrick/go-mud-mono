import {IMudCommand} from "../core/mud.icommand";
import {IViewService} from "./view.service";
import {IWorldService} from "../core/world";
import {LookCommand} from "../commands/look.command";
import {GotoCommand} from "../commands/goto.command";
import {SayCommand} from "../commands/say.command";
import {MoveCommand} from "../commands/move.command";
import {CloseCommand} from "../commands/close.command";
import {OpenCommand} from "../commands/open.command";
import {InventoryCommand} from "../commands/inventory.command";
import {LoadCommand} from "../commands/load.command";
import {MobLoadCommand} from "../commands/mload.command";
import {DropCommand} from "../commands/drop.command";
import {GetCommand} from "../commands/get.command";
import {SitCommand} from "../commands/sit.command";
import {StandCommand} from "../commands/stand.command";
import {SleepCommand} from "../commands/sleep.command";
import {IGameEvent, TriggerType} from "../domain/events/events";
import {Actor} from "../domain/actor";
import {IScriptService} from "./script.service";
import {WearCommand} from "../commands/wear.command";
import {EquipmentCommand} from "../commands/equipment.command";
import {RemoveCommand} from "../commands/remove.command";
import {NoneCommand} from "../commands/none.command";
import {ScoreCommand} from "../commands/score.command";

export interface ICommandService {
    registerCommands(): void;

    runCommand(inputEntity: Actor, input: string | undefined | null): void;

    parseCommand(input: string | undefined | null): string;

    findCommand(input: string | undefined | null): IMudCommand;

    parseArgs(input: string | undefined | null): string;
}

export class CommandService implements ICommandService {

    public commands: Map<string, IMudCommand>;
    public viewService: IViewService;
    private readonly scriptService: IScriptService
    public world: IWorldService;
    public readonly nullCommand: IMudCommand = new NoneCommand();

    public constructor(viewService: IViewService, world: IWorldService, scriptService: IScriptService) {
        this.viewService = viewService;
        this.world = world;
        this.scriptService = scriptService;
        this.commands = new Map<string, IMudCommand>();
    }

    registerCommands(): void {
        this.commands.set("look", new LookCommand(this.world, this.viewService));
        this.commands.set("goto", new GotoCommand(this.world));
        this.commands.set("say", new SayCommand(this.viewService));
        this.commands.set("north", new MoveCommand(this.world, this.viewService, 0));
        this.commands.set("east", new MoveCommand(this.world, this.viewService, 1));
        this.commands.set("south", new MoveCommand(this.world, this.viewService, 2));
        this.commands.set("west", new MoveCommand(this.world, this.viewService, 3));
        this.commands.set("up", new MoveCommand(this.world, this.viewService, 4));
        this.commands.set("down", new MoveCommand(this.world, this.viewService, 5));
        this.commands.set("close", new CloseCommand(this.world, this.viewService));
        this.commands.set("open", new OpenCommand(this.world, this.viewService));
        this.commands.set("inventory", new InventoryCommand());
        this.commands.set("oload", new LoadCommand(this.world));
        this.commands.set("mload", new MobLoadCommand(this.world));
        this.commands.set("drop", new DropCommand(this.viewService));
        this.commands.set("get", new GetCommand(this.viewService));
        this.commands.set("sit", new SitCommand(this.viewService));
        this.commands.set("stand", new StandCommand(this.viewService));
        this.commands.set("sleep", new SleepCommand(this.viewService));
        this.commands.set("wear", new WearCommand(this.viewService));
        this.commands.set("equipment", new EquipmentCommand(this.world));
        this.commands.set("remove", new RemoveCommand(this.viewService));
        this.commands.set("score", new ScoreCommand(this.world));
        // * fix up character creation, etc.
        // * get, put (container?) - Haven't done object types yet.
        // * lock/unlock doors - would require key.
        // * reset objects - put, equip? - equipment not done yet!
        // * slay (add before kill)
        // * kill / combat - Haven't done modules yet?
        // * command security
        // real score
        // who
        // chat
        // description
        // give
        // tell
        // disconnect/reconnect - right now the player just quits/disappears.
        // change password?
    }

    public shortcutMap = new Map<string, string>([
        ["s", "south"],
        ["sc", "score"],
        ["u", "up"],
        ["d", "down"],
        ["n", "north"],
        ["e", "east"],
        ["w", "west"],
        ["l", "look"],
        ["o", "open"],
        ["c", "close"],
        ["i", "inventory"],
        [".", "say"],
        ["eq", "equipment"],
        ["w", "wear"]
    ]);

    runCommand(inputEntity: Actor, input: string | undefined | null): void {
        if (input) {
            const commandStr = this.parseCommand(input);
            const commandArgs = this.parseArgs(input);
            const command = this.findCommand(commandStr);
            if (command !== this.nullCommand) {
                const event = command.invoke(inputEntity, commandArgs);
                this.scriptService.runScriptForEvent(inputEntity, event, this);
                return;
            }

            // generic action, and check for it.
            let event: IGameEvent = {
                entity: inputEntity,
                type: TriggerType.Keyword,
                target: null,
                args: [commandStr, commandArgs]
            }

            if (this.scriptService.runScriptForEvent(inputEntity, event, this)) {
                return;
            }
            // Run the null command which delegates help lookup, etc.
            command.invoke(inputEntity,commandStr);
        }
    }

    parseCommand(input: string | undefined | null): string {
        if (input && input.length > 0) {
            const cmd = input.trim().split(" ")[0].toLowerCase();
            return this.shortcutMap.get(cmd) || cmd;
        }
        return "";
    }

    findCommand(command: string): IMudCommand {
        if (command && command.length > 0 && this.commands.has(command)) {
            return this.commands.get(command);
        }
        return this.nullCommand;
    }

    parseArgs(input: string | undefined | null): string {
        if (input && input.length > 0) {
            const spaceIdx = input.indexOf(" ");
            if (spaceIdx !== -1) {
                return input.substr(spaceIdx + 1);
            }
        }
        return "";
    }
}
