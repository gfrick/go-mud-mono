import {IPlayer, playerModel} from "../repository/player.model";
import {Actor} from "../domain/actor";
import Logger from "../core/logger";

class SaveService {

    public static saveProgress = async (entities: Actor[]) => {
        let entity: Actor;
        Logger.info(`Saving ${entities.length} players.`);
        for( let aIndex = 0; aIndex < entities.length; aIndex++) {
            entity = entities[aIndex];
            const playerData = await playerModel.findById(entity.controller.getPlayerId());
            if (!playerData) {
                Logger.error("FATAL: Attempt to save player failed for " + entity.controller.getPlayerId());
                return false;
            }
            const update = {
                $set: {
                    state: entity.getActorDto()
                }
            };
            await playerModel.updateOne({
                "_id": playerData.id
            }, update);
            Logger.info(`Saved player ${playerData.name}`);
        }
    }

    public static async loadPlayerTemplate(id: string): Promise<IPlayer | null> {
        const player = await playerModel.findById(id);
        console.log(JSON.stringify(player.state));
        return player;
    }
}

export {SaveService};
