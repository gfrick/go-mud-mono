import {CallbackError, connect} from "mongoose";
import Logger from "../core/logger";

const getMongoUri = (process: NodeJS.Process) => {
    Logger.info(process.env);
    let MONGO_HOST = process.env.DATABASE_HOST_NODE || "mongo";
    let MONGO_PORT = process.env.DATABSE_PORT || 27017;
    let MONGO_DB = process.env.DATABASE || "omud";
    let MONGO_USER =  process.env.DATABASE_USERNAME || "";
    let MONGO_PASS =  process.env.DATABASE_PASSWORD || "";
    let MONGO_CREDS = MONGO_USER ? `${MONGO_USER}:${MONGO_PASS}@` : "";
    if( !MONGO_CREDS) {
        Logger.error("You are using Mongo without credentials!!!");
    }
    let MONGO_URI = `mongodb://${MONGO_CREDS}${MONGO_HOST}:${MONGO_PORT}/${MONGO_DB}?retryWrites=true&w=majority`;
    return MONGO_URI;
}

const initMongo = (uri: string, success: Function, fail: Function ) => {
    try {
        connect(uri, (callBackError: CallbackError) => {
            if( !callBackError) {
                Logger.info('Connected to Mongo DB Successfully!!');
                success();
            } else {
                fail(callBackError.message);
            }
        });
    } catch(err) {
        fail(err);
    }
}

export { initMongo, getMongoUri };
