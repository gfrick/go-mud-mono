import * as mongoose from "mongoose";
import {IAccount} from "../models/account";

const accountSchema = new mongoose.Schema<IAccount>({
    username: { type: String, unique: true,required: true },
    password: { type: String, required: true }
});

const accountModel = mongoose.model<IAccount>('Account', accountSchema, "account");

export default accountModel ;
