import * as mongoose from "mongoose";
import {IAccount} from "../models/account";
import {ActorDto} from "../domain/actor";

interface INewPlayer {
    name: string;
}

interface IPlayer {
    id: string;
    name: string;
    owner: string;
    state: ActorDto;
}

const playerSchema = new mongoose.Schema<IPlayer>({
    name: {type: String, unique: true, required: true},
    owner: {type: String, required: true},
    state: {type: Object, required: true}
});

const playerModel = mongoose.model<IPlayer>('Player', playerSchema, "player");

const getPlayerForAccount = async (username: string, account: IAccount): Promise<IPlayer | null> => {
    const playerEntity = await playerModel.findOne({
        owner: account.id,
        name: username.toLowerCase()
    });
    return playerEntity;
}

export {INewPlayer, IPlayer, playerModel, getPlayerForAccount};

