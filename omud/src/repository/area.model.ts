import * as mongoose from "mongoose";
import {IArea} from "../domain/area";
import Logger from "../core/logger";

const areaSchema = new mongoose.Schema<IArea>({
    header: {type: Object, required: true},
    rooms: {type: Object, required: true},
    items: {type: Object, required: true},
    actors: {type: Object, required: true},
    resets: {type: Object, required: true},
    scripts: {type: Object, required: false}
});

const areaModel = mongoose.model<IArea>('Area', areaSchema, "area");

export class AreaRepository {
    public static async loadAllAreaTemplates(): Promise<IArea[]> {
        const areaTemplates = [];
        await areaModel.find({},
            (err, docs: any[]) => {
                if (err) {
                    Logger.error("Error finding areas " + err.message);
                    return;
                }
                if (docs) {
                    Logger.info("Loaded " + docs.length + " area templates.");
                    docs.forEach((areaTemplate: any) => {
                        const id = areaTemplate.id;
                        console.log(id);
                        const area = areaTemplate.toObject() as IArea;
                        area._id = id;
                        if (area.header) {
                            areaTemplates.push(area);
                        } else {
                            Logger.error(`Invalid area, not making available.`);
                        }
                    }, this);
                    areaTemplates.forEach((areaTemplate: IArea) => {
                        Logger.info(`${areaTemplate.header.areaName} with id ${areaTemplate._id} available.`);
                    })
                }
            });
        return areaTemplates;
    }
}

export default areaModel;
