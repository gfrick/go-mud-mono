import { Actor } from "../domain/actor";
import {IGameEvent} from "../domain/events/events";

export interface IMudCommand {
    invoke(inputEntity: Actor, args?: string) : IGameEvent;
}
