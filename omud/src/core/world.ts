import {Area} from "../domain/area";
import {Actor, IActorTemplate} from "../domain/actor";
import {IItemTemplate, Item} from "../domain/item";
import {Room} from "../domain/room";
import {Position} from "../domain/position";
import {TemplateService} from "../services/template.service";
import {AreaService} from "../services/area.service";
import {IGameScript} from "../domain/events/events";
import {IEntityContainer, IItemContainer} from "../domain/container";
import {AreaRepository} from "../repository/area.model";
import Logger from "./logger";

export interface IWorldService {
    findObjectTemplate(vnum: number): IItemTemplate | null;

    findEntityTemplate(vnum: number): IActorTemplate | null;

    findRoom(vnum: string | number): Room | null;

    entityTemplateToWorld(mobTemplate: IActorTemplate, room: Room);

    objectTemplateToContainer(objTemplate: IItemTemplate, entity: IItemContainer);

    showRoomToEntity(inputEntity: Actor, currentRoom?: Room);
    showEquipmentToEntity(inputEntity: Actor, lookEntity: Actor);
    showScoreToEntity(inputEntity: Actor);

    moveEntityToVnum(entity: Actor, vnum: string | number): boolean;

    countEntities(vnum: number): number;

    getTriggerScript(script: number): IGameScript | null;
}

export default class World implements IWorldService {
    public areas: Area[];
    public entities: Actor[];

    public constructor() {
        this.areas = [];
        this.entities = [];
    }

    public async init(): Promise<boolean> {
        const templates = await AreaRepository.loadAllAreaTemplates();
        if( templates && templates.length > 0) {
            this.areas = AreaService.createAreasFromTemplates(templates);
            return true;
        }
        return false;
    }

    public tick() {
        Logger.info("Ticked World.");
        this.areas.forEach((area) => {
            this.tickArea(area);
        });
    }

    public removeEntity(entity: Actor): void {
        if (entity.currentRoom) {
            entity.currentRoom.removeEntity(entity);
        }
        const index = this.entities.findIndex(x => x === entity);
        if (index !== -1) {
            this.entities.splice(index, 1);
            // TODO "ACT("$n disappears")"
        }
    }

    public loadEntity( state: any, toRoom: number): Actor {
        const entity = new Actor(null, state);
        this.moveEntityToVnum(entity, toRoom);
        this.entities.push(entity);
        return entity;
    }

    public isReady(): boolean {
        return !!(this.areas && this.areas.length > 0);
    }

    public findRoom(vnum: string | number): Room | null {
        let room = null;
        this.areas.every((area) => {
            room = area.findRoom(vnum);
            return !room;
        })
        return room;
    }

    public getTriggerScript(vnum: number): IGameScript | null {
        let script = null;
        this.areas.every((area) => {
            script = area.findScript(vnum);
            return !script;
        })
        return script;
    }

    public findEntityTemplate(vnum: number): IActorTemplate | null {
        let template = null;
        this.areas.every((area) => {
            template = area.findEntityTemplate(vnum);
            return !template;
        })
        return template;
    }

    public findObjectTemplate(vnum: number): IItemTemplate | null {
        let template = null;
        this.areas.every((area) => {
            template = area.findObjectTemplate(vnum);
            return !template;
        })
        return template;
    }

    private tickArea(area: Area) {
        const resets = area.resets;

        resets.actors.forEach((reset) => {
            const mobTemplate = this.findEntityTemplate(reset.actorVnum);
            if (!mobTemplate) {
                Logger.error("Missing mob template " + reset.actorVnum);
                return;
            }
            const room = this.findRoom(reset.roomVnum);
            if (!room) {
                Logger.error("Missing room for mob " + reset.roomVnum);
            }
            const existingGlobal = this.countEntities(mobTemplate.vnum);
            const existingRoom = room.countEntities(mobTemplate.vnum);
            if (existingGlobal < reset.maxWorld && existingRoom < reset.maxRoom) {
                this.entityTemplateToWorld(mobTemplate, room);
            } else {
                Logger.error(`There are ${existingGlobal}/${existingRoom} in the world already.`);
            }
        });
        resets.items.forEach((reset) => {
            const objTemplate = this.findObjectTemplate(reset.itemVnum);
            if (!objTemplate) {
                Logger.error("Missing item template " + reset.itemVnum);
                return;
            }
            const room = this.findRoom(reset.roomVnum);
            if (!room) {
                Logger.error("Missing room for item " + reset.roomVnum);
            }
            const existingRoom = room.countItems(objTemplate.vnum);
            const existingGlobal = 0;
            if (existingRoom === 0) {
                this.objectTemplateToContainer(objTemplate, room);
            } else {
                Logger.error(`There are ${existingGlobal}/${existingRoom} items in the world already.`);
            }
        });


        const rooms = area.rooms;
        rooms.forEach((room) => {
            room.exits.forEach((exit) => {
                if (exit.exitFlags !== exit.defaultExitFlags) {
                    console.log(`Resetting exit flags in ${room.name} from ${exit.exitFlags} to ${exit.defaultExitFlags}`);
                    exit.exitFlags = exit.defaultExitFlags;
                }
            })
        })
    }

    public showEquipmentToEntity(inputEntity: Actor, lookEntity: Actor) {
        lookEntity.templateToEntity(TemplateService.createEquipmentView(inputEntity, lookEntity));
    }
    public showScoreToEntity(inputEntity: Actor) {
        inputEntity.templateToEntity(TemplateService.createScoreView(inputEntity));
    }

    public showRoomToEntity(inputEntity: Actor, currentRoom: Room = inputEntity.currentRoom) {
        if (inputEntity.getPosition() <= Position.POS_SLEEPING) {
            inputEntity.stringToEntity(`You are ${inputEntity.getPositionName()}!`);
            return;
        }
        inputEntity.templateToEntity(TemplateService.createRoomView(inputEntity, currentRoom));
    }

    public entityTemplateToWorld(mobTemplate: IActorTemplate, room: Room) {
        Logger.info(`Adding NPC ${mobTemplate.vnum} Entity to world.`);
        const entity = new Actor(mobTemplate);
        this.moveEntityToRoom(entity, room);
        this.entities.push(entity);
    }

    public objectTemplateToContainer(objTemplate: IItemTemplate, entity: IItemContainer) {
        const item = new Item(objTemplate);
        this.moveItemToContainer(item, entity);
    }

    public moveEntityToVnum(entity: Actor, vnum: string | number): boolean {
        const room = this.findRoom(vnum);
        if (room) {
            this.moveEntityToRoom(entity, room);
            return true;
        }
        return false;
    }

    private moveEntityToRoom(entity: Actor, iRoom: IEntityContainer) {
        if (entity.currentRoom) {
            entity.currentRoom.removeEntity(entity);
        }
        iRoom.addEntity(entity);
    }

    public moveItemToContainer(item: Item, entity: IItemContainer) {
        if (item.inside) {
            item.inside.removeItem(item);
        }
        entity.addItem(item);
        item.inside = entity;
    }

    public countEntities(vnum: number | string): number {
        return this.entities.filter((x) => x.template === vnum).length;
    }
}
