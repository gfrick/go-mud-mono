import World from "./world";
import {Actor, IPlayerController} from "../domain/actor";
import {CommandService} from "../services/command.service";
import {SaveService} from "../services/save.service";
import {IPlayer, playerModel} from "../repository/player.model";
import {ScriptService} from "../services/script.service";
import { ViewService} from "../services/view.service";
import Logger from "./logger";

interface IGameServer {
    canLoadPlayerEntity(controller: IPlayerController): boolean;
    removePlayerEntity(controller: IPlayerController): void;
    loadPlayerEntity(controller: IPlayerController): Promise<boolean>;
    tick(): void;
}

class Mud implements IGameServer {
    public world: World;
    public players: Actor[];
    public tickCount: number;
    public progressTick: number;
    public scriptService: ScriptService;
    public commandService: CommandService;

    public constructor() {
        this.world = new World();
        this.players = [];
        this.tickCount = 0;
        this.progressTick = 0;
        this.scriptService = new ScriptService(this.world);
        this.commandService = new CommandService(new ViewService(), this.world, this.scriptService);
    }

    public init = async (): Promise<boolean> => {
        let success = await this.world.init();
        if( success ) {
            this.commandService.registerCommands();
        }
        return success;
    }

    public tick = () => {
        this.runPlayerCommands();
        this.tickCount++;
        if (this.tickCount >= 60) {
            Logger.info("TICK");
            this.tickCount = 0;
            if (!this.world.isReady()) {
                Logger.error("But there is no world");
            }
            this.world.tick();
            this.saveProgress();
        }
    }

    private runPlayerCommands() {
        const players = this.players;
        const length = players.length;
        for( let pIndex = 0; pIndex < length; pIndex++) {
            const inputEntity = players[pIndex];
            const input = players[pIndex].controller.handlePlayerInput();
            this.commandService.runCommand(inputEntity, input);
        }
        // Get ticked scripts here and run them.
    }

    private saveProgress = () => {
        SaveService.saveProgress(this.players).then((result) => {
            Logger.debug("Player progress saved");
        })
    }

    public removePlayerEntity = (controller: IPlayerController): void => {
        let removeIndex = -1;
        let removeEntity = null;
        this.players.forEach((existingController, index) => {
            if (controller === existingController.controller) {
                removeEntity = existingController;
                removeIndex = index;
            }
        });
        if (removeIndex >= 0) {
            this.players.splice(removeIndex,1);
            this.world.removeEntity(removeEntity);
            removeEntity.controller = null;
        }
    }

    public canLoadPlayerEntity(controller: IPlayerController): boolean {
        let foundEntity = null;
        this.players.forEach((existingController) => {
            if (controller.getPlayerId() === existingController.controller.getPlayerId()) {
                foundEntity = existingController;
            }
        });
        return foundEntity === null;
    }

    public loadPlayerEntity = async (controller: IPlayerController): Promise<boolean> => {
        const playerData = await SaveService.loadPlayerTemplate(controller.getPlayerId());
        if (!playerData) {
            Logger.error("FATAL: Attempt to load player failed for " + controller.getPlayerId());
            return false;
        }
        Logger.info(`Loading ${playerData.name} to room ${playerData.state.lastRoomVnum}`);
        const entity = this.world.loadEntity( playerData.state, playerData.state.lastRoomVnum || 5001);

        entity.controller = controller;
        this.players.push(entity);
        this.commandService.findCommand("look").invoke(entity);
        return true;
    }

}

export {Mud, IGameServer}
