import {IGameServer} from "./mud";
import WebSocket, {Data} from 'ws';
import { User} from "../models/user";
import {IPlayer} from "../repository/player.model";
import Logger from "./logger";
import {IAccount} from "../models/account";
import {IPlayerController} from "../domain/actor";

export default class ConnectionHost {
    public gameServer: IGameServer;
    public users: IPlayerController[];

    constructor(gameServer: IGameServer) {
        this.users = [];
        this.gameServer = gameServer;
    }

    public handleConnection(ws: WebSocket, wss: WebSocket.Server, player: IPlayer, account: IAccount) : void{
        const newUser = new User(player.id, account, ws);
        newUser.queueHostOutput({
            message: 'Connected.'
        });
        if( !this.gameServer.canLoadPlayerEntity(newUser)) {
            newUser.queueHostOutput({
                message: "Character is already playing."
            });
            ws.close(4403, "already_playing");
            return;
        }
        const entity = this.gameServer.loadPlayerEntity(newUser);
        if (entity) {
            ws.on('message', (data: Data) => {
                newUser.queueHostInput(data);
            });
            ws.on('close', (data: Data) => {
                console.log("Connection closed");
                this.gameServer.removePlayerEntity(newUser);
            });
            ws.on('pong', (data: Data) => {
                console.log("*** PONG ***");
                console.log(data);
            });
            ws.ping();
            this.users.push(newUser);
            return;
        }
        newUser.queueHostOutput({
            message: "Unable to load character."
        });
        ws.close(4404,"player_not_found");
    }

    public hostTick() : void {
        this.runUserCommands()
        this.gameServer.tick();
        this.outputToUsers();
    }

    public runUserCommands() : void {
        this.users.forEach((inputUser) => {
            if (!inputUser.isInputReady()) return;
            const input = inputUser.handleHostInput();
            if (this.runHostCommand(input)) return;
            inputUser.queuePlayerInput(input);
        });
    }

    public outputToUsers(): void {
        this.users.forEach((outputUser) => {
            while (outputUser.isOutputReady()) {
                const output = outputUser.handleHostOutput();
                if (!output) return;
                outputUser.sendHostOutput(output);
            }
        });
    }

    public runHostCommand(data: any): boolean {
        Logger.info("Host Command Parser got " + data);
        return false;
    }
}

