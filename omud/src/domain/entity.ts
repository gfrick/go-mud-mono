import {IGameEvent, Trigger} from "./events/events";

interface EntityDto {
     properties: { [p: string]: number | string };
     tags: string[];
     triggers: Trigger[];
}

class Entity {
    protected properties: { [p: string]: number | string };
    protected tags: string[];
    protected triggers: Trigger[];

    public getTriggers(event: IGameEvent): Trigger[] {
        if (this.triggers && this.triggers.length > 0) {
            return this.triggers.filter((fTrigger) => fTrigger.type === event.type)
        }
        return [];
    }

    public getEntityDto() : EntityDto {
        return {
            properties: this.properties,
            tags: this.tags,
            triggers: this.triggers
        }
    }
}

export { Entity, EntityDto }
