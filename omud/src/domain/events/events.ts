// 1. You picked up your first item!
// 2. You equipped your first item!
// 3. Collect all keys in area
// 4. Picked all locks in area
// 5. killed 10 rats
// 6.

/**
 * The design here has Three primary parts
 * Trigger: A record of what entity should run what scripts when
 * Event: Something that might match to a trigger
 * Run: Running the script with a context, called a Trigger
 */

import {Actor} from "../actor";
import {Item} from "../item";
import {Room} from "../room";
import {IExit} from "../exit";
import {Area} from "../area";

interface Trigger {
    vnum: number,
    script: number, // script.
    type: TriggerType,
    typeArg: string
    // You likely want flags here like "Only self"
    // OR, the TriggerType determines who can trigger on it.
}

enum TriggerType {
    Enter,
    Position,
    Leave,
    Get,
    Drop,
    Open,
    Close,
    Keyword,
    Tick,
    Fail,
    None,
    Wear,
    Remove
}

interface ITriggerInfo {
    name: string,
    type: TriggerType,
    help: string,
    argAllowed: boolean
}

const AllTriggers: ITriggerInfo[] = [
    {
        name: "Enter",
        type: TriggerType.Enter,
        help: "Happens when entity enters a room. Arg can be number between 1 and 100 that is the chance of trigger. Defaults to 100%",
        argAllowed: true
    },
    {
        name: "Leave",
        type: TriggerType.Leave,
        help: "Happens when entity leaves a room. Arg can be number between 1 and 100 that is the chance of trigger. Defaults to 100%",
        argAllowed: true
    },
    {
        name: "Change Position",
        type: TriggerType.Position,
        help: "Happens when entity changes position. Arg is position: sleep,stand,rest,sit",
        argAllowed: true
    },
    {
        name: "Get",
        type: TriggerType.Get,
        help: "Happens when entity gets an item. For items it triggers when they are picked up.",
        argAllowed: false
    },
    {
        name: "Drop",
        type: TriggerType.Drop,
        help: "Happens when entity drops an item. For items it triggers when they are dropped.",
        argAllowed: false
    },
    {
        name: "Open",
        type: TriggerType.Open,
        help: "Happens when entity opens a door.",
        argAllowed: false
    },
    {
        name: "Close",
        type: TriggerType.Close,
        help: "Happens when entity closes a door. asdf",
        argAllowed: false
    },
    {
        name: "Keyword",
        type: TriggerType.Keyword,
        help: "Happens when entity uses a keyword. Won't work if there is an existing command. Arg is the keyword.",
        argAllowed: true
    }

];

interface IGameScript {
    vnum: number;
    name: string;
    script: string;
}

interface ITriggerContainer {
    getTriggers(event: IGameEvent): Trigger[];
    getTriggeredEvents(event: IGameEvent): ITriggeredEvent[];
}

interface IGameEvent {
    type: TriggerType,
    entity: Actor,
    target: Actor | Room | Item | IExit | Area | null,
    args: string[]
}

/**
 * An event was created by some action in the game.
 * The event was matched to a trigger and should be run.
 * The script should be run by the receiver with reference to the event and trigger.
 * Feel free to suggest a better name.
 */
interface ITriggeredEvent {
    event: IGameEvent,
    trigger: Trigger,
    receiver: Actor | Room | Item | IExit | Area
}

// So there is a Trigger, and an Event, what is the domain name for 'a triggered event'

const failEvent = (entity: Actor): IGameEvent => {
    return {
        type: TriggerType.Fail,
        entity: entity,
        target: null,
        args: []
    };
}

const noEvent = (entity: Actor): IGameEvent => {
    return {
        type: TriggerType.None,
        entity: entity,
        target: null,
        args: []
    };
}

export {
    IGameEvent,
    failEvent,
    noEvent,
    IGameScript,
    ITriggerContainer,
    Trigger, TriggerType,ITriggeredEvent,
    ITriggerInfo,AllTriggers
};
