import {BitFlag} from "./flags";

interface IExit {
    direction: number;
    description: string;
    toVnum: number;
    keyword: string;
    keyVnum: number;
    exitFlags: number;
    defaultExitFlags: number;
}

enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST,
    UP,
    DOWN,
    NONE// Represent NULL, UNDEFINED, invalid, etc.
}

enum ExitFlags {
    EX_ISDOOR = (BitFlag.A),
    EX_CLOSED = (BitFlag.B),
    EX_LOCKED = (BitFlag.C),
    EX_HIDDEN = (BitFlag.D),
    EX_PICKPROOF = (BitFlag.F),
    EX_NOPASS = (BitFlag.G),
    EX_EASY = (BitFlag.H),
    EX_HARD = (BitFlag.I),
    EX_INFURIATING = (BitFlag.J),
    EX_NOCLOSE = (BitFlag.K),
    EX_NOLOCK = (BitFlag.L)
}

class ExitService {
    private static oppositeName = ["the south", "the west", "the north", "the east", "below", "above"];
    private static reverseDirection = [Direction.SOUTH, Direction.WEST, Direction.NORTH, Direction.EAST, Direction.DOWN, Direction.UP];
    private static exits = ["north", "east", "south", "west", "up", "down"];

    public static dirName(direction: Direction): string {
        return this.exits[direction];
    }

    public static lookup(direction: string): Direction {
        const dir = this.exits.findIndex(x => x.startsWith(direction));
        return dir == -1 ? Direction.NONE : dir;
    }

    public static reverseDirName(direction: Direction): string {
        return this.oppositeName[direction];
    }

    public static reverseDir(direction: Direction): Direction {
        return this.reverseDirection[direction];
    }
}

export {IExit, Direction, ExitService, ExitFlags};

