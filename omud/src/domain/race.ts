import {Size} from "./size";

class Race {

    public name: string;
    public isPcRace: boolean;
    public movementMsg: string;
    public defaultSize: number;
    public defaultDamage: string;
    public prefix: string;

    constructor(name: string) {
        this.name = name;
        this.isPcRace = false;
        this.movementMsg = "-walks-";
        this.defaultSize = Size.Medium;
        this.prefix = "a";
        this.defaultDamage = "none";
    }

}

export {Race};
