 enum ItemType {
    ITEM_LIGHT = 1, // You can light it, it lasts a certain amount of time
    ITEM_SCROLL = 2, // can can read it to cast a spell
    ITEM_WAND = 3, // you can use it to cast a spell
    ITEM_STAFF = 4, // not sure why this is different then weapon
    ITEM_WEAPON = 5, // you can fight with this
    // 6, 7
    ITEM_TREASURE = 8, // turns into gold when you pick it up?
    ITEM_ARMOR = 9, // can be worn, provides protection
    ITEM_POTION = 10, // can quaff to cast a spell.
    ITEM_CLOTHING = 11, // can be worn
    ITEM_FURNITURE = 12, // can be sit on, providing healing/mana benefit
    ITEM_TRASH = 13, //
    // 14
    ITEM_CONTAINER = 15, // can put items in, take out
    // 16
    ITEM_DRINK_CON = 17, // can be 'filled', drank from, and has container size/etc
    ITEM_KEY = 18, // duh
    ITEM_FOOD = 19, // can be eaten, can provide benefits, etc
    ITEM_MONEY = 20, // not sure the difference from treasure
    // 21
    ITEM_BOAT = 22, // dumb.
    ITEM_CORPSE_NPC = 23,
    ITEM_CORPSE_PC = 24,
    ITEM_FOUNTAIN = 25, // can be drank from, is infinite.
    ITEM_PILL = 26, // can be eaten.
    // 27
    ITEM_MAP = 28, // really this is a 'document'... it contains text.
    ITEM_PORTAL = 29, // represents a portal in a room when the spell is cast.
    ITEM_WARP_STONE = 30,
    ITEM_ROOM_KEY = 31,
    ITEM_GEM = 32,
    ITEM_JEWELRY = 33,
    ITEM_JUKEBOX = 34,
    ITEM_PROJECTILE = 35,
    ITEM_UPGRADE = 36,
    ITEM_ORE = 37,
    ITEM_RESTRING = 38,
    ITEM_ENCHANT = 39
}


const ItemTypeSet = [
    {type: ItemType.ITEM_LIGHT, name: 'Light'},
    {type: ItemType.ITEM_SCROLL, name: 'Scroll'},
    {type: ItemType.ITEM_WAND, name: 'Wand'},
    {type: ItemType.ITEM_STAFF, name: 'Staff'},
    {type: ItemType.ITEM_WEAPON, name: 'Weapon'},
    {type: ItemType.ITEM_TREASURE, name: 'Treasure'},
    {type: ItemType.ITEM_ARMOR, name: 'Armor'},
    {type: ItemType.ITEM_POTION, name: 'Potion'},
    {type: ItemType.ITEM_CLOTHING, name: 'Clothing'},
    {type: ItemType.ITEM_FURNITURE, name: 'Furniture'},
    {type: ItemType.ITEM_TRASH, name: 'Trash'},
    {type: ItemType.ITEM_CONTAINER, name: 'Container'},
    {type: ItemType.ITEM_DRINK_CON, name: 'Drink Container'},
    {type: ItemType.ITEM_KEY, name: 'Key'},
    {type: ItemType.ITEM_FOOD, name: 'Food'},
    {type: ItemType.ITEM_MONEY, name: 'Money'},
    {type: ItemType.ITEM_BOAT, name: 'Boat'},
    {type: ItemType.ITEM_FOUNTAIN, name: 'Fountain'},
    {type: ItemType.ITEM_PILL, name: 'Pill'},
    {type: ItemType.ITEM_MAP, name: 'Map'},
    {type: ItemType.ITEM_PORTAL, name: 'Portal'},
    {type: ItemType.ITEM_WARP_STONE, name: 'Warp Stone'},
    {type: ItemType.ITEM_ROOM_KEY, name: 'Room Key'},
    {type: ItemType.ITEM_GEM, name: 'Gem'},
    {type: ItemType.ITEM_JEWELRY, name: 'Jewelry'},
    {type: ItemType.ITEM_JUKEBOX, name: 'Jukebox'},
    {type: ItemType.ITEM_ORE, name: 'Ore'}
];

export {ItemType, ItemTypeSet };
