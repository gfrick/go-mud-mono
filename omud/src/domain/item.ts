import {IGameEvent, Trigger, ITriggerContainer, ITriggeredEvent} from "./events/events";
import {ItemType} from "./item-type";
import {IItemContainer} from "./container";
import {Actor} from "./actor";
import {Entity} from "./entity";

interface IItemTemplate {
    vnum: number;
    name: string;
    shortDesc: string;
    longDesc: string;
    description: string;
    level: number;
    material: string;
    condition: number;
    cost: number;
    weight: number;
    wearFlags: number;
    type: number;
    properties: { [p: string]: number | string };
    tags: string[];
    triggers: Trigger[];
}

interface ItemDto extends IItemTemplate {}

class Item extends Entity implements IItemContainer, ITriggerContainer {

    private readonly template: IItemTemplate;
    public inside?: IItemContainer;
    public readonly type: ItemType;
    public wearFlags: number;
    public wornBy?: Actor;
    items: Item[];

    constructor(template: IItemTemplate) {
        super();
        this.template = template;
        this.type = template.type;
        this.items = [];
        this.wearFlags = template.wearFlags;
        // TODO, this likely shouldn't be shallow? They likely should go in the state so they're saveD?!?!
        this.tags = template.tags.slice();
        this.triggers = template.triggers.slice();
        this.properties = template.properties;
    }

    // TODO, more here later... as this will currently lose any contained items or new tags...
    // this should only have the template number.
    public getItemDto(): ItemDto {
        return this.template;
    }

    getTriggeredEvents(event: IGameEvent): ITriggeredEvent[] {
        const item = this;
        if( this.triggers && this.triggers.length > 0) {
            return this.triggers.filter((fTrigger) => fTrigger.type === event.type).map(
                (trigger) => {
                    return {
                        event: event,
                        trigger: trigger,
                        receiver: item
                    };
                }
            )
        }
        return [];
    }

    addItem(item: Item): void {
        if (this.type !== ItemType.ITEM_CONTAINER) {
            throw "not_a_container";
        }
        this.items.push(item);
        item.inside = this;
    }

    getTriggers(event: IGameEvent): Trigger[] {
        if (this.triggers && this.triggers.length > 0) {
            return this.triggers.filter((fTrigger) => fTrigger.type === event.type)
        }
        // Not yet sure if this is a good idea.
        // item.inside is a container....
        // item.inside.forEach((item) => {
        //     triggers.push(item.getTriggers(event));
        // });
        return [];
    }

    removeItem(item: Item): void {
        if (this.type !== ItemType.ITEM_CONTAINER) {
            throw "not_a_container";
        }
        const itemIdx = this.items.findIndex(x => x === item);
        this.items.splice(itemIdx, 1);
        item.inside = null;
    }

    findItem(name: string): Item | null {
        if (this.type !== ItemType.ITEM_CONTAINER) {
            throw "not_a_container";
        }
        return this.items.find(x => x.getName().includes(name));
    }

    public countItems(vnum: number) {
        return this.items.filter((x) => x.getVnum() != undefined
            && x.getVnum() === vnum).length;
    }

    public getShortDesc(): string {
        return this.template.shortDesc;
    }

    public getLongDesc(): string {
        return this.template.longDesc;
    }

    public getName(): string {
        return this.template.name;
    }

    public getVnum(): number {
        return this.template.vnum;
    }
}

export {IItemTemplate, Item, ItemDto};
