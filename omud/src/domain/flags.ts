enum BitFlag {
    A = 1,
    B = 2,
    C = 4,
    D = 8,
    E = 16,
    F = 32,
    G = 64,
    H = 128,
    I = 256,
    J = 512,
    K = 1024,
    L = 2048,
    M = 4096,
    N = 8192,
    O = 16384,
    P = 32768,
    Q = 65536,
    R = 131072,
    S = 262144,
    T = 524288,
    U = 1048576,
    V = 2097152,
    W = 4194304,
    X = 8388608,
    Y = 16777216,
    Z = 33554432,
    aa = 67108864,
    bb = 134217728,
    cc = 268435456,
    dd = 536870912,
    ee = 1073741824
}

class BitFlags {
    public static hasFlag(value: number, flag: number) {
        return (value & flag) === flag;
    }

    public static addFlag(value: number, flag: number): number {
        return (value | flag);
    }

    public static removeFlag(value: number, flag: number): number {
        return value & (~flag);
    }
}

export {BitFlag, BitFlags}
