import {Actor} from "./actor";
import {Item} from "./item";

interface IEntityContainer {
    addEntity(entity: Actor);

    removeEntity(entity: Actor);

    findEntity(entity: Actor): Actor | null;

    countEntities(vnum: number): number;
}

interface IItemContainer {
    addItem(item: Item);

    removeItem(item: Item);

    findItem(name: string): Item | null;
}

export {IItemContainer, IEntityContainer};
