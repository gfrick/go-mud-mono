import {IActorTemplate} from "./actor";
import * as _ from "lodash";
import {IItemTemplate} from "./item";
import {IRoom, Room} from "./room";
import { IResetList} from "./reset";
import {IAreaHeader} from "./area-header";
import {IGameScript} from "./events/events";

interface IArea {
    header: IAreaHeader;
    _id: string;
    rooms: IRoom[];
    items: IItemTemplate[];
    actors: IActorTemplate[];
    resets: IResetList;
    scripts: IGameScript[];
}

class Area {
    constructor(data: IArea) {
        this.header = data.header;
        this.id = data._id;
        this.mobTemplates = data.actors;
        this.objectTemplates = data.items;
        this.roomTemplates = data.rooms;
        this.rooms = this.createRooms(data.rooms);
        this.resets = data.resets;
        this.scripts = data.scripts || [];
    }

    private createRooms(roomTemplates: IRoom[]) {
        const rooms = [];
        roomTemplates.forEach((template) => {
            rooms.push(new Room(_.cloneDeep(template)));
        })
        return rooms;
    }

    header: IAreaHeader;
    readonly id: string;
    private mobTemplates: IActorTemplate[];
    private objectTemplates: IItemTemplate[];
    readonly resets: IResetList;
    readonly rooms: Room[];
    private roomTemplates: IRoom[];
    private scripts: IGameScript[];

    public findEntityTemplate(vnum: number): IActorTemplate | undefined {
        return this.mobTemplates.find(x => x.vnum == vnum);
    }

    public findObjectTemplate(vnum: number): IItemTemplate | undefined {
        return this.objectTemplates.find(x => x.vnum == vnum);
    }

    public findRoom(vnum: string | number): Room | null {
        return this.rooms.find(room => room.vnum == vnum);
    }

    public findScript(vnum: number): IGameScript | null {
        return this.scripts.find(s => s.vnum == vnum);
    }
}

export {IArea, Area};
