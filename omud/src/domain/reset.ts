interface IResetList {
    actors: IActorSpawn[];
    items: IItemSpawn[];
}

interface IActorSpawn {
    actorVnum: number;
    roomVnum: number;
    maxWorld: number;
    maxRoom: number;
}

interface IItemSpawn {
    itemVnum: number;
    roomVnum: number;
    maxWorld: number;
    maxRoom: number;
}

export {IResetList, IActorSpawn, IItemSpawn};
