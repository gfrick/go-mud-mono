enum Size {
    Tiny = 0,
    Small = 1,
    Medium = 2,
    Large = 3,
    Gargantuan = 4
}

class SizeService {

    public static readonly sizes = [
        {size: 0, description: "Extra Small"},
        {size: 1, description: "Small"},
        {size: 2, description: "Medium"},
        {size: 3, description: "Large"},
        {size: 4, description: "Extra Large"},
    ];

    public static valueOf(size: string): Size {
        let size2 = size.toLowerCase().trim();
        if (size2 === ("extra small") || size === ("XS"))
            return Size.Tiny;
        else if (size2 === ("small") || size === ("S"))
            return Size.Small;
        else if (size2 === ("medium") || size === ("M"))
            return Size.Medium;
        else if (size2 === ("large") || size === ("L"))
            return Size.Large;
        else if (size2 === ("extra large") || size === ("XL"))
            return Size.Gargantuan;
        else
            return Size.Medium;
    }

    public static toString(size: Size): string {
        switch (size) {
            case Size.Tiny:
                return "Extra Small";
            case Size.Small:
                return "Small";
            case Size.Medium:
                return "Medium";
            case Size.Large:
                return "Large";
            case Size.Gargantuan:
                return "Extra Large";
            default:
                return "!SIZE CLASS!";
        }

    }

}

export {Size, SizeService};
