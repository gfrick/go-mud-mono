interface IAreaHeader {
    builder: string;
    lowVnum: number;
    highVnum: number;
    areaName: string;

}

export {IAreaHeader};
