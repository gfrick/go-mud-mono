import {BitFlag} from "./flags";

enum WearFlags {
    Take = BitFlag.A,
    Finger = BitFlag.B,
    Neck = BitFlag.C,
    Body = BitFlag.D,
    Head = BitFlag.E,
    Legs = BitFlag.F,
    Feet = BitFlag.G,
    Hands = BitFlag.H,
    Arms = BitFlag.I,
    Shield = BitFlag.J,
    About = BitFlag.K,
    Waist = BitFlag.L,
    Wrist = BitFlag.M,
    Wield = BitFlag.N,
    Hold = BitFlag.O,
    NoSacrifice = BitFlag.P
}

export {WearFlags};
