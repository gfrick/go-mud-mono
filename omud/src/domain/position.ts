
export interface IPosition {
    name: string
}

export enum Position {
    POS_DEAD = 0,
    POS_MORTAL = 1,
    POS_INCAP = 2,
    POS_STUNNED = 3,
    POS_SLEEPING = 4,
    POS_RESTING = 5,
    POS_SITTING = 6,
    POS_FIGHTING = 7,
    POS_STANDING = 8,
}

export const Positions: IPosition[] = [
    {
        name: "dead"
    },
    {
        name: "mortally wounded"
    },
    {
        name: "incapacitated"
    },
    {
        name: "stunned"
    },
    {
        name: "sleeping"
    },
    {
        name: "resting"
    },
    {
        name: "sitting"
    },
    {
        name: "fighting"
    },
    {
        name: "standing"
    }
] ;
