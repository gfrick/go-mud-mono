enum Sector {
    Inside, City, Field, Forest,
    Hills, Mountain,
    Swim, NoSwim, Unused, Air, Desert, Jungle,
    Underwater, Marsh, Swamp, Tundra, Rainforest, Cavern
}

class SectorService {

    public static readonly sectors =
        ["Inside", "City", "Field", "Forest", "Hills", "Mountain",
            "Swim", "No swim", "Unused", "Air", "Desert", "Jungle",
            "Underwater", "Marsh", "Swamp", "Tundra", "Rainforest", "Cavern"];

}

export { Sector };
