import {Position, Positions} from "./position";
import {Room} from "./room";
import {IGameEvent, Trigger, ITriggerContainer, ITriggeredEvent} from "./events/events";
import {IItemContainer} from "./container";
import WebSocket from "ws";
import {Equipment, EquipmentDto} from "./equipment";
import {Entity} from "./entity";
import {Item, ItemDto} from "./item";
import {Sex} from "./sex";

interface IPlayerController {
    handlePlayerInput(): string;

    queuePlayerOutput(data: any): void;

    getPlayerId(): string;

    queueHostInput(data: WebSocket.Data): void;

    handleHostInput(): WebSocket.Data;

    handleHostOutput(): any;

    queueHostOutput(data: any): void;

    queuePlayerInput(data: WebSocket.Data);

    isInputReady(): boolean;

    isOutputReady(): boolean;

    sendHostOutput(data: any): void;
}

interface IActorTemplate {
    vnum: number;
    name: string;
    shortDesc: string;
    longDesc: string;
    description: string;
    size: number;
    level: number;
    alignment: number;
    race: string;
    hitRoll: number;
    damageType: string;
    startPosition: number;
    defaultPosition: number;
    sex: number;
    wealth: number;
    triggers: Trigger[];
    tags: string[];
    properties: { [key: string]: number | string };
}

interface ActorDto {
    lastRoomVnum: number;
    name: string;
    shortDesc: string;
    longDesc: string;
    description: string;
    level: number;
    race: string;
    position: number;
    sex: number;
    triggers: Trigger[];
    tags: string[];
    properties: { [key: string]: number | string };
    items: ItemDto[],
    equipment: EquipmentDto
}

class Actor extends Entity implements IItemContainer, ITriggerContainer {

    public controller?: IPlayerController;
    public currentRoom?: Room;
    public template: number = -1;
    name: string = "empty soul";
    shortDesc: string = "an empty soul";
    longDesc: string = "An empty soul floats here.";
    description: string = "A ghostly image with no discerning features";
    sex: Sex.NEUTRAL;
    level: number = 1;
    race: string = "human";
    position: number = Position.POS_STANDING;
    equipment: Equipment = new Equipment();
    items: Item[] = [];

    constructor(template?: IActorTemplate, state?: ActorDto) {
        super();
        if( template ) {
            this.template = template.vnum;
            Object.assign(this, template);
            this.tags = template.tags.slice();
            this.triggers = template.triggers.slice();
            this.properties = template.properties;
        }
        if( state ) {
            console.log(state);
            Object.assign(this, state);
            this.items = state.items.map(dto => new Item(dto));
            this.equipment = new Equipment(state.equipment);
        }
    }

    public getActorDto(): ActorDto {
        let entityDto = super.getEntityDto();
        let save = {
            lastRoomVnum: this.currentRoom ? this.currentRoom.vnum : -1,
            name: this.name,
            description: this.description,
            level: this.level,
            longDesc: this.longDesc,
            position: this.position,
            race: this.race,
            shortDesc: this.shortDesc,
            properties: entityDto.properties,
            sex: this.sex,
            tags: entityDto.tags,
            triggers: entityDto.triggers,
            items: this.items.map(item => item.getItemDto()),
            equipment: this.equipment.getEquipmentDto()
        }
        console.log( JSON.stringify(save));
        return save;
    }

    public countItems(vnum: number) {
        return this.items.filter((x) => x.getVnum() != undefined
            && x.getVnum() === vnum).length;
    }

    public addItem(item: Item): void {
        this.items.push(item);
        item.inside = this;
    }

    public removeItem(item: Item): void {
        const itemIdx = this.items.findIndex(x => x === item);
        this.items.splice(itemIdx, 1);
        item.inside = null;
    }

    public findItem(name: string): Item | undefined {
        return this.items.find(x => x.getName().includes(name));
    }

    getTriggeredEvents(event: IGameEvent): ITriggeredEvent[] {
        let triggers = [];
        const actor = this;
        if (this.triggers && this.triggers.length > 0) {
            const triggered = this.triggers.filter((fTrigger) => fTrigger.type === event.type).map(
                (trigger) => {
                    return {
                        event: event,
                        trigger: trigger,
                        receiver: actor
                    };
                }
            );
            triggers = triggers.concat(triggered);
        }
        // May have something in their inventory, such as the item they just picked up.
        this.items.forEach((item) => {
            triggers = triggers.concat(item.getTriggeredEvents(event));
        });
        return triggers;
    }

    public stringToEntity(s: string): void {
        if (this.controller) {
            this.controller.queuePlayerOutput({
                "message": s
            });
        }
    }

    getTriggers(event: IGameEvent): Trigger[] {
        let triggers = super.getTriggers(event);

        // May have something in their inventory, such as the item they just picked up.
        this.items.forEach((item) => {
            triggers = triggers.concat(item.getTriggers(event));
        });
        return triggers;
    }

    public templateToEntity(data: any) {
        if (this.controller) {
            this.controller.queuePlayerOutput(data);
        }
    }

    public getLevel(): number {
        return this.level;
    }

    public getShortName(): string {
        return this.shortDesc;
    }

    public getPositionName(): string {
        return Positions[this.position].name;
    }

    public getPosition(): Position {
        return this.position;
    }

    public getRace(): string {
        return this.race;
    }

    public getSex(): Sex {
        return this.sex;
    }

    findEntityInRoom(args: string): Actor | null {
        return this.currentRoom.entities.find(x => x.name.indexOf(args) != -1);
    }
}

export {IActorTemplate, IPlayerController, Actor, ActorDto};
