import {Direction, ExitService, IExit} from "./exit";
import {Actor} from "./actor";
import {Item} from "./item";
import {IGameEvent, Trigger, ITriggerContainer, ITriggeredEvent} from "./events/events";
import {IEntityContainer, IItemContainer} from "./container";
import {Entity} from "./entity";

interface IRoom  {
    vnum: number;
    sector: number;
    flags: number;
    description: string;
    name: string;
    exits: IExit[];
    mapX: number;
    mapY: number;
    triggers: Trigger[];
    tags: string[];
    properties: { [key: string]: number | string };
}

class Room extends Entity implements IItemContainer, IEntityContainer, ITriggerContainer {
    private template: IRoom;

    public entities: Actor[] = [];
    public readonly vnum: number = -1;
    public readonly description: string = "this place should not exist.";
    public readonly name: string = "nowhere";
    public exits: IExit[] = [];
    public items: Item[] = [];

    constructor(template: IRoom) {
        super();
        this.template = template;
        this.vnum = template.vnum;
        this.description = template.description;
        this.name = template.name;
        this.exits = template.exits;
        this.entities = [];
        // TODO, this likely shouldn't be shallow? They likely should go in the state so they're saveD?!?!
        this.tags = template.tags ? template.tags.slice() : [];
        this.triggers = template.triggers ? template.triggers.slice() : [];
        this.properties = template.properties;
    }

    public countItems(vnum: number) {
        return this.items.filter((x) => x.getVnum() != undefined
            && x.getVnum() === vnum).length;
    }

    public addItem(item: Item): void {
        this.items.push(item);
        item.inside = this;
    }

    public removeItem(item: Item): void {
        const itemIdx = this.items.findIndex(x => x === item);
        this.items.splice(itemIdx, 1);
        item.inside = null;
    }

    public findItem(name: string): Item | undefined {
        return this.items.find(x => x.getName().includes(name));
    }

    getTriggers(event: IGameEvent): Trigger[] {
        let triggers = super.getTriggers(event);

        this.items.forEach((item) => {
            triggers = triggers.concat(item.getTriggers(event));
        });
        this.entities.forEach((actor) => {
            triggers = triggers.concat(actor.getTriggers(event));
        });
        return triggers;
    }

    getTriggeredEvents(event: IGameEvent): ITriggeredEvent[] {
        let triggers: ITriggeredEvent[] = [];
        const room = this;
        if (this.triggers && this.triggers.length > 0) {
            const triggered = this.triggers.filter((fTrigger) => fTrigger.type === event.type).map(
                (trigger) => {
                    return {
                        event: event,
                        trigger: trigger,
                        receiver: room
                    };
                }
            )
            triggers = triggers.concat(triggered);
        }
        this.items.forEach((item) => {
            triggers = triggers.concat(item.getTriggeredEvents(event));
        });
        this.entities.forEach((actor) => {
            triggers = triggers.concat(actor.getTriggeredEvents(event));
        });
        return triggers;
    }

    addEntity(entity: Actor) {
        this.entities.push(entity);
        entity.currentRoom = this;
    }

    findEntity(entity: Actor): Actor | null {
        return this.entities.find(x => x === entity);
    }

    removeEntity(entity: Actor) {
        const entityIdx = this.entities.findIndex(x => x === entity);
        this.entities.splice(entityIdx, 1);
        entity.currentRoom = null;
    }

    countEntities(vnum: number): number {
        return this.entities.filter((x) => x.template === vnum).length;
    }

    public findExitByDirection(direction: Direction): IExit | null {
        return this.exits.find(exit => exit.direction === direction);
    }

    public findExitByName(name: string): IExit | null {
        const asDirection = ExitService.lookup(name);
        if (asDirection != Direction.NONE) {
            return this.exits.find(exit => exit.direction === asDirection);
        }
        return this.exits.find(exit => exit.keyword && exit.keyword.startsWith(name));
    }
}

export {IRoom, Room};
