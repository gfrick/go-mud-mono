import { BitFlags} from "./flags";
import {Item, ItemDto} from "./item";
import {WearFlags} from "./wear-flags";

interface IWornItem {
    slotDesc: string;
    itemDesc: string;
}

interface EquipmentDto {
    equipment: WearPositionDto[];
}

interface WearPositionDto {
    wearFlags: WearFlags;
    description: string;
    slot: ItemDto | null;
}

class Equipment {
    private readonly equipment: WearPosition[];

    constructor(dto?: EquipmentDto) {
        this.equipment = [
            new WearPosition(WearFlags.Head, "head"),
            new WearPosition(WearFlags.About, "about"),
            new WearPosition(WearFlags.Neck, "neck"),
            new WearPosition(WearFlags.Body, "body"),
            new WearPosition(WearFlags.Arms, "arms"),
            new WearPosition(WearFlags.Hands, "hands"),
            new WearPosition(WearFlags.Waist, "waist"),
            new WearPosition(WearFlags.Legs, "legs"),
            new WearPosition(WearFlags.Feet, "feet"),
            new WearPosition(WearFlags.Shield, "shield"),
            new WearPosition(WearFlags.Wrist, "wrist1"),
            new WearPosition(WearFlags.Wrist, "wrist2"),
            new WearPosition(WearFlags.Finger, "finger1"),
            new WearPosition(WearFlags.Finger, "finger2"),
            new WearPosition(WearFlags.Hold, "hold1"),
            new WearPosition(WearFlags.Hold, "hold2")
        ]
        if( dto ) {
            dto.equipment.forEach((eq)=> {
                if( eq.slot) {
                    this.equip(new Item(eq.slot));
                }
            });
        }
    }

    public getEquipmentDto() : EquipmentDto {
        return {
            equipment: this.equipment.map(equip => equip.getWearPositionDto())
        }
    }

    public canEquip(item: Item): boolean {
        return this.equipment.some( x => x.canEquip(item));
    }

    public equip(item: Item): boolean {
        if( !this.canEquip(item)) {
            return false;
        }
        let position = this.equipment.find(x => x.canEquip(item));
        if( position === undefined) {
            return false;
        }
        return position.equip(item);
    }

    public remove(name: string): Item | null {
        let result = this.equipment.find(x => x.hasItem(name));
        if( result ) {
            return result.remove();
        }
        return null;
    }

    public hasItem(name: string): boolean {
        return this.equipment.some( x => x.hasItem(name));
    }

    public getItem(): Item[]  {
        return this.equipment.filter(x => x.hasItem()).map(x => x.getItem());
    }

    public getItemView(/* Pass who is looking? */): IWornItem[] {
        return this.equipment.map(x => x.getItemView());
    }
}

class WearPosition {
    private readonly wearFlags: WearFlags;
    private readonly description: string;
    private slot: Item | null;

    constructor(wearFlag: WearFlags,  description: string) {
        this.wearFlags = wearFlag;
        this.description = description;
        this.slot = null;
    }

    public getWearPositionDto(): WearPositionDto {
        return {
            wearFlags: this.wearFlags,
            description: this.description,
            slot: this.slot ? this.slot.getItemDto() : null
        }
    }

    public canEquip(item: Item): boolean {
        return ( this.slot === null && BitFlags.hasFlag(item.wearFlags, this.wearFlags));
    }

    public equip(item: Item): boolean {
        if( !this.canEquip(item)) {
            return false;
        }
        this.slot = item;
        return true;
    }

    public remove(): Item | null {
        let result = this.slot;
        this.slot = null;
        return result;
    }

    public hasItem(name?: string): boolean {
        if( name) {
            return this.slot !== null ? this.slot.getName().includes(name) : false;
        }
        return this.slot !== null;
    }

    public getItem(): Item | null {
        return this.slot;
    }

    public getItemView(/* Pass who is looking? */): IWornItem {
        return {
            slotDesc: this.description,
            itemDesc: this.hasItem() ? this.slot.getShortDesc() : "Nothing"
        };
    }
}

export { WearPosition, IWornItem, Equipment, EquipmentDto, WearPositionDto }
