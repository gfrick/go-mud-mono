interface INewAccount {
    username: string;
    password: string;
}

interface IAccount extends INewAccount {
    id: string;
    permissions: string[];
}

export {IAccount, INewAccount};
