import WebSocket from "ws";
import {IPlayerController} from "../domain/actor";
import {IAccount} from "./account";

class User implements IPlayerController {
    private readonly account: IAccount;
    private readonly playerId: string;
    private readonly connection: WebSocket;
    private inputBuffer: WebSocket.Data[];
    private outputBuffer: any[];
    private commandQueue: string[];

    constructor(playerId: string, account: IAccount, connection: WebSocket) {
        this.playerId = playerId;
        this.account = account;
        this.connection = connection;
        this.inputBuffer = [];
        this.outputBuffer = [];
        this.commandQueue = [];
    }

    getPlayerId(): string {
        return this.playerId;
    }

    sendHostOutput(data: any): void {
        this.connection.send(JSON.stringify(data));
    }

    handleHostOutput() {
        return this.outputBuffer.shift();
    }

    isInputReady(): boolean {
        return this.connection.readyState === WebSocket.OPEN && this.inputBuffer.length > 0;
    }

    isOutputReady(): boolean {
        return this.connection.readyState === WebSocket.OPEN && this.outputBuffer.length > 0
    }

    queueHostInput(data: WebSocket.Data) {
        this.inputBuffer.push(data);
    }

    queueHostOutput(data: any) {
        this.outputBuffer.push(data);
    }

    handlePlayerInput(): string {
        return this.commandQueue.shift();
    }

    queuePlayerOutput(data: any): void {
        this.outputBuffer.push(data);
    }

    handleHostInput(): WebSocket.Data {
        return this.inputBuffer.shift();
    }

    queuePlayerInput(data: WebSocket.Data) {
        if (typeof (data) === 'string') {
            this.commandQueue.push(JSON.parse(data).message);
        }
    }
}

export { User, WebSocket}
