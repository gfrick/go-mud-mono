import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IWorldService} from "../core/world";
import {noEvent} from "../domain/events/events";

export class MobLoadCommand implements IMudCommand {

    private worldService: IWorldService;

    constructor(worldService: IWorldService) {
        this.worldService = worldService;
    }

    invoke(inputEntity: Actor, args: string) {
        const template = this.worldService.findEntityTemplate(parseInt(args,10));
        if( !template ) {
            inputEntity.stringToEntity(`mobile with vnum ${args} not found.`);
            return noEvent(inputEntity);
        }
        this.worldService.entityTemplateToWorld(template, inputEntity.currentRoom);
        inputEntity.stringToEntity(`You summoned a ${template.shortDesc}.`);
        return noEvent(inputEntity);
    }
}
