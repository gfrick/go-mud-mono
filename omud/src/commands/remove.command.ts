import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IViewService} from "../services/view.service";
import {ActFlags} from "../domain/act-flags";
import {Position} from "../domain/position";
import {noEvent, TriggerType} from "../domain/events/events";

export class RemoveCommand implements IMudCommand {

    private viewService: IViewService;

    constructor( viewService: IViewService) {
        this.viewService = viewService;
    }

    invoke(inputEntity: Actor, args: string) {
        const hasObj = inputEntity.equipment.hasItem(args);
        if (!hasObj) {
            inputEntity.stringToEntity(`You aren't wearing any ${args}.`);
            return noEvent(inputEntity);
        }

        let item = inputEntity.equipment.remove(args);
        if( item === null ) {
            inputEntity.stringToEntity("You cannot remove it.");
            return noEvent(inputEntity);
        }

        inputEntity.addItem(item);
        item.wornBy = null;
        this.viewService.act_new(`$n removes $P.`, inputEntity, null, item, ActFlags.TO_ROOM, Position.POS_RESTING);
        inputEntity.stringToEntity(`You remove ${item.getShortDesc()}.`);

        return {
            type: TriggerType.Remove,
            entity: inputEntity,
            target: item,
            args: [args]
        }
    }
}
