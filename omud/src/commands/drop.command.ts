import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IViewService} from "../services/view.service";
import {ActFlags} from "../domain/act-flags";
import {Position} from "../domain/position";
import {failEvent,  TriggerType} from "../domain/events/events";

export class DropCommand implements IMudCommand {
    private viewService: IViewService;

    constructor(viewService: IViewService) {
        this.viewService = viewService;
    }

    invoke(inputEntity: Actor, args: string) {
        const obj = inputEntity.findItem(args);
        if( !obj ) {
            inputEntity.stringToEntity(`You don't have ${args}.`);
            return failEvent(inputEntity);
        }
        inputEntity.removeItem(obj);
        inputEntity.currentRoom.addItem(obj);
        let exitMsg = `$n drops $P.`;
        this.viewService.act_new( exitMsg, inputEntity, null, obj, ActFlags.TO_ROOM, Position.POS_RESTING );
        inputEntity.stringToEntity(`You drop ${obj.getShortDesc()}`);
        return {
            type: TriggerType.Drop,
            entity: inputEntity,
            target: obj,
            args: [args]
        }
    }
}
