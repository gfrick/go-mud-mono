import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IViewService} from "../services/view.service";
import {ActFlags} from "../domain/act-flags";
import {Position} from "../domain/position";
import {BitFlags} from "../domain/flags";
import {WearFlags} from "../domain/wear-flags";
import {noEvent, TriggerType} from "../domain/events/events";

export class GetCommand implements IMudCommand {

    private viewService: IViewService;

    constructor( viewService: IViewService) {
        this.viewService = viewService;
    }

    invoke(inputEntity: Actor, args: string) {
        const obj = inputEntity.currentRoom.findItem(args);
        if (!obj) {
            inputEntity.stringToEntity(`There is no ${args} here.`);
            return noEvent(inputEntity);
        }
        if (!BitFlags.hasFlag(obj.wearFlags, WearFlags.Take)) {
            inputEntity.stringToEntity("You can't take that.");
            return noEvent(inputEntity);
        }
        inputEntity.currentRoom.removeItem(obj);
        inputEntity.addItem(obj);

        this.viewService.act_new(`$n picks up $P.`, inputEntity, null, obj, ActFlags.TO_ROOM, Position.POS_RESTING);
        inputEntity.stringToEntity(`You pick up ${obj.getShortDesc()}.`);
        return {
            type: TriggerType.Get,
            entity: inputEntity,
            target: obj,
            args: [args]
        }
    }
}
