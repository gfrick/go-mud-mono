import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IViewService} from "../services/view.service";
import {ActFlags} from "../domain/act-flags";
import {Position} from "../domain/position";
import {noEvent, TriggerType} from "../domain/events/events";

export class WearCommand implements IMudCommand {

    private viewService: IViewService;

    constructor( viewService: IViewService) {
        this.viewService = viewService;
    }

    invoke(inputEntity: Actor, args: string) {
        const obj = inputEntity.findItem(args);
        if (!obj) {
            inputEntity.stringToEntity(`You do not have any ${args}.`);
            return noEvent(inputEntity);
        }

        if( !inputEntity.equipment.canEquip(obj) ) {
            inputEntity.stringToEntity("You can't wear that.");
            return noEvent(inputEntity);
        }

        if(! inputEntity.equipment.equip(obj)) {
            // for now, force removal...
            inputEntity.stringToEntity("You are already wearing something there.");
            return noEvent(inputEntity);
        }

        // TODO clean this process up so that it can't partially happen.
        inputEntity.removeItem(obj);
        obj.wornBy = inputEntity;
        this.viewService.act_new(`$n wears $P.`, inputEntity, null, obj, ActFlags.TO_ROOM, Position.POS_RESTING);
        inputEntity.stringToEntity(`You wear ${obj.getShortDesc()}.`);

        return {
            type: TriggerType.Wear,
            entity: inputEntity,
            target: obj,
            args: [args]
        }
    }
}
