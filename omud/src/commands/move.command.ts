import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IViewService} from "../services/view.service";
import {BitFlags} from "../domain/flags";
import {Direction, ExitFlags, ExitService} from "../domain/exit";
import {ActFlags} from "../domain/act-flags";
import {Position} from "../domain/position";
import {IWorldService} from "../core/world";
import {failEvent, noEvent, TriggerType} from "../domain/events/events";

export class MoveCommand implements IMudCommand {

    private worldService: IWorldService;
    private readonly _direction: Direction;
    private viewService: IViewService;

    constructor(worldService: IWorldService, viewService: IViewService, direction: Direction) {
        this.worldService = worldService;
        this._direction = direction;
        this.viewService = viewService;
    }

    invoke(inputEntity: Actor, args: string) {
        if (inputEntity.position < Position.POS_STANDING) {
            if (inputEntity.position === Position.POS_FIGHTING) {
                inputEntity.stringToEntity("But you are still fighting!");
            }
            inputEntity.stringToEntity("You should stand up first.");
            return failEvent(inputEntity);
        }
        const exit = inputEntity.currentRoom.exits.find(exit => exit.direction === this._direction);
        if( !exit ) {
            inputEntity.stringToEntity("There is no exit in that direction.");
            return failEvent(inputEntity);
        }
        if (BitFlags.hasFlag(exit.exitFlags, ExitFlags.EX_ISDOOR)) {
            if (BitFlags.hasFlag(exit.exitFlags, ExitFlags.EX_CLOSED)) {
                inputEntity.stringToEntity(`The ${exit.keyword || 'door'} is closed.`);
                return failEvent(inputEntity);
            }
        }
        if (exit.toVnum) {
            let exitMsg = `$n leaves $T.`;
            this.viewService.act_new(exitMsg, inputEntity, null, ExitService.dirName(exit.direction), ActFlags.TO_ROOM, Position.POS_RESTING);
            this.worldService.moveEntityToVnum(inputEntity, exit.toVnum);
            exitMsg = `$n walks in from ${ExitService.reverseDirName(exit.direction)}.`;
            this.viewService.act_new(exitMsg, inputEntity, null, null, ActFlags.TO_ROOM, Position.POS_RESTING);
            this.worldService.showRoomToEntity(inputEntity);
            return {
                entity: inputEntity,
                args: [this._direction + '', exit.toVnum + ''],
                target: inputEntity.currentRoom,
                type: TriggerType.Enter
            };
        }
        return noEvent(inputEntity);
    }
}
