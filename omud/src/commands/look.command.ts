import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IWorldService} from "../core/world";
import {IViewService} from "../services/view.service";
import {noEvent} from "../domain/events/events";

export class LookCommand implements IMudCommand {
    private worldService: IWorldService;
    private viewService: IViewService;

    constructor(worldService: IWorldService, viewService: IViewService) {
        this.worldService = worldService;
        this.viewService = viewService;
    }

    invoke(inputEntity: Actor, args: string) {
        if (args && args.trim().length > 0) {
            // TODO find an object
            const door = inputEntity.currentRoom.findExitByName(args);
            if (door) {
                this.viewService.showExitToEntity(inputEntity, door);
                return noEvent(inputEntity);
            }
            const entity = inputEntity.findEntityInRoom(args);
            if (entity) {
                this.viewService.showEntityToEntity(inputEntity, entity);
                return noEvent(inputEntity);
            }
            inputEntity.stringToEntity(`Didn't find any ${args}`);
            return noEvent(inputEntity);
        }
        this.worldService.showRoomToEntity(inputEntity);
        return noEvent(inputEntity);
    }
}
