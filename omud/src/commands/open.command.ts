import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IViewService} from "../services/view.service";
import {BitFlags} from "../domain/flags";
import {ExitFlags, ExitService} from "../domain/exit";
import {ActFlags} from "../domain/act-flags";
import {Position} from "../domain/position";
import {IWorldService} from "../core/world";
import {noEvent, TriggerType} from "../domain/events/events";

export class OpenCommand implements IMudCommand {
    private worldService: IWorldService;
    private viewService: IViewService;

    constructor(worldService: IWorldService, viewService: IViewService) {
        this.worldService = worldService;
        this.viewService = viewService;
    }

    invoke(inputEntity: Actor, args: string) {
        if (!args || args.trim().length === 0) {
            inputEntity.stringToEntity(`What do you want to open?`);
            return noEvent(inputEntity);
        }
        // TODO find an object
        const exit = inputEntity.currentRoom.findExitByName(args);
        if (!exit) {
            inputEntity.stringToEntity(`Didn't find any ${args}.`);
            return noEvent(inputEntity);
        }
        if (!BitFlags.hasFlag(exit.exitFlags, ExitFlags.EX_ISDOOR)) {
            inputEntity.stringToEntity(`There is nothing to close.`);
            return noEvent(inputEntity);
        }
        if (!BitFlags.hasFlag(exit.exitFlags, ExitFlags.EX_CLOSED)) {
            inputEntity.stringToEntity(`The ${exit.keyword || 'door'} is already open.`);
            return noEvent(inputEntity);
        }
        exit.exitFlags = BitFlags.removeFlag(exit.exitFlags, ExitFlags.EX_CLOSED);
        inputEntity.stringToEntity(`You open the ${exit.keyword || 'door'}.`);
        let exitMsg = `$n opens the $T.`;
        this.viewService.act_new(exitMsg, inputEntity, null, exit.keyword || 'door', ActFlags.TO_ROOM, Position.POS_RESTING);
        const oppDir = ExitService.reverseDir(exit.direction);
        const oppRoom = this.worldService.findRoom(exit.toVnum);
        if (oppRoom) {
            const oppExit = oppRoom.findExitByDirection(oppDir);
            if (oppExit) {
                oppExit.exitFlags = BitFlags.removeFlag(oppExit.exitFlags, ExitFlags.EX_CLOSED);
                this.viewService.echoToRoom(oppRoom, `The ${exit.keyword || 'door'} opens.`, inputEntity);
            }
        }
        return {
            type: TriggerType.Open,
            entity: inputEntity,
            target: exit,
            args: [args]
        }
    }
}
