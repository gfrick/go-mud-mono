import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {noEvent} from "../domain/events/events";

export class InventoryCommand implements IMudCommand {

    invoke(inputEntity: Actor, args: string) {
        inputEntity.stringToEntity("Your inventory: ");
        if (inputEntity.items && inputEntity.items.length > 0) {
            inputEntity.items.forEach((item ) => {
                inputEntity.stringToEntity(`- ${item.getShortDesc()}`);
            })
            return noEvent(inputEntity);
        }
        inputEntity.stringToEntity("*Empty*");
        return noEvent(inputEntity)
    }
}
