import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IGameEvent, noEvent} from "../domain/events/events";

export class NoneCommand implements IMudCommand {

    constructor() {
    }

    invoke(inputEntity: Actor, command: string): IGameEvent {
        inputEntity.stringToEntity(`Command '${command}' not found.`);
        return noEvent(inputEntity);
    }
}
