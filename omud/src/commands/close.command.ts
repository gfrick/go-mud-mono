import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IViewService} from "../services/view.service";
import {BitFlags} from "../domain/flags";
import {ActFlags} from "../domain/act-flags";
import {Position} from "../domain/position";
import {ExitFlags, ExitService} from "../domain/exit";
import {IWorldService} from "../core/world";
import {failEvent, IGameEvent, noEvent, TriggerType} from "../domain/events/events";

export class CloseCommand implements IMudCommand {
    private worldService: IWorldService;
    private viewService: IViewService;

    constructor(worldService: IWorldService, viewService: IViewService) {
        this.worldService = worldService;
        this.viewService = viewService;
    }

    invoke(inputEntity: Actor, args: string): IGameEvent {
        if (args && args.trim().length > 0) {
            // TODO find an object
            const exit = inputEntity.currentRoom.findExitByName(args);
            if (exit) {
                if (BitFlags.hasFlag(exit.exitFlags, ExitFlags.EX_ISDOOR)) {
                    if (BitFlags.hasFlag(exit.exitFlags, ExitFlags.EX_CLOSED)) {
                        inputEntity.stringToEntity(`The ${exit.keyword || 'door'} is already closed.`);
                        return failEvent(inputEntity);
                    }
                    exit.exitFlags = BitFlags.addFlag(exit.exitFlags, ExitFlags.EX_CLOSED);
                    inputEntity.stringToEntity(`You close the ${exit.keyword || 'door'}.`);
                    let exitMsg = `$n closes the $T.`;
                    this.viewService.act_new(exitMsg, inputEntity, null, exit.keyword || 'door', ActFlags.TO_ROOM, Position.POS_RESTING);
                    const oppDir = ExitService.reverseDir(exit.direction);
                    const oppRoom = this.worldService.findRoom(exit.toVnum);
                    if (oppRoom) {
                        const oppExit = oppRoom.findExitByDirection(oppDir);
                        if (oppExit) {
                            oppExit.exitFlags = BitFlags.addFlag(oppExit.exitFlags, ExitFlags.EX_CLOSED);
                            this.viewService.echoToRoom(oppRoom, `The ${exit.keyword || 'door'} closes.`, inputEntity);
                        }
                    }
                    return {
                        type: TriggerType.Close,
                        entity: inputEntity,
                        target: exit,
                        args: [args]
                    };
                }
                inputEntity.stringToEntity(`There is nothing to close.`);
                return noEvent(inputEntity);
            }
        }
        inputEntity.stringToEntity(`What do you want to close?`);
        return noEvent(inputEntity);
    }
}
