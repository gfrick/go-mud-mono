import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {noEvent} from "../domain/events/events";
import {IWorldService} from "../core/world";

export class EquipmentCommand implements IMudCommand {

    private worldService: IWorldService;

    constructor(worldService: IWorldService) {
        this.worldService = worldService;
    }

    invoke(inputEntity: Actor, args: string) {
        inputEntity.stringToEntity("You are wearing");
        this.worldService.showEquipmentToEntity(inputEntity, inputEntity);
        return noEvent(inputEntity);
    }
}
