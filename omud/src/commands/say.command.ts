import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IViewService} from "../services/view.service";
import {IGameEvent, TriggerType} from "../domain/events/events";

export class SayCommand implements IMudCommand {
    private viewService: IViewService;

    constructor(viewService: IViewService) {
        this.viewService = viewService;
    }
    invoke(inputEntity: Actor, statement: string): IGameEvent {
        this.viewService.echoToRoom(inputEntity.currentRoom, `${inputEntity.getShortName()} says '${statement}'`, inputEntity);
        inputEntity.stringToEntity(`You say '${statement}'`);
        return {
            entity: inputEntity,
            type: TriggerType.None,
            target: inputEntity.currentRoom,
            args: [statement]
        }
    }
}
