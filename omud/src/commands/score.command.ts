import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {noEvent} from "../domain/events/events";
import {IWorldService} from "../core/world";

export class ScoreCommand implements IMudCommand {

    private worldService: IWorldService;

    constructor(worldService: IWorldService) {
        this.worldService = worldService;
    }

    invoke(inputEntity: Actor, args: string) {
        this.worldService.showScoreToEntity(inputEntity);
        return noEvent(inputEntity);
    }
}
