import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IWorldService} from "../core/world";
import { noEvent} from "../domain/events/events";

export class LoadCommand implements IMudCommand {

    private worldService: IWorldService;

    constructor(worldService: IWorldService) {
        this.worldService = worldService;
    }

    invoke(inputEntity: Actor, args: string) {
        const template = this.worldService.findObjectTemplate(parseInt(args,10));
        if( !template ) {
            inputEntity.stringToEntity(`Item with vnum ${args} not found.`);
            return noEvent(inputEntity);
        }
        this.worldService.objectTemplateToContainer(template,inputEntity);
        inputEntity.stringToEntity(`You created a ${template.shortDesc}.`);
        return noEvent(inputEntity);
    }
}
