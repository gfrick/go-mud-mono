import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IWorldService} from "../core/world";
import {noEvent} from "../domain/events/events";

export class GotoCommand implements IMudCommand {

    private worldService: IWorldService;

    constructor(worldService: IWorldService) {
        this.worldService = worldService;
    }

    invoke(inputEntity: Actor, args: string) {
        if (this.worldService.moveEntityToVnum(inputEntity, args.trim())) {
            this.worldService.showRoomToEntity(inputEntity);
        } else {
            inputEntity.stringToEntity(`Room ${args} not found.`);
        }
        return noEvent(inputEntity);
    }
}
