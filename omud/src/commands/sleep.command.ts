import {IMudCommand} from "../core/mud.icommand";
import {Actor} from "../domain/actor";
import {IViewService} from "../services/view.service";
import {Position} from "../domain/position";
import {ActFlags} from "../domain/act-flags";
import {noEvent, TriggerType} from "../domain/events/events";

export class SleepCommand implements IMudCommand {
    private viewService: IViewService;

    constructor(viewService: IViewService) {
        this.viewService = viewService;
    }

    invoke(inputEntity: Actor, statement: string) {
        if (inputEntity.position <= Position.POS_STUNNED) {
            inputEntity.stringToEntity(`You can't do anything right now.`);
            return noEvent(inputEntity);
        }
        if (inputEntity.position === Position.POS_FIGHTING) {
            inputEntity.stringToEntity("Maybe after you finish up this fight.");
            return noEvent(inputEntity);
        }
        if (inputEntity.position === Position.POS_SLEEPING) {
            inputEntity.stringToEntity("You are already sleeping.");
            return noEvent(inputEntity);
        }
        let exitMsg = `$n goes to sleep.`;
        this.viewService.act_new(exitMsg, inputEntity, null, null, ActFlags.TO_ROOM, Position.POS_RESTING);
        inputEntity.stringToEntity(`You go to sleep.`);
        inputEntity.position = Position.POS_SLEEPING;
        return {
            entity: inputEntity,
            type: TriggerType.None,
            target: inputEntity.currentRoom,
            args: [statement]
        }
    }
}
