import express from 'express';
import accountModel from "../repository/account.model";
import {SecurityService} from "../services/security.service";
import {CallbackError} from 'mongoose';
import {IAccount} from "../models/account";

const authController = express.Router();
const securityService = new SecurityService();

authController.get('/logout', function (req, res) {
    res.status(200);
    res.send(true);
})

authController.post('/login', function (req, res) {
    const account = req.body as IAccount;

    if (!account || !account.username || !account.password) {
        res.status(400);
        res.send("missing_username_or_password");
    }
    accountModel.find({
        username: account.username.toLowerCase()
    }, (err: CallbackError, docs: IAccount[]) => {
        if (err) {
            console.log("Error Logging User In: " + err.message);
            res.status(400);
            res.send("");
            return;
        }
        if (docs) {
            if (docs.length === 1) {
                const foundUser = docs[0];
                if (foundUser.password === account.password) {
                    securityService.addSecurityCookies(foundUser, res);
                    res.status(200);
                    res.send(foundUser);
                    return;
                } else {
                    res.status(400);
                    res.send("bad_password");
                    return;
                }
            } else {
                console.log("Found more than one user for login " + account.username);
            }
        }
        res.status(400);
        res.send("no_account");
    });
})

export default authController;
