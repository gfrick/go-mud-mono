import express from 'express';
import { IUserRequest} from "../services/security.service";
import {CallbackError} from "mongoose";
import {MongoError} from "mongodb";
import areaModel from "../repository/area.model";
import {IArea} from "../domain/area";
import Logger from "../core/logger";

const areaController = express.Router();

areaController.get('/', function (req: IUserRequest, res) {
    const user = req.user;
    if (!user) {
        res.status(403);
        res.send("");
    }
    areaModel.find({}, (err: CallbackError, docs: IArea[]) => {
        if (err) {
            Logger.error("Error finding areas for user: " + err.message);
            res.status(500);
            res.send("server_error");
            return;
        }
        if (docs) {
            res.status(200);
            res.send(docs);
            return;
        }
        res.status(200);
        res.send([]);
    });
})

areaController.get('/:areaId', function (req: IUserRequest, res) {
    const user = req.user;
    if (!user) {
        res.status(403);
        res.send("");
    }
    areaModel.find({
        id: req.params.areaId
    }, (err, docs: IArea[]) => {
        if (err) {
            Logger.error("Error finding area " + req.params.areaId + ": " + err.message);
            res.status(500);
            res.send("server_error");
            return;
        }
        if (docs ) {
            res.status(200);
            res.send(docs);
            return;
        }
        res.status(200);
        res.send([]);
    });
})

areaController.post('/', function (req: IUserRequest, res) {

    const user = req.user;
    if (!user) {
        res.status(403);
        res.send("");
        return;
    }

    const newAreaRequest = req.body as IArea;
    Logger.error(newAreaRequest);
    if( !newAreaRequest || !newAreaRequest.header ) {
        res.status(400);
        res.send("missing_name");
        return;
    }
    if( newAreaRequest._id != undefined ) {
        if( newAreaRequest._id === "") {
            newAreaRequest._id = undefined;
        } else {
            res.status(400);
            res.send("use_put_to_update_existing");
            return;
        }
    }

    const newArea = new areaModel(newAreaRequest);
    newArea.save( (error: MongoError, savedArea: IArea) => {
        if( error ) {
            Logger.error("Error saving new area: " + error.message);
            if( error.code === 11000 ) {
                res.status(409);
                res.send("already_exists");
                return;
            }
            res.status(500);
            res.send("error_saving");
            return;
        }
        res.status(200);
        res.send(savedArea);
        Logger.info(savedArea);
        Logger.info("Created area: " + savedArea.header.areaName)
    });
})

areaController.put('/:areaId', async function (req: IUserRequest, res) {

    const user = req.user;
    if (!user) {
        res.status(403);
        res.send("");
        return;
    }

    const foundArea = await areaModel.findById(req.params.areaId);
    if (!foundArea) {
        Logger.error("FATAL: Attempt to update area failed for " + req.params.areaId);
        res.status(400);
        res.send("id_not_matched");
        return;
    }
    const updatedData = req.body as IArea;
    console.log(updatedData);
    if( !updatedData || !updatedData.header ) {
        res.status(400);
        res.send("missing_name");
        return;
    }

    const update = {
        $set: {
            "rooms": updatedData.rooms,
            "resets": updatedData.resets,
            "actors": updatedData.actors,
            "items": updatedData.items,
            "header": updatedData.header,
            "scripts": updatedData.scripts
        }
    };
    await areaModel.updateOne({
        "_id": foundArea.id
    }, update, null, (error: MongoError, result: any) => {
        if( error ) {
            console.log("Error saving new area: " + error.message);
            if( error.code === 11000 ) {
                res.status(409);
                res.send("already_exists");
                return;
            }
            res.status(500);
            res.send("error_saving");
            return;
        }
        res.status(200);
        res.send(result); // TODO, exposes mongo result
    });

})

areaController.delete('/:areaId', async function (req: IUserRequest, res) {

    const user = req.user;
    if (!user) {
        res.status(403);
        res.send("");
        return;
    }

    const foundArea = await areaModel.findById(req.params.areaId);
    if (!foundArea) {
        Logger.error("FATAL: Attempt to delete area failed for " + req.params.areaId);
        res.status(400);
        res.send("missing_name");
        return;
    }

    const deleteId = foundArea.id;
    await areaModel.deleteOne({
        "_id": deleteId
    }, null, (error: MongoError) => {
        if( error ) {
            console.log("Error deleting area: " + error.message + " / " + error.code);
            res.status(500);
            res.send("error_deleting");
            return;
        }
        res.status(200);
        res.send(deleteId); // TODO, exposes mongo result
    });
})

export default areaController;
