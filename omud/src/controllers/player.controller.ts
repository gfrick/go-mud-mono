import express from 'express';
import { IUserRequest} from "../services/security.service";
import {
    INewPlayer,
    IPlayer,
    playerModel
} from "../repository/player.model";
import {CallbackError} from "mongoose";
import {MongoError} from "mongodb";
import Logger from "../core/logger";
import {Actor} from "../domain/actor";

const playerController = express.Router();

playerController.get('/', function (req: IUserRequest, res) {
    const user = req.user;
    if (!user) {
        res.status(403);
        res.send("");
    }
    playerModel.find({
        owner: user.id.toString()
    }, (err: CallbackError, docs: IPlayer[]) => {
        if (err) {
            Logger.error("Error finding players for user: " + err.message);
            res.status(500);
            res.send("server_error");
            return;
        }
        if (docs) {
            res.status(200);
            res.send(docs);
            return;
        }
        res.status(200);
        res.send([]);
    });
})

playerController.get('/:playerId', function (req: IUserRequest, res) {
    const user = req.user;
    if (!user) {
        res.status(403);
        res.send("");
    }
    playerModel.find({
        owner: user.id.toString(),
        id: req.params.playerId
    }, (err, docs: IPlayer[]) => {
        if (err) {
            Logger.error("Error finding player " + req.params.playerId + ": " + err.message);
            res.status(500);
            res.send("server_error");
            return;
        }
        if (docs ) {
            res.status(200);
            res.send(docs);
            return;
        }
        res.status(200);
        res.send([]);
    });
})

playerController.post('/', function (req: IUserRequest, res) {

    const user = req.user;
    if (!user) {
        res.status(403);
        res.send("");
        return;
    }

    const newPlayerRequest = req.body as INewPlayer;
    if( !newPlayerRequest || !newPlayerRequest.name ) {
        res.status(400);
        res.send("missing_name");
        return;
    }

    let actor = new Actor();
    actor.name = newPlayerRequest.name.toLowerCase();
    actor.race = 'human'; // newPlayerRequest.race.toLowerCase();
    actor.shortDesc = actor.name.replace(/^\w/, c => c.toUpperCase());

    const model: Partial<IPlayer> = {
        name: actor.name,
        owner: user.id.toString(),
        state: (actor).getActorDto()
    };
    model.state.lastRoomVnum = 5001;

    const newPlayer = new playerModel(model);
    newPlayer.save( (error: MongoError, savedPlayer: IPlayer) => {
        if( error ) {
            Logger.error("Error saving new player: " + error.message);
            if( error.code === 11000 ) {
                res.status(409);
                res.send("already_exists");
                return;
            }
            res.status(500);
            res.send("error_saving");
            return;
        }
        res.status(200);
        res.send(savedPlayer);
        Logger.info("Created player: " + savedPlayer.name)
    });
})

playerController.delete('/', function (req, res) {
    res.status(200);
    res.send("Unavailable, but 200. lol.");
})

export default playerController;
