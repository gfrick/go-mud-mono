import express from 'express';
import { IUserRequest} from "../services/security.service";
import {CallbackError} from "mongoose";
import {MongoError} from "mongodb";
import accountModel from "../repository/account.model";
import Logger from "../core/logger";
import {IAccount, INewAccount} from "../models/account";

const accountController = express.Router();

accountController.get('/', function (req: IUserRequest, res) {
    const user = req.user;
    if (!user || !user.permissions || !user.permissions.includes("admin")) {
        res.status(403);
        res.send("");
    }
    accountModel.find({ }, (err: CallbackError, docs: IAccount[]) => {
        if (err) {
            Logger.error("Error finding accounts: " + err.message);
            res.status(500);
            res.send("server_error");
            return;
        }
        if (docs) {
            res.status(200);
            res.send(docs);
            return;
        }
        res.status(200);
        res.send([]);
    });
})

accountController.get('/:accountId', function (req: IUserRequest, res) {
    const user = req.user;
    if (!user || !user.permissions || !user.permissions.includes("admin")) {
        res.status(403);
        res.send("");
    }
    accountModel.find({
        id: req.params.accountId
    }, (err, docs: IAccount[]) => {
        if (err) {
            Logger.error("Error finding account " + req.params.accountId + ": " + err.message);
            res.status(500);
            res.send("server_error");
            return;
        }
        if (docs ) {
            res.status(200);
            res.send(docs);
            return;
        }
        res.status(200);
        res.send([]);
    });
})

accountController.post('/', function (req: IUserRequest, res) {

    const user = req.user;
    if (!user) {
        res.status(403);
        res.send("");
        return;
    }

    const newAccountRequest = req.body as INewAccount;
    if( !newAccountRequest || !newAccountRequest.username || !newAccountRequest.password ) {
        res.status(400);
        res.send("missing_user_pass");
        return;
    }

    const model: Partial<IAccount> = {
        username: newAccountRequest.username.toLowerCase(),
        password: newAccountRequest.password,
        permissions: ["play"]
    };

    const newAccount = new accountModel(model);
    newAccount.save( (error: MongoError, savedPlayer: IAccount) => {
        if( error ) {
            Logger.error("Error saving new account: " + error.message);
            if( error.code === 11000 ) {
                res.status(409);
                res.send("already_exists");
                return;
            }
            res.status(500);
            res.send("error_saving");
            return;
        }
        res.status(200);
        res.send(savedPlayer);
        Logger.info("Created account: " + savedPlayer.username)
    });
})

accountController.delete('/', function (req, res) {
    res.status(200);
    res.send("Unavailable.");
});



export default accountController;
