import express from 'express';
import authController from "./auth.controller";
import { SecurityService} from "../services/security.service";
import playerController from "./player.controller";
import areaController from "./area.controller";
import accountController from "./account.controller";

const router = express.Router();
const securityService = new SecurityService();

router.use('/auth', authController );
router.use('/players', securityService.checkTokenMiddleware, playerController );
router.use('/areas', securityService.checkTokenMiddleware, areaController );
router.use('/accounts', securityService.checkTokenMiddleware, accountController );

export default router;
