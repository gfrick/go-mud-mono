import express, { Application} from 'express';
import http, {IncomingMessage} from 'http';
import WebSocket from 'ws';
import ConnectionHost from "./core/connection-host";
import appRouter from './controllers/api';
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import {SecurityService} from "./services/security.service";
import {IPlayer, getPlayerForAccount} from "./repository/player.model";
import Logger from "./core/logger";
import {getMongoUri, initMongo} from "./repository/startup";
import {IAccount} from "./models/account";
import {Mud} from "./core/mud";

const application: Application = express();
application.use(bodyParser.urlencoded({extended: true}));
application.use(bodyParser.json());
application.use(cookieParser());
application.use('/', appRouter);

const MONGO_URI = getMongoUri(process);

initMongo(MONGO_URI,
    () => {
    Logger.info("Connected to Mongo, starting World.");
    startWorld();
    },
    (msg: any) => {
    Logger.error(msg);
    Logger.error("Could not connect to mongo, game will not start.");
});

function startWorld() {
    const mud = new Mud();
    const connectionHost = new ConnectionHost(mud);
    mud.init().then((success: boolean) => {
        if( success ) {
            Logger.info("World Ready, starting Server.");
            startServer(connectionHost);
        } else {
            throw "Could not start world, server will not start.";
        }
    });
}

function startServer(connectionHost: ConnectionHost, securityService = new SecurityService()) {
    const PORT = process.env.PORT || 8080;
    const server = http.createServer(express);
    const wss = new WebSocket.Server({noServer: true})

    server.on('request', application);
    server.on('upgrade', function upgrade(request, socket, head) {

        const account = securityService.getAccountForWebSocket(request);
        if (account.id === "") {
            socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
            socket.destroy();
            return;
        }

        if (request.url.indexOf("=") < 0 || request.url.indexOf("/play?key=") < 0) {
            socket.write('HTTP/1.1 400 Bad Request\r\n\r\n');
            socket.destroy();
            return;
        }

        const username = request.url.split("=")[1];
        getPlayerForAccount(username, account).then( (player) => {
            if (!player) {
                socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
                socket.destroy();
                return;
            }
            wss.handleUpgrade(request, socket, head, function done(ws) {
                wss.emit('connection', ws, request, player, account);
            });
        });
    });

    wss.on('connection', function connection(ws: WebSocket, request: IncomingMessage, player: IPlayer, account: IAccount) {
        Logger.info(`Received player ${player.name}.`);
        Logger.info(`With account ${account.username}.`);
        connectionHost.handleConnection(ws, wss, player, account);
    })

    setInterval(() => {
        connectionHost.hostTick();
    }, 500);

    server.listen(PORT, (): void => {
        Logger.info(`Server is listening on ${PORT}!`)
    });
}
