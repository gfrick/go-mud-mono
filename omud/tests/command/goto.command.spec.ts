import 'jest';
import {Actor} from "../../src/domain/actor";
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultActor} from "../../src/mocks/actor.default";
import {worldService} from "../../src/mocks/world.service.mock";
import {GotoCommand} from "../../src/commands/goto.command";

describe('Command: Goto', () => {

    const mockWorldService = worldService();
    const command = new GotoCommand(mockWorldService);

    it('should move the character between rooms.', () => {
        const room = defaultRoom(999);
        const room2 = defaultRoom(998);
        const actor = defaultActor(999);
        room.addEntity(actor);

        mockWorldService.moveEntityToVnum = (entity: Actor, vnum: string | number): boolean => {
            if (entity === actor && vnum === '998') {
                if (entity.currentRoom) {
                    entity.currentRoom.removeEntity(entity);
                }
                room2.addEntity(entity);
                return true;
            }
            return false;
        }
        const event = command.invoke(actor, "998");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room2);
    });


});

