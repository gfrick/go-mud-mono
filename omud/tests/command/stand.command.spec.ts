import 'jest';
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultActor} from "../../src/mocks/actor.default";
import {viewService} from "../../src/mocks/view.service.mock";
import {StandCommand} from "../../src/commands/stand.command";
import {Position} from "../../src/domain/position";

describe('Command: Stand', () => {
    const mock = viewService();
    const command = new StandCommand(mock);

    it('should change position.', () => {
        const room = defaultRoom(999);
        const actor = defaultActor(999);
        actor.position = Position.POS_SLEEPING;
        room.addEntity(actor);
        expect(actor.getPosition()).toEqual(Position.POS_SLEEPING);

        const event = command.invoke(actor, "stand");
        expect(event).toBeDefined();
        expect(actor.getPosition()).toEqual(Position.POS_STANDING);
    });
});

