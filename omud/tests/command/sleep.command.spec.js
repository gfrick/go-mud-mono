"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const actor_1 = require("../../src/domain/actor");
const room_1 = require("../../src/domain/room");
const view_service_mock_1 = require("../../src/mocks/view.service.mock");
const position_1 = require("../../src/domain/position");
const sleep_command_1 = require("../../src/commands/sleep.command");
describe('Command: Sleep', () => {
    const mock = view_service_mock_1.viewService();
    const command = new sleep_command_1.SleepCommand(mock);
    it('should change position.', () => {
        const room = room_1.defaultRoom(999);
        const actor = actor_1.defaultActor(999);
        actor.state.position = position_1.Position.POS_STANDING;
        room.addEntity(actor);
        expect(actor.getPosition()).toEqual(position_1.Position.POS_STANDING);
        const event = command.invoke(actor, "stand");
        expect(event).toBeDefined();
        expect(actor.getPosition()).toEqual(position_1.Position.POS_SLEEPING);
    });
});
//# sourceMappingURL=sleep.command.spec.js.map