"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const say_command_1 = require("../../src/commands/say.command");
const actor_1 = require("../../src/domain/actor");
const room_1 = require("../../src/domain/room");
const view_service_mock_1 = require("../../src/mocks/view.service.mock");
describe('Command: Say', () => {
    const mock = view_service_mock_1.viewService();
    const command = new say_command_1.SayCommand(mock);
    it('should say the text.', () => {
        const room = room_1.defaultRoom(999);
        const actor = actor_1.defaultActor(999);
        const event = command.invoke(actor, "hello");
        expect(event).toBeDefined();
    });
});
//# sourceMappingURL=say.command.spec.js.map