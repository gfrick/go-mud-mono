"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const item_1 = require("../../src/domain/item");
const actor_1 = require("../../src/domain/actor");
const room_1 = require("../../src/domain/room");
const get_command_1 = require("../../src/commands/get.command");
const events_1 = require("../../src/domain/events/events");
const flags_1 = require("../../src/domain/flags");
const wear_flags_1 = require("../../src/domain/wear-flags");
const view_service_mock_1 = require("../../src/mocks/view.service.mock");
describe('Command: Get', () => {
    const mock = view_service_mock_1.viewService();
    const get = new get_command_1.GetCommand(mock);
    it('should return the correct event.', () => {
        const room = room_1.defaultRoom(999);
        const item = item_1.defaultItem(999);
        const actor = actor_1.defaultActor(999);
        item.wearFlags = flags_1.BitFlags.addFlag(item.wearFlags, wear_flags_1.WearFlags.Take); // can pick it up.
        room.addItem(item);
        room.addEntity(actor);
        const event = get.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.Get);
    });
    it('should handle missing item.', () => {
        const room = room_1.defaultRoom(999);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        let output = [];
        actor.controller = {
            getPlayerId() {
                return "tester";
            },
            handlePlayerInput() {
                return "";
            },
            queuePlayerOutput(data) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        };
        const event = get.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.None);
        expect(output.length).toEqual(1);
        expect(output.shift()).toEqual(`There is no tab here.`);
    });
    it('Item must have take flag, be visible, etc.', () => {
        const room = room_1.defaultRoom(999);
        const actor = actor_1.defaultActor(999);
        const item = item_1.defaultItem(999);
        room.addEntity(actor);
        room.addItem(item);
        let output = [];
        actor.controller = {
            getPlayerId() {
                return "tester";
            },
            handlePlayerInput() {
                return "";
            },
            queuePlayerOutput(data) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        };
        const event = get.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.None);
        expect(output.length).toEqual(1);
        expect(output.shift()).toEqual(`You can't take that.`);
    });
});
//# sourceMappingURL=get.command.spec.js.map