"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const item_1 = require("../../src/domain/item");
const actor_1 = require("../../src/domain/actor");
const room_1 = require("../../src/domain/room");
const events_1 = require("../../src/domain/events/events");
const flags_1 = require("../../src/domain/flags");
const wear_flags_1 = require("../../src/domain/wear-flags");
const view_service_mock_1 = require("../../src/mocks/view.service.mock");
const drop_command_1 = require("../../src/commands/drop.command");
describe('Command: Drop', () => {
    const mock = view_service_mock_1.viewService();
    const dropCommand = new drop_command_1.DropCommand(mock);
    it('should return the correct event.', () => {
        const room = room_1.defaultRoom(999);
        const item = item_1.defaultItem(999);
        const actor = actor_1.defaultActor(999);
        item.wearFlags = flags_1.BitFlags.addFlag(item.wearFlags, wear_flags_1.WearFlags.Take); // can pick it up.
        actor.addItem(item);
        room.addEntity(actor);
        const event = dropCommand.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.Drop);
    });
    it('should handle missing item.', () => {
        const room = room_1.defaultRoom(999);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        let output = [];
        actor.controller = {
            getPlayerId() {
                return "tester";
            },
            handlePlayerInput() {
                return "";
            },
            queuePlayerOutput(data) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        };
        const event = dropCommand.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.Fail);
        expect(output.length).toEqual(1);
        expect(output.shift()).toEqual(`You don't have tab.`);
    });
});
//# sourceMappingURL=drop.command.spec.js.map