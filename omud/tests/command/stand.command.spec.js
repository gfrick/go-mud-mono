"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const actor_1 = require("../../src/domain/actor");
const room_1 = require("../../src/domain/room");
const view_service_mock_1 = require("../../src/mocks/view.service.mock");
const stand_command_1 = require("../../src/commands/stand.command");
const position_1 = require("../../src/domain/position");
describe('Command: Stand', () => {
    const mock = view_service_mock_1.viewService();
    const command = new stand_command_1.StandCommand(mock);
    it('should change position.', () => {
        const room = room_1.defaultRoom(999);
        const actor = actor_1.defaultActor(999);
        actor.state.position = position_1.Position.POS_SLEEPING;
        room.addEntity(actor);
        expect(actor.getPosition()).toEqual(position_1.Position.POS_SLEEPING);
        const event = command.invoke(actor, "stand");
        expect(event).toBeDefined();
        expect(actor.getPosition()).toEqual(position_1.Position.POS_STANDING);
    });
});
//# sourceMappingURL=stand.command.spec.js.map