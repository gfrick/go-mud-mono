import 'jest';
import {SayCommand} from "../../src/commands/say.command";
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultActor} from "../../src/mocks/actor.default";
import {viewService} from "../../src/mocks/view.service.mock";

describe('Command: Say', () => {
    const mock = viewService();
    const command = new SayCommand(mock);
    it('should say the text.', () => {
        const room = defaultRoom(999);
        const actor = defaultActor(999);
       const event = command.invoke(actor, "hello");
       expect(event).toBeDefined();
    });
});

