"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const actor_1 = require("../../src/domain/actor");
const room_1 = require("../../src/domain/room");
const events_1 = require("../../src/domain/events/events");
const view_service_mock_1 = require("../../src/mocks/view.service.mock");
const close_command_1 = require("../../src/commands/close.command");
const world_service_mock_1 = require("../../src/mocks/world.service.mock");
const exit_1 = require("../../src/domain/exit");
describe('Command: Close', () => {
    const mockViewService = view_service_mock_1.viewService();
    const mockWorldService = world_service_mock_1.worldService();
    const closeCommand = new close_command_1.CloseCommand(mockWorldService, mockViewService);
    it('should return the correct event.', () => {
        const room = room_1.defaultRoom(999);
        room.exits.push({
            toVnum: 0,
            direction: exit_1.Direction.NORTH,
            keyword: "",
            defaultExitFlags: exit_1.ExitFlags.EX_ISDOOR | exit_1.ExitFlags.EX_CLOSED,
            exitFlags: exit_1.ExitFlags.EX_ISDOOR,
            keyVnum: 0,
            description: ""
        });
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        const event = closeCommand.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.Close);
    });
    it('should handle missing door.', () => {
        const room = room_1.defaultRoom(999);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        let output = [];
        actor.controller = {
            getPlayerId() {
                return "tester";
            },
            handlePlayerInput() {
                return "";
            },
            queuePlayerOutput(data) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        };
        const event = closeCommand.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.None);
        expect(output.length).toEqual(1);
        expect(output.shift()).toEqual(`What do you want to close?`);
    });
});
//# sourceMappingURL=close.command.spec.js.map