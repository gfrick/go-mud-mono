import 'jest';
import { IItemTemplate} from "../../src/domain/item";
import {Actor, IActorTemplate} from "../../src/domain/actor";
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultActor} from "../../src/mocks/actor.default";
import {defaultItem} from "../../src/mocks/item.defaut";
import {IGameScript, TriggerType} from "../../src/domain/events/events";
import {LoadCommand} from "../../src/commands/load.command";
import {IWorldService} from "../../src/core/world";
import {IItemContainer} from "../../src/domain/container";
import {Room} from "../../src/domain/room";

const worldService = jest.fn<IWorldService, []>(() => {
    return {
        objectTemplateToContainer(objTemplate: IItemTemplate, entity: IItemContainer) {
            const item = defaultItem(999, objTemplate);
            entity.addItem(item);
        },
        findRoom(vnum: string | number): Room | null {
            return null;
        },
        findObjectTemplate(vnum: number): IItemTemplate | null {
            return {
                condition: 100,
                cost: 1,
                description: "can tab",
                level: 1,
                longDesc: "A can tab lies here being defaulty.",
                material: "aluminum",
                name: "can tab",
                properties: {},
                shortDesc: "a can tab",
                tags: [],
                triggers: [],
                type: 0,
                vnum: vnum,
                wearFlags: 0,
                weight: 1
            };
        },
        countEntities(vnum: number): number {
            return 0;
        },
        findEntityTemplate(vnum: number): IActorTemplate | null {
            return null;
        },
        entityTemplateToWorld(mobTemplate: IActorTemplate, room: Room) {
        },
        getTriggerScript(script: number): IGameScript | null {
            return {
                name: "testScript",
                vnum: 999,
                script: "(function cool(event) { event.entity.stringToEntity(`test output`); })"
            };
        },
        showEquipmentToEntity(inputEntity: Actor, lookEntity: Actor) {
        },
        moveEntityToVnum(entity: Actor, vnum: string | number): boolean {
            return false;
        },
        showRoomToEntity(inputEntity: Actor, currentRoom?: Room) {
        },
        showScoreToEntity(inputEntity: Actor) {}
    };
});

describe('Command: Load', () => {

    const mock = worldService();
    const get = new LoadCommand(mock);

    it('should return the correct event.', () => {
        const room = defaultRoom(999);
        const actor = defaultActor(999);
        room.addEntity(actor);
        const event = get.invoke(actor, "999");
        expect(event).toBeDefined();
        expect(event.type).toEqual(TriggerType.None);
    });


});

