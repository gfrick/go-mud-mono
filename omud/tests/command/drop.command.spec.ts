import 'jest';
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultActor} from "../../src/mocks/actor.default";
import {defaultItem} from "../../src/mocks/item.defaut";
import {TriggerType} from "../../src/domain/events/events";
import {BitFlags} from "../../src/domain/flags";
import {WearFlags} from "../../src/domain/wear-flags";
import {viewService} from "../../src/mocks/view.service.mock";
import {DropCommand} from "../../src/commands/drop.command";
import WebSocket = require("ws");

describe('Command: Drop', () => {

    const mock = viewService();
    const dropCommand = new DropCommand(mock);

    it('should return the correct event.', () => {
        const room = defaultRoom(999);
        const item = defaultItem(999);
        const actor = defaultActor(999);
        item.wearFlags = BitFlags.addFlag(item.wearFlags, WearFlags.Take); // can pick it up.
        actor.addItem(item);
        room.addEntity(actor);
        const event = dropCommand.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(TriggerType.Drop);
    });

    it('should handle missing item.', () => {
        const room = defaultRoom(999);
        const actor = defaultActor(999);
        room.addEntity(actor);

        let output: string[] = [];
        actor.controller = {
            handleHostInput(): WebSocket.Data {
                return undefined;
            }, handleHostOutput(): any {
            }, isInputReady(): boolean {
                return false;
            }, isOutputReady(): boolean {
                return false;
            }, queueHostInput(data: WebSocket.Data): void {
            }, queueHostOutput(data: any): void {
            }, queuePlayerInput(data: WebSocket.Data) {
            }, sendHostOutput(data: any): void {
            },
            getPlayerId(): string {
                return "tester";
            },
            handlePlayerInput(): string {
                return "";
            },
            queuePlayerOutput(data: any) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        }
        const event = dropCommand.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(TriggerType.Fail);
        expect(output.length).toEqual(1);
        expect(output.shift()).toEqual(`You don't have tab.`);
    });


});

