import 'jest';
import {viewService} from "../../src/mocks/view.service.mock";
import {Position} from "../../src/domain/position";
import {SitCommand} from "../../src/commands/sit.command";
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultActor} from "../../src/mocks/actor.default";

describe('Command: Sit', () => {
    const mock = viewService();
    const command = new SitCommand(mock);

    it('should change position.', () => {
        const room = defaultRoom(999);
        const actor = defaultActor(999);
        actor.position = Position.POS_SLEEPING;
        room.addEntity(actor);
        expect(actor.getPosition()).toEqual(Position.POS_SLEEPING);

        const event = command.invoke(actor, "sit");
        expect(event).toBeDefined();
        expect(actor.getPosition()).toEqual(Position.POS_SITTING);
    });
});

