"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const item_1 = require("../../src/domain/item");
const actor_1 = require("../../src/domain/actor");
const room_1 = require("../../src/domain/room");
const events_1 = require("../../src/domain/events/events");
const load_command_1 = require("../../src/commands/load.command");
const worldService = jest.fn(() => {
    return {
        objectTemplateToContainer(objTemplate, entity) {
            const item = item_1.defaultItem(999, objTemplate);
            entity.addItem(item);
        },
        findRoom(vnum) {
            return null;
        },
        findObjectTemplate(vnum) {
            return {
                condition: 100,
                cost: 1,
                description: "can tab",
                level: 1,
                longDesc: "A can tab lies here being defaulty.",
                material: "aluminum",
                name: "can tab",
                properties: {},
                shortDesc: "a can tab",
                tags: [],
                triggers: [],
                type: 0,
                vnum: vnum,
                wearFlags: 0,
                weight: 1
            };
        },
        countEntities(vnum) {
            return 0;
        },
        findEntityTemplate(vnum) {
            return null;
        },
        entityTemplateToWorld(mobTemplate, room) {
        },
        getTriggerScript(script) {
            return {
                name: "testScript",
                vnum: 999,
                script: "(function cool(event) { event.entity.stringToEntity(`test output`); })"
            };
        },
        moveEntityToVnum(entity, vnum) {
            return false;
        },
        showRoomToEntity(inputEntity, currentRoom) {
        },
    };
});
describe('Command: Load', () => {
    const mock = worldService();
    const get = new load_command_1.LoadCommand(mock);
    it('should return the correct event.', () => {
        const room = room_1.defaultRoom(999);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        const event = get.invoke(actor, "999");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.None);
    });
});
//# sourceMappingURL=load.command.spec.js.map