"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const exit_1 = require("../../src/domain/exit");
const actor_1 = require("../../src/domain/actor");
const room_1 = require("../../src/domain/room");
const move_command_1 = require("../../src/commands/move.command");
const view_service_mock_1 = require("../../src/mocks/view.service.mock");
const world_service_mock_1 = require("../../src/mocks/world.service.mock");
const position_1 = require("../../src/domain/position");
describe('Command: Move', () => {
    const mockViewService = view_service_mock_1.viewService();
    const mockWorldService = world_service_mock_1.worldService();
    const command = new move_command_1.MoveCommand(mockWorldService, mockViewService, exit_1.Direction.NORTH);
    it('should move the character between rooms and inform actors.', () => {
        const room = room_1.defaultRoom(999);
        const room2 = room_1.defaultRoom(998);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        room.exits.push({
            toVnum: 998,
            direction: exit_1.Direction.NORTH,
            keyword: "",
            defaultExitFlags: 0,
            exitFlags: 0,
            keyVnum: 0,
            description: ""
        });
        mockWorldService.moveEntityToVnum = (entity, vnum) => {
            if (entity === actor && vnum === 998) {
                if (entity.currentRoom) {
                    entity.currentRoom.removeEntity(entity);
                }
                room2.addEntity(entity);
                return true;
            }
            return false;
        };
        const event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room2);
    });
    it('should require a valid position', () => {
        const room = room_1.defaultRoom(999);
        const room2 = room_1.defaultRoom(998);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        room.exits.push({
            toVnum: 998,
            direction: exit_1.Direction.NORTH,
            keyword: "",
            defaultExitFlags: 0,
            exitFlags: 0,
            keyVnum: 0,
            description: ""
        });
        actor.state.position = position_1.Position.POS_SLEEPING;
        let event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room);
        actor.state.position = position_1.Position.POS_FIGHTING;
        event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room);
    });
    it('should require an exit', () => {
        const room = room_1.defaultRoom(999);
        const room2 = room_1.defaultRoom(998);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        room.exits.push({
            toVnum: 998,
            direction: exit_1.Direction.SOUTH,
            keyword: "",
            defaultExitFlags: 0,
            exitFlags: 0,
            keyVnum: 0,
            description: ""
        });
        const event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room);
    });
    it('should require a non-zero target vnum', () => {
        const room = room_1.defaultRoom(999);
        const room2 = room_1.defaultRoom(998);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        room.exits.push({
            toVnum: 0,
            direction: exit_1.Direction.NORTH,
            keyword: "",
            defaultExitFlags: 0,
            exitFlags: 0,
            keyVnum: 0,
            description: ""
        });
        const event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room);
    });
    it('should respect closed doors', () => {
        const room = room_1.defaultRoom(999);
        const room2 = room_1.defaultRoom(998);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        room.exits.push({
            toVnum: 0,
            direction: exit_1.Direction.NORTH,
            keyword: "",
            defaultExitFlags: exit_1.ExitFlags.EX_ISDOOR | exit_1.ExitFlags.EX_CLOSED,
            exitFlags: exit_1.ExitFlags.EX_ISDOOR | exit_1.ExitFlags.EX_CLOSED,
            keyVnum: 0,
            description: ""
        });
        const event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room);
    });
});
//# sourceMappingURL=move.command.spec.js.map