import 'jest';
import {Direction, ExitFlags} from "../../src/domain/exit";
import {Actor} from "../../src/domain/actor";
import {MoveCommand} from "../../src/commands/move.command";
import {viewService} from "../../src/mocks/view.service.mock";
import {worldService} from "../../src/mocks/world.service.mock";
import {Position} from "../../src/domain/position";
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultActor} from "../../src/mocks/actor.default";

describe('Command: Move', () => {
    const mockViewService = viewService();
    const mockWorldService = worldService();
    const command = new MoveCommand(mockWorldService, mockViewService, Direction.NORTH);

    it('should move the character between rooms and inform actors.', () => {
        const room = defaultRoom(999);
        const room2 = defaultRoom(998);
        const actor = defaultActor(999);
        room.addEntity(actor);
        room.exits.push({
            toVnum: 998,
            direction: Direction.NORTH,
            keyword: "",
            defaultExitFlags: 0,
            exitFlags: 0,
            keyVnum: 0,
            description: ""
        });
        mockWorldService.moveEntityToVnum = (entity: Actor, vnum: string | number): boolean => {
            if (entity === actor && vnum === 998) {
                if (entity.currentRoom) {
                    entity.currentRoom.removeEntity(entity);
                }
                room2.addEntity(entity);
                return true;
            }
            return false;
        }
        const event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room2);
    });

    it('should require a valid position', () => {
        const room = defaultRoom(999);
        const room2 = defaultRoom(998);
        const actor = defaultActor(999);
        room.addEntity(actor);
        room.exits.push({
            toVnum: 998,
            direction: Direction.NORTH,
            keyword: "",
            defaultExitFlags: 0,
            exitFlags: 0,
            keyVnum: 0,
            description: ""
        });
        actor.position = Position.POS_SLEEPING;
        let event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room);
        actor.position = Position.POS_FIGHTING;
        event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room);
    });

    it('should require an exit', () => {
        const room = defaultRoom(999);
        const room2 = defaultRoom(998);
        const actor = defaultActor(999);
        room.addEntity(actor);
        room.exits.push({
            toVnum: 998,
            direction: Direction.SOUTH,
            keyword: "",
            defaultExitFlags: 0,
            exitFlags: 0,
            keyVnum: 0,
            description: ""
        });
        const event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room);
    });

    it('should require a non-zero target vnum', () => {
        const room = defaultRoom(999);
        const room2 = defaultRoom(998);
        const actor = defaultActor(999);
        room.addEntity(actor);
        room.exits.push({
            toVnum: 0,
            direction: Direction.NORTH,
            keyword: "",
            defaultExitFlags: 0,
            exitFlags: 0,
            keyVnum: 0,
            description: ""
        });
        const event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room);
    });

    it('should respect closed doors', () => {
        const room = defaultRoom(999);
        const room2 = defaultRoom(998);
        const actor = defaultActor(999);
        room.addEntity(actor);
        room.exits.push({
            toVnum: 0,
            direction: Direction.NORTH,
            keyword: "",
            defaultExitFlags: ExitFlags.EX_ISDOOR | ExitFlags.EX_CLOSED,
            exitFlags: ExitFlags.EX_ISDOOR | ExitFlags.EX_CLOSED,
            keyVnum: 0,
            description: ""
        });
        const event = command.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room);
    });
});

