import 'jest';
import {TriggerType} from "../../src/domain/events/events";
import {ScoreCommand} from "../../src/commands/score.command";
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultActor} from "../../src/mocks/actor.default";
import {worldService} from "../../src/mocks/world.service.mock";

describe('Command: Score', () => {

    const mockWorldService = worldService();
    const score = new ScoreCommand(mockWorldService);

    it('should return the correct event.', () => {
        const room = defaultRoom(999);
        const actor = defaultActor(999);
        room.addEntity(actor);
        const event = score.invoke(actor, "999");
        expect(event).toBeDefined();
        expect(event.type).toEqual(TriggerType.None);
    });
    
});

