import 'jest';
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultActor} from "../../src/mocks/actor.default";
import {viewService} from "../../src/mocks/view.service.mock";
import {Position} from "../../src/domain/position";
import {SleepCommand} from "../../src/commands/sleep.command";

describe('Command: Sleep', () => {
    const mock = viewService();
    const command = new SleepCommand(mock);

    it('should change position.', () => {
        const room = defaultRoom(999);
        const actor = defaultActor(999);
        actor.position = Position.POS_STANDING;
        room.addEntity(actor);
        expect(actor.getPosition()).toEqual(Position.POS_STANDING);

        const event = command.invoke(actor, "stand");
        expect(event).toBeDefined();
        expect(actor.getPosition()).toEqual(Position.POS_SLEEPING);
    });
});

