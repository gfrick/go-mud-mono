"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const actor_1 = require("../../src/domain/actor");
const room_1 = require("../../src/domain/room");
const world_service_mock_1 = require("../../src/mocks/world.service.mock");
const goto_command_1 = require("../../src/commands/goto.command");
describe('Command: Goto', () => {
    const mockWorldService = world_service_mock_1.worldService();
    const command = new goto_command_1.GotoCommand(mockWorldService);
    it('should move the character between rooms.', () => {
        const room = room_1.defaultRoom(999);
        const room2 = room_1.defaultRoom(998);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        mockWorldService.moveEntityToVnum = (entity, vnum) => {
            if (entity === actor && vnum === '998') {
                if (entity.currentRoom) {
                    entity.currentRoom.removeEntity(entity);
                }
                room2.addEntity(entity);
                return true;
            }
            return false;
        };
        const event = command.invoke(actor, "998");
        expect(event).toBeDefined();
        expect(actor.currentRoom).toEqual(room2);
    });
});
//# sourceMappingURL=goto.command.spec.js.map