"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const actor_1 = require("../../src/domain/actor");
const room_1 = require("../../src/domain/room");
const view_service_mock_1 = require("../../src/mocks/view.service.mock");
const position_1 = require("../../src/domain/position");
const sit_command_1 = require("../../src/commands/sit.command");
describe('Command: Sit', () => {
    const mock = view_service_mock_1.viewService();
    const command = new sit_command_1.SitCommand(mock);
    it('should change position.', () => {
        const room = room_1.defaultRoom(999);
        const actor = actor_1.defaultActor(999);
        actor.state.position = position_1.Position.POS_SLEEPING;
        room.addEntity(actor);
        expect(actor.getPosition()).toEqual(position_1.Position.POS_SLEEPING);
        const event = command.invoke(actor, "sit");
        expect(event).toBeDefined();
        expect(actor.getPosition()).toEqual(position_1.Position.POS_SITTING);
    });
});
//# sourceMappingURL=sit.command.spec.js.map