import 'jest';
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultActor} from "../../src/mocks/actor.default";
import {TriggerType} from "../../src/domain/events/events";
import {viewService} from "../../src/mocks/view.service.mock";
import {worldService} from "../../src/mocks/world.service.mock";
import {Direction, ExitFlags} from "../../src/domain/exit";
import {OpenCommand} from "../../src/commands/open.command";
import WebSocket = require("ws");

describe('Command: Open', () => {

    const mockViewService = viewService();
    const mockWorldService = worldService();
    const openCommand = new OpenCommand(mockWorldService,mockViewService);

    it('should return the correct event.', () => {
        const room = defaultRoom(999);
        room.exits.push({
            toVnum: 0,
            direction: Direction.NORTH,
            keyword: "",
            defaultExitFlags: ExitFlags.EX_ISDOOR | ExitFlags.EX_CLOSED,
            exitFlags: ExitFlags.EX_ISDOOR | ExitFlags.EX_CLOSED,
            keyVnum: 0,
            description: ""
        });
        const actor = defaultActor(999);
        room.addEntity(actor);
        const event = openCommand.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(event.type).toEqual(TriggerType.Open);
    });

    it('should handle missing door.', () => {
        const room = defaultRoom(999);
        const actor = defaultActor(999);
        room.addEntity(actor);

        let output: string[] = [];
        actor.controller = {
            handleHostInput(): WebSocket.Data {
                return undefined;
            }, handleHostOutput(): any {
            }, isInputReady(): boolean {
                return false;
            }, isOutputReady(): boolean {
                return false;
            }, queueHostInput(data: WebSocket.Data): void {
            }, queueHostOutput(data: any): void {
            }, queuePlayerInput(data: WebSocket.Data) {
            }, sendHostOutput(data: any): void {
            },
            getPlayerId(): string {
                return "tester";
            },
            handlePlayerInput(): string {
                return "";
            },
            queuePlayerOutput(data: any) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        }
        const event = openCommand.invoke(actor, "north");
        expect(event).toBeDefined();
        expect(event.type).toEqual(TriggerType.None);
        expect(output.length).toEqual(1);
        expect(output.shift()).toEqual(`Didn't find any north.`);
    });

});

