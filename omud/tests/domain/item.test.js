"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const item_1 = require("../../src/domain/item");
const itemTemplate = {
    "condition": 100,
    "cost": 1,
    "level": 1,
    "longDesc": "A small flagstone well with thatch roof.",
    "material": "stone",
    "shortDesc": "flagstone well",
    "wearFlags": 0,
    "weight": 100,
    "type": 13,
    "vnum": 5001,
    "name": "flagstone well",
    "description": "",
    "triggers": [],
    "tags": [],
    "properties": {}
};
describe('Item', () => {
    it('should create an item', () => {
        const item = new item_1.Item(itemTemplate);
        expect(item).toBeTruthy();
    });
});
//# sourceMappingURL=item.test.js.map