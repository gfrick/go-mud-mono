
import 'jest';
import {Actor, IActorTemplate} from "../../src/domain/actor";

const actorTemplate: IActorTemplate =      {
	"alignment": 0,
	"damageType": "punch",
	"defaultPosition": 8,
	"hitRoll": 1,
	"level": 1,
	"longDesc": "A priestess stands here in a plain cloak.",
	"race": "human",
	"sex": 2,
	"shortDesc": "Priestess Liliana",
	"size": 2,
	"startPosition": 8,
	"wealth": 10,
	"vnum": 5001,
	"name": "priestess liliana diris",
	"description": "She has long, curled, brown hair shaved on the left side and brown eyes.\nShe has rough, sunburned, gray skin.\nShe stands 104cm (3'4\") tall and has a regular build.\nShe has an oval, very common face.",
	"triggers": [],
	"tags": [],
	"properties": {}
};

describe('Actor', () => {
	it('should create an actor', () => {
		const actor = new Actor(actorTemplate);
		expect(actor).toBeTruthy();
	});
});

