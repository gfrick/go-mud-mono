"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const room_1 = require("../../src/domain/room");
const events_1 = require("../../src/domain/events/events");
const item_1 = require("../../src/domain/item");
const actor_1 = require("../../src/domain/actor");
const roomTemplate = {
    "vnum": 5001,
    "name": "Town Square",
    "description": "A wide road here is paved with a combination of brick and stone, leading off both east and west. The paving here forms a partial square, larger than the road itself. A cobblestone path leads south towards a three story timber building. A dirt path leads north to some ruins.",
    "sector": 1,
    "exits": [
        {
            "toVnum": 5002,
            "direction": 2,
            "exitFlags": 0,
            "description": null,
            "keyword": null,
            "keyVnum": 0,
            "defaultExitFlags": 0
        },
        {
            "toVnum": 5003,
            "direction": 0,
            "exitFlags": 0,
            "description": null,
            "keyword": "test",
            "keyVnum": 0,
            "defaultExitFlags": 0
        },
        {
            "toVnum": 5004,
            "direction": 1,
            "exitFlags": 0,
            "description": null,
            "keyword": null,
            "keyVnum": 0,
            "defaultExitFlags": 0
        },
        {
            "toVnum": 5005,
            "direction": 3,
            "exitFlags": 0,
            "description": null,
            "keyword": null,
            "keyVnum": 0,
            "defaultExitFlags": 0
        }
    ],
    "properties": {},
    "mapX": 0,
    "mapY": 0,
    "flags": null,
    "tags": [
        "asdf"
    ],
    "triggers": [
        {
            type: events_1.TriggerType.Enter,
            vnum: 1000,
            script: 1000,
            typeArg: "100"
        }
    ]
};
describe('Room', () => {
    it('should create a room from valid data', () => {
        const room = new room_1.Room(roomTemplate);
        expect(room).toBeTruthy();
    });
    it('should hold triggers', () => {
        let room = room_1.defaultRoom(999);
        const item = item_1.defaultItem(999);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        const sampleEvent = {
            args: [],
            entity: actor,
            target: undefined,
            type: undefined
        };
        const sampleResult0 = room.getTriggers(sampleEvent);
        expect(sampleResult0).toEqual([]);
        room = new room_1.Room(roomTemplate);
        expect(room).toBeTruthy();
        const sampleResult1 = room.getTriggers(sampleEvent);
        expect(sampleResult1).toEqual([]);
        sampleEvent.type = events_1.TriggerType.Enter;
        const sampleResult2 = room.getTriggers(sampleEvent);
        expect(sampleResult2.length).toEqual(1);
    });
    it('should hold items', () => {
        const room = new room_1.Room(roomTemplate);
        const item = item_1.defaultItem(999);
        expect(room.items).toEqual([]);
        expect(room.countItems(0)).toEqual(0);
        expect(room.findItem("asdf")).toEqual(undefined);
        room.addItem(item);
        expect(room.items).toEqual([item]);
        expect(room.findItem("asdf")).toEqual(undefined);
        expect(room.findItem("tab")).toEqual(item);
        expect(room.countItems(999)).toEqual(1);
        room.removeItem(item);
        expect(room.items).toEqual([]);
        expect(room.countItems(999)).toEqual(0);
    });
    it('should hold actors', () => {
        const room = new room_1.Room(roomTemplate);
        const actor = actor_1.defaultActor(999);
        expect(room.entities).toEqual([]);
        expect(room.countEntities(0)).toEqual(0);
        expect(room.findEntity(actor)).toEqual(undefined);
        room.addEntity(actor);
        expect(room.entities).toEqual([actor]);
        expect(room.findEntity(null)).toEqual(undefined);
        expect(room.findEntity(actor)).toEqual(actor);
        expect(room.countEntities(999)).toEqual(1);
        room.removeEntity(actor);
        expect(room.entities).toEqual([]);
        expect(room.countEntities(999)).toEqual(0);
    });
    it('should have exits', () => {
        const room = new room_1.Room(roomTemplate);
        expect(room.exits).toBeDefined();
        expect(room.exits.length).toEqual(roomTemplate.exits.length);
        expect(room.findExitByName(null)).not.toBeDefined();
        expect(room.findExitByName("zzzzz")).not.toBeDefined();
        expect(room.findExitByName(roomTemplate.exits[1].keyword)).toEqual(roomTemplate.exits[1]);
        expect(room.findExitByDirection(roomTemplate.exits[2].direction)).toEqual(roomTemplate.exits[2]);
        expect(room.findExitByDirection(4)).not.toBeDefined();
        const exit = room.findExitByName("north");
        expect(exit).toBeDefined();
        expect(exit).toEqual(roomTemplate.exits[1]);
    });
    // Tags
    // Properties.
    it('should create a valid default room', () => {
        expect(room_1.defaultRoom(999)).toBeTruthy();
    });
});
//# sourceMappingURL=room.test.js.map