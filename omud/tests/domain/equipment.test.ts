import {IItemTemplate, Item} from "../../src/domain/item";
import {WearFlags} from "../../src/domain/wear-flags";
import {Equipment, WearPosition} from "../../src/domain/equipment";

const itemTemplate: IItemTemplate = {
    "condition": 100,
    "cost": 1,
    "level": 1,
    "longDesc": "A small stone lies here.",
    "material": "stone",
    "shortDesc": "a small stone",
    "wearFlags": WearFlags.Take | WearFlags.Hold,
    "weight": 100,
    "type": 13,
    "vnum": 5001,
    "name": "small stone",
    "description": "",
    "triggers": [],
    "tags": [],
    "properties": {}
};

describe('Wear Slot', () => {

    it('should equip an item to a slot', () => {
        const item = new Item(itemTemplate);
        expect(item).toBeTruthy();
        let equipment = new WearPosition(WearFlags.Hold, "hold1");
        expect(equipment.getItemView()).toEqual({
            slotDesc: "hold1",
            itemDesc: "Nothing"
        });
        let canEquip = equipment.canEquip(item);
        expect(canEquip).toEqual(true);
        let result = equipment.equip(item);
        expect(result).toBeTruthy();
        expect(equipment.getItemView()).toEqual({
            slotDesc: "hold1",
            itemDesc: item.getShortDesc()
        });
    });

    it('should remove an item from a slot', () => {
        const item = new Item(itemTemplate);
        expect(item).toBeTruthy();
        let equipment = new WearPosition(WearFlags.Hold, "hold1");
        let result = equipment.equip(item);
        expect(result).toBeTruthy();
        let removedItem = equipment.remove();
        expect(removedItem).toEqual(item);
    });

    it('should allow item retrieval and determination', () => {
        const item = new Item(itemTemplate);
        expect(item).toBeTruthy();
        let equipment = new WearPosition(WearFlags.Hold, "hold1");
        let result = equipment.equip(item);
        expect(result).toBeTruthy();
        expect(equipment.hasItem()).toEqual(true);
        expect(equipment.hasItem("stone")).toEqual(true);
        expect(equipment.getItem()).toEqual(item);
    });

    it('should refuse an item that does not match the slot', () => {
        const item = new Item(itemTemplate);
        expect(item).toBeTruthy();
        let equipment = new WearPosition(WearFlags.Head, "head");
        let canEquip = equipment.canEquip(item);
        expect(canEquip).toEqual(false);
    });

    it('should refuse an item if the slot is full', () => {
        const item = new Item(itemTemplate);
        expect(item).toBeTruthy();
        let equipment = new WearPosition(WearFlags.Hold, "head");
        let result = equipment.equip(item);
        expect(result).toBeTruthy();
        let canEquip = equipment.canEquip(item);
        expect(canEquip).toEqual(false);
    });

});

describe('Equipment', () => {

    it('should equip an item to a slot', () => {
        const item = new Item(itemTemplate);
        expect(item).toBeTruthy();
        let equipment = new Equipment();
        expect(equipment.getItemView()).toEqual([
            { slotDesc: "head", itemDesc: "Nothing" },
            { slotDesc: "about", itemDesc: "Nothing" },
            { slotDesc: "neck",  itemDesc: "Nothing" },
            { slotDesc: "body", itemDesc: "Nothing" },
            { slotDesc: "arms", itemDesc: "Nothing" },
            { slotDesc: "hands", itemDesc: "Nothing" },
            { slotDesc: "waist", itemDesc: "Nothing" },
            { slotDesc: "legs", itemDesc: "Nothing" },
            { slotDesc: "feet", itemDesc: "Nothing" },
            { slotDesc: "shield", itemDesc: "Nothing" },
            { slotDesc: "wrist1", itemDesc: "Nothing" },
            { slotDesc: "wrist2", itemDesc: "Nothing" },
            { slotDesc: "finger1", itemDesc: "Nothing" },
            { slotDesc: "finger2", itemDesc: "Nothing" },
            { slotDesc: "hold1", itemDesc: "Nothing" },
            { slotDesc: "hold2", itemDesc: "Nothing" }
        ]);
        let canEquip = equipment.canEquip(item);
        expect(canEquip).toEqual(true);
        let result = equipment.equip(item);
        expect(result).toBeTruthy();
        expect(equipment.getItemView()).toEqual([
            { slotDesc: "head", itemDesc: "Nothing" },
            { slotDesc: "about", itemDesc: "Nothing" },
            { slotDesc: "neck", itemDesc: "Nothing" },
            { slotDesc: "body", itemDesc: "Nothing" },
            { slotDesc: "arms", itemDesc: "Nothing" },
            { slotDesc: "hands", itemDesc: "Nothing" },
            { slotDesc: "waist", itemDesc: "Nothing" },
            { slotDesc: "legs", itemDesc: "Nothing" },
            { slotDesc: "feet", itemDesc: "Nothing" },
            { slotDesc: "shield", itemDesc: "Nothing" },
            { slotDesc: "wrist1", itemDesc: "Nothing" },
            { slotDesc: "wrist2", itemDesc: "Nothing" },
            { slotDesc: "finger1", itemDesc: "Nothing" },
            { slotDesc: "finger2", itemDesc: "Nothing" },
            { slotDesc: "hold1", itemDesc: item.getShortDesc() },
            { slotDesc: "hold2",  itemDesc: "Nothing" }
        ]);
    });

    it('should remove an item from a slot', () => {
        const item = new Item(itemTemplate);
        expect(item).toBeTruthy();
        let equipment = new Equipment();
        let result = equipment.equip(item);
        expect(result).toBeTruthy();
        let removedItem = equipment.remove(item.getName());
        expect(removedItem).toEqual(item);
    });

    it('should allow item retrieval and determination', () => {
        const item = new Item(itemTemplate);
        expect(item).toBeTruthy();
        let equipment = new Equipment();
        let result = equipment.equip(item);
        expect(result).toBeTruthy();
        expect(equipment.hasItem(item.getName())).toEqual(true);
        expect(equipment.hasItem("stone")).toEqual(true);
        expect(equipment.getItem()).toEqual([item]);
    });

    it('should refuse an item that does not match the slot', () => {
        const item = new Item(itemTemplate);
        item.wearFlags = 0;
        expect(item).toBeTruthy();
        let equipment = new Equipment();
        let canEquip = equipment.canEquip(item);
        expect(canEquip).toEqual(false);
    });

    it('should refuse an item if the slot is full', () => {
        // There are two hold slots.
        const item = new Item(itemTemplate);
        const item2 = new Item(itemTemplate);
        expect(item).toBeTruthy();
        let equipment = new Equipment();
        let result = equipment.equip(item);
        expect(result).toBeTruthy();
        result = equipment.equip(item2);
        expect(result).toBeTruthy();
        let canEquip = equipment.canEquip(item);
        expect(canEquip).toEqual(false);
    });

});
