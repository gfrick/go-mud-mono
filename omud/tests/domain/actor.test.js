"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const actor_1 = require("../../src/domain/actor");
const actorTemplate = {
    "alignment": 0,
    "armor": [
        0,
        0,
        0,
        0
    ],
    "damageDice": {
        "diceCount": 1,
        "diceFaces": 1,
        "bonus": 1
    },
    "damageType": "punch",
    "defaultPosition": 8,
    "group": 0,
    "hitDice": {
        "diceCount": 1,
        "diceFaces": 1,
        "bonus": 1
    },
    "hitRoll": 1,
    "level": 1,
    "longDesc": "A priestess stands here in a plain cloak.",
    "manaDice": {
        "diceCount": 1,
        "diceFaces": 1,
        "bonus": 1
    },
    "race": "human",
    "sex": 2,
    "shortDesc": "Priestess Liliana",
    "size": 2,
    "startPosition": 8,
    "wealth": 10,
    "vnum": 5001,
    "name": "priestess liliana diris",
    "description": "She has long, curled, brown hair shaved on the left side and brown eyes.\nShe has rough, sunburned, gray skin.\nShe stands 104cm (3'4\") tall and has a regular build.\nShe has an oval, very common face.",
    "triggers": [],
    "tags": [],
    "properties": {}
};
describe('Actor', () => {
    it('should create an actor', () => {
        const actor = new actor_1.Actor(actorTemplate);
        expect(actor).toBeTruthy();
    });
});
//# sourceMappingURL=actor.test.js.map