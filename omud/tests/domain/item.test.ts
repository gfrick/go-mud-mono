
import 'jest';
import {IItemTemplate, Item} from "../../src/domain/item";

const itemTemplate: IItemTemplate = {
	"condition": 100,
	"cost": 1,
	"level": 1,
	"longDesc": "A small flagstone well with thatch roof.",
	"material": "stone",
	"shortDesc": "flagstone well",
	"wearFlags": 0,
	"weight": 100,
	"type": 13,
	"vnum": 5001,
	"name": "flagstone well",
	"description": "",
	"triggers": [],
	"tags": [],
	"properties": {}
};

describe('Item', () => {
	it('should create an item', () => {
		const item = new Item(itemTemplate);
		expect(item).toBeTruthy();
	});
});

