import 'jest';
import {viewService} from "../../src/mocks/view.service.mock";
import {worldService} from "../../src/mocks/world.service.mock";
import {CommandService} from "../../src/services/command.service";
import {scriptService} from "../../src/mocks/script.service.mock";

describe('Service: Command', () => {

    const mockViewService = viewService();
    const mockWorldService = worldService();
    const mockScriptService = scriptService();
    const service = new CommandService(mockViewService, mockWorldService, mockScriptService);
    service.registerCommands();

    it('should parse a bad command to null', () => {
        let result = service.parseCommand("hello");
        expect(result).toEqual("hello");
        result = service.parseCommand("");
        expect(result).toEqual("");
        result = service.parseCommand(null);
        expect(result).toEqual("");
    });

    it('should parse a valid command to that command', () => {
        let result = service.parseCommand("look");
        expect(result).toEqual("look");
        result = service.parseCommand("look guard");
        expect(result).toEqual("look");
    });

    it('should parse a shortcut command to that command', () => {
        const result = service.parseCommand("l");
        expect(result).toEqual("look");
    });

    it('should find commands to run', () => {
        let result = service.findCommand("look");
        expect(result).toBeTruthy();
        result = service.findCommand("asdf");
        expect(result).toEqual(service.nullCommand);
    });

});

