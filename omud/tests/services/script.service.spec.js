"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const item_1 = require("../../src/domain/item");
const actor_1 = require("../../src/domain/actor");
const room_1 = require("../../src/domain/room");
const events_1 = require("../../src/domain/events/events");
const flags_1 = require("../../src/domain/flags");
const wear_flags_1 = require("../../src/domain/wear-flags");
const script_service_1 = require("../../src/services/script.service");
const get_command_1 = require("../../src/commands/get.command");
const view_service_mock_1 = require("../../src/mocks/view.service.mock");
const world_service_mock_1 = require("../../src/mocks/world.service.mock");
describe('Service: Script', () => {
    const mockViewService = view_service_mock_1.viewService();
    const mockWorldService = world_service_mock_1.worldService();
    const service = new script_service_1.ScriptService(mockWorldService);
    const get = new get_command_1.GetCommand(mockViewService);
    it('should run the correct item script, sending output to player.', () => {
        const room = room_1.defaultRoom(999);
        const item = item_1.defaultItem(999, {
            triggers: [
                {
                    type: events_1.TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        room.addItem(item);
        item.wearFlags = flags_1.BitFlags.addFlag(item.wearFlags, wear_flags_1.WearFlags.Take); // can pick it up.
        let output = [];
        actor.controller = {
            getPlayerId() {
                return "tester";
            },
            handlePlayerInput() {
                return "";
            },
            queuePlayerOutput(data) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        };
        const event = get.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.Get);
        const triggers = room.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(1);
        service.runScriptForEvent(actor, event);
        expect(output.length).toEqual(2);
        expect(output.shift()).toEqual("You pick up a can tab.");
        expect(output.shift()).toEqual("test output");
    });
    it('should run the correct room script, sending output to player.', () => {
        const room = room_1.defaultRoom(999, {
            triggers: [
                {
                    type: events_1.TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const item = item_1.defaultItem(999);
        const actor = actor_1.defaultActor(999);
        room.addEntity(actor);
        room.addItem(item);
        item.wearFlags = flags_1.BitFlags.addFlag(item.wearFlags, wear_flags_1.WearFlags.Take); // can pick it up.
        let output = [];
        actor.controller = {
            getPlayerId() {
                return "tester";
            },
            handlePlayerInput() {
                return "";
            },
            queuePlayerOutput(data) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        };
        const event = get.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.Get);
        const triggers = room.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(1);
        service.runScriptForEvent(actor, event);
        expect(output.length).toEqual(2);
        expect(output.shift()).toEqual("You pick up a can tab.");
        expect(output.shift()).toEqual("test output");
    });
    it('should run the correct actor script, sending output to player.', () => {
        const room = room_1.defaultRoom(999);
        const item = item_1.defaultItem(999);
        const actor = actor_1.defaultActor(999, {
            triggers: [
                {
                    type: events_1.TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        room.addEntity(actor);
        room.addItem(item);
        item.wearFlags = flags_1.BitFlags.addFlag(item.wearFlags, wear_flags_1.WearFlags.Take); // can pick it up.
        let output = [];
        actor.controller = {
            getPlayerId() {
                return "tester";
            },
            handlePlayerInput() {
                return "";
            },
            queuePlayerOutput(data) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        };
        const event = get.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.Get);
        const triggers = room.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(1);
        service.runScriptForEvent(actor, event);
        expect(output.length).toEqual(2);
        expect(output.shift()).toEqual("You pick up a can tab.");
        expect(output.shift()).toEqual("test output");
    });
    it('should run the correct script on all, sending output to player.', () => {
        const room = room_1.defaultRoom(999, {
            triggers: [
                {
                    type: events_1.TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const item = item_1.defaultItem(999, {
            triggers: [
                {
                    type: events_1.TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const actor = actor_1.defaultActor(999, {
            triggers: [
                {
                    type: events_1.TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const actor2 = actor_1.defaultActor(1000);
        room.addEntity(actor);
        room.addEntity(actor2);
        room.addItem(item);
        item.wearFlags = flags_1.BitFlags.addFlag(item.wearFlags, wear_flags_1.WearFlags.Take); // can pick it up.
        let output = [];
        actor2.controller = {
            getPlayerId() {
                return "tester";
            },
            handlePlayerInput() {
                return "";
            },
            queuePlayerOutput(data) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        };
        const event = get.invoke(actor2, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(events_1.TriggerType.Get);
        let triggers = room.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(3);
        triggers = actor.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(1);
        triggers = item.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(1);
        service.runScriptForEvent(actor2, event);
        expect(output.length).toEqual(4);
        expect(output.shift()).toEqual("You pick up a can tab.");
        expect(output.shift()).toEqual("test output");
        expect(output.shift()).toEqual("test output");
        expect(output.shift()).toEqual("test output");
    });
});
//# sourceMappingURL=script.service.spec.js.map