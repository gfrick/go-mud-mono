import 'jest';
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultActor} from "../../src/mocks/actor.default";
import {defaultItem} from "../../src/mocks/item.defaut";
import {TriggerType} from "../../src/domain/events/events";
import {BitFlags} from "../../src/domain/flags";
import {WearFlags} from "../../src/domain/wear-flags";
import {ScriptService} from "../../src/services/script.service";
import {GetCommand} from "../../src/commands/get.command";
import {viewService} from "../../src/mocks/view.service.mock";
import {worldService} from "../../src/mocks/world.service.mock";
import {commandService} from "../../src/mocks/command.service.mock";
import WebSocket = require("ws");

describe('Service: Script', () => {

    const mockViewService = viewService();
    const mockWorldService = worldService();
    const mockCommandService = commandService();
    const service = new ScriptService(mockWorldService);
    const get = new GetCommand(mockViewService);

    it('should run the correct item script, sending output to player.', () => {
        const room = defaultRoom(999);
        const item = defaultItem(999, {
            triggers: [
                {
                    type: TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const actor = defaultActor(999);
        room.addEntity(actor);
        room.addItem(item);
        item.wearFlags = BitFlags.addFlag(item.wearFlags, WearFlags.Take); // can pick it up.
        let output: string[] = [];
        actor.controller = {
            handleHostInput(): WebSocket.Data {
                return undefined;
            }, handleHostOutput(): any {
            }, isInputReady(): boolean {
                return false;
            }, isOutputReady(): boolean {
                return false;
            }, queueHostInput(data: WebSocket.Data): void {
            }, queueHostOutput(data: any): void {
            }, queuePlayerInput(data: WebSocket.Data) {
            }, sendHostOutput(data: any): void {
            },
            getPlayerId(): string {
                return "tester";
            },
            handlePlayerInput(): string {
                return "";
            },
            queuePlayerOutput(data: any) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        }

        const event = get.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(TriggerType.Get);

        const triggers = room.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(1);

        service.runScriptForEvent(actor, event, mockCommandService);
        expect(output.length).toEqual(2);
        expect(output.shift()).toEqual("You pick up a can tab.");
        expect(output.shift()).toEqual("test output");
    });

    it('should run the correct room script, sending output to player.', () => {
        const room = defaultRoom(999, {
            triggers: [
                {
                    type: TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const item = defaultItem(999);
        const actor = defaultActor(999);
        room.addEntity(actor);
        room.addItem(item);
        item.wearFlags = BitFlags.addFlag(item.wearFlags, WearFlags.Take); // can pick it up.
        let output: string[] = [];
        actor.controller = {
            handleHostInput(): WebSocket.Data {
                return undefined;
            }, handleHostOutput(): any {
            }, isInputReady(): boolean {
                return false;
            }, isOutputReady(): boolean {
                return false;
            }, queueHostInput(data: WebSocket.Data): void {
            }, queueHostOutput(data: any): void {
            }, queuePlayerInput(data: WebSocket.Data) {
            }, sendHostOutput(data: any): void {
            },
            getPlayerId(): string {
                return "tester";
            },
            handlePlayerInput(): string {
                return "";
            },
            queuePlayerOutput(data: any) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        }

        const event = get.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(TriggerType.Get);

        const triggers = room.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(1);

        service.runScriptForEvent(actor, event, mockCommandService);
        expect(output.length).toEqual(2);
        expect(output.shift()).toEqual("You pick up a can tab.");
        expect(output.shift()).toEqual("test output");
    });

    it('should run the correct actor script, sending output to player.', () => {
        const room = defaultRoom(999);
        const item = defaultItem(999);
        const actor = defaultActor(999, {
            triggers: [
                {
                    type: TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        room.addEntity(actor);
        room.addItem(item);
        item.wearFlags = BitFlags.addFlag(item.wearFlags, WearFlags.Take); // can pick it up.
        let output: string[] = [];
        actor.controller ={
            handleHostInput(): WebSocket.Data {
                return undefined;
            }, handleHostOutput(): any {
            }, isInputReady(): boolean {
                return false;
            }, isOutputReady(): boolean {
                return false;
            }, queueHostInput(data: WebSocket.Data): void {
            }, queueHostOutput(data: any): void {
            }, queuePlayerInput(data: WebSocket.Data) {
            }, sendHostOutput(data: any): void {
            },
            getPlayerId(): string {
                return "tester";
            },
            handlePlayerInput(): string {
                return "";
            },
            queuePlayerOutput(data: any) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        }

        const event = get.invoke(actor, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(TriggerType.Get);

        const triggers = room.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(1);

        service.runScriptForEvent(actor, event, mockCommandService);
        expect(output.length).toEqual(2);
        expect(output.shift()).toEqual("You pick up a can tab.");
        expect(output.shift()).toEqual("test output");
    });

    it('should run the correct script on all, sending output to player.', () => {
        const room = defaultRoom(999, {
            triggers: [
                {
                    type: TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const item = defaultItem(999, {
            triggers: [
                {
                    type: TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const actor = defaultActor(999, {
            triggers: [
                {
                    type: TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const actor2 = defaultActor(1000);

        room.addEntity(actor);
        room.addEntity(actor2);
        room.addItem(item);
        item.wearFlags = BitFlags.addFlag(item.wearFlags, WearFlags.Take); // can pick it up.
        let output: string[] = [];
        actor2.controller = {
            handleHostInput(): WebSocket.Data {
                return undefined;
            }, handleHostOutput(): any {
            }, isInputReady(): boolean {
                return false;
            }, isOutputReady(): boolean {
                return false;
            }, queueHostInput(data: WebSocket.Data): void {
            }, queueHostOutput(data: any): void {
            }, queuePlayerInput(data: WebSocket.Data) {
            }, sendHostOutput(data: any): void {
            },
            getPlayerId(): string {
                return "tester";
            },
            handlePlayerInput(): string {
                return "";
            },
            queuePlayerOutput(data: any) {
                if (data && data.message) {
                    output.push(data.message);
                }
            }
        }

        const event = get.invoke(actor2, "tab");
        expect(event).toBeDefined();
        expect(event.type).toEqual(TriggerType.Get);

        let triggers = room.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(3);
        triggers = actor.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(1);
        triggers = item.getTriggers(event);
        expect(triggers).toBeDefined();
        expect(triggers).toHaveLength(1);

        service.runScriptForEvent(actor2, event, mockCommandService);
        expect(output.length).toEqual(4);
        expect(output.shift()).toEqual("You pick up a can tab.");
        expect(output.shift()).toEqual("test output");
        expect(output.shift()).toEqual("test output");
        expect(output.shift()).toEqual("test output");
    });
});

