import 'jest';
import {TriggerType} from "../../src/domain/events/events";
import {TemplateService} from "../../src/services/template.service";
import {viewService} from "../../src/mocks/view.service.mock";
import {worldService} from "../../src/mocks/world.service.mock";
import {commandService} from "../../src/mocks/command.service.mock";
import {defaultRoom} from "../../src/mocks/room.default";
import {defaultItem} from "../../src/mocks/item.defaut";
import {defaultActor} from "../../src/mocks/actor.default";

describe('Service: Template', () => {

    const mockViewService = viewService();
    const mockWorldService = worldService();
    const mockCommandService = commandService();
    const service = new TemplateService();

    it('should return a complete score template', () => {
        const room = defaultRoom(999);
        const item = defaultItem(999, {
            triggers: [
                {
                    type: TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const actor = defaultActor(999);
        room.addEntity(actor);
        room.addItem(item);

        const template = TemplateService.createScoreView(actor);
        expect(template).toBeDefined();
        expect(template.score.level).toEqual(1);
        expect(template.score.name).toEqual("a black cat");
    });

    it('should return a complete room template', () => {
        const room = defaultRoom(999);
        const item = defaultItem(999, {
            triggers: [
                {
                    type: TriggerType.Get,
                    vnum: 1000,
                    script: 999,
                    typeArg: "100"
                }
            ]
        });
        const actor = defaultActor(999);
        room.addEntity(actor);
        room.addItem(item);

        const template = TemplateService.createRoomView(actor);
        expect(template).toBeDefined();
    });

});

