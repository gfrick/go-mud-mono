#!/bin/sh
: ${MONGO_INITDB_HOST:=mongo}
: ${MONGO_PORT:=27017}

echo "**** Initial Mongo Seeding on: ($MONGO_INITDB_HOST:$MONGO_PORT)."
until nc -z $MONGO_INITDB_HOST $MONGO_PORT
do
    echo "Waiting for Mongo ($MONGO_INITDB_HOST:$MONGO_PORT) to start..."
    sleep 0.5
done

mongo $MONGO_INITDB_DATABASE --host $MONGO_INITDB_HOST --port $MONGO_PORT -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase admin --eval "db.createUser({user: '$DATABASE_USERNAME', pwd: '$DATABASE_PASSWORD', roles:[{role:'dbOwner', db: '$MONGO_INITDB_DATABASE'}]});"
mongoimport --host $MONGO_INITDB_HOST --db $MONGO_INITDB_DATABASE --collection account --drop --file ./init.json --jsonArray --username $DATABASE_USERNAME --password $DATABASE_PASSWORD
mongoimport --host $MONGO_INITDB_HOST --db $MONGO_INITDB_DATABASE --collection area --drop --file ./areas.json --jsonArray --username $DATABASE_USERNAME --password $DATABASE_PASSWORD
