
* init.json is account (should be accounts.json?)
* You should be able to put any areas you want pre-loaded mongo-style into the areas.json

## Deployment
The build will target the following results:

1. The client is simply an artifact, and expects to be run behind a proxy (nginx in this case)
2. the mud will have its Dockerfile.prod become its normal Dockerfile
3. The docker-compose.prod.yml will be promoted to just docker-compose.yml
4. everything should arrive in 'omud-run'

With these in place in the deployed production sequence, 'docker-compose up' should launch the game.

This obviously is not working right, as things aren't obvious to run.
